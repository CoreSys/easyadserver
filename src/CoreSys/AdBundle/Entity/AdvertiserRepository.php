<?php

namespace CoreSys\AdBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * AdvertiserRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class AdvertiserRepository extends EntityRepository
{
    /**
     * @param $data
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getDatatablesQueryBuilder( &$data )
    {
        $q = $this->createQueryBuilder( 'e' )
            ->setMaxResults( intval( $data[ 'limit' ]))
            ->setFirstResult( intval( $data[ 'offset']));

        $search_roles = false;

        if( !empty( $data[ 'searchString' ] ) ) {
            $search_query = array();
            $search_user = false;
            $search_user_vars = array();
            $search_campaign_count = false;
            $search_ad_count = false;

            foreach( $data[ 'columns' ] as $col ) {
                if( $col[ 'searchable' ] ) {
                    $col[ 'id' ] = preg_replace('/_(.?)/e',"strtoupper('$1')", $col['id'] );
//                    echo $col[ 'id' ] . '<br>';
                    if( strstr( $col[ 'id' ], 'user.' ) ) {
                        $search_user = true;
                        $search_user_vars[] = $col[ 'id' ];
                    } else if( $col[ 'id' ] == 'campaignsCount' ) {
                        $search_campaign_count = true;
                    } else if( $col[ 'id' ] == 'adsCount' ) {
                        $search_ad_count = true;
                    } else {
                        $search_query[] = 'e.' . $col[ 'id' ] . ' LIKE :search';
                    }
                }
            }

            if( $search_user ) {
                $string = array();
                foreach( $search_user_vars as $k => $v ) {
                    $var = str_replace( 'user.', 'u.', $v );
                    $string[] = $var . ' LIKE \'%' . $data[ 'searchString' ] . '%\'';

                    $search_query[] = $var . ' LIKE :search';
                }
                $string = '(' . implode( ' OR ', $string ) . ')';
                $q->leftJoin( 'e.user', 'u', 'WITH', $string );
            }

            if( $search_roles ) {
                $q->leftJoin( 'e.sys_roles', 'r', 'WITH', 'r.name LIKE \'%' . $data[ 'searchString' ] . '%\'');
                $search_query[] = 'r.name LIKE :search';
            }

            $search_query = '(' . implode( ' OR ', $search_query ) . ')';
            $q->where( $search_query )
                ->setParameter( 'search', '%' . $data[ 'searchString' ] . '%' );
        }

        $sort_col = $data[ 'columns' ][ $data[ 'sortColumn' ] ][ 'id' ];
        $sort_dir = $data[ 'sortDir' ];

        $sort_col = preg_replace('/_(.?)/e',"strtoupper('$1')",$sort_col);

        $q->orderBy( 'e.' . $sort_col, $sort_dir );

        return $q;
    }

    /**
     * @param $data
     * @return \Doctrine\ORM\Query|\Doctrine\ORM\QueryBuilder
     */
    public function getDatatablesQuery( &$data ) {
        $q = $this->getDatatablesQueryBuilder( $data );
        $q = $q->getQuery();

        return $q;
    }

    /**
     * @param $data
     * @param bool $return_results
     * @return array
     */
    public function getDatatablesResults( &$data, $return_results = false )
    {
        $q = $this->getDatatablesQuery( $data );

        $results = $q->getResult();

        if( $return_results ) {
            return $results;
        }

        $return = array(
            'sEcho' => $data[ 'echo' ],
            'iTotalRecords' => count( $results ),
            'iTotalDisplayRecords' => $this->getDatatablesAllCount( $data ),
            'aaData' => array()
        );

        foreach( $results as $idx => $entity ) {
            $entity_data = $this->getDataTablesEntityDataRow( $entity );

            if( !isset( $return[ 'aaData' ][ $idx ] ) ) {
                $return[ 'aaData' ][ $idx ] = array();
            }

            if( empty( $data[ 'columns' ][ 0 ][ 'id' ] ) ) {
                $return[ 'aaData' ][ $idx ][0] = null;
            }

            foreach( $entity_data as $k => $v ) {
                $return[ 'aaData' ][ $idx ][ $k ] = $v;
            }

            if( empty( $data[ 'columns' ][ count( $data[ 'columns' ] ) - 1 ][ 'id'] ) )  {
                $return[ 'aaData' ][ $idx ][ count( $data[ 'columns' ]) -1] = null;
            }
        }

        return $return;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getDatatablesAllCount( $data )
    {
        $q = $this->getDatatablesQueryBuilder( $data );
        $q->select( 'COUNT(e.id)' );
        $q = $q->getQuery();

        $q->setFirstResult( null );
        $q->setMaxResults( null );

        $results = $q->getResult();

        return $q->getSingleScalarResult();
    }

    /**
     * @param $entity
     * @return array
     */
    public function getDataTablesEntityDataRow( &$entity )
    {
        $return = array();

        foreach( $entity->getAdminTableArray() as $k => $v ) {
            $return[ $k ] = $v;
        }

        if( $return[ 'active' ] === true) {
            $return[ 'active' ] = '<i class="fa fa-check green" title="Active"></i>';
            $return[ 'status' ] = '<i class="fa fa-check green" title="Active"></i>';
        } else {
            $return[ 'active' ] = '<i class="fa fa-times red" title="InActive"></i>';
            $return[ 'status' ] = '<i class="fa fa-times red" title="InActive"></i>';
        }

        $class = get_class( $entity );
        $class = explode( DIRECTORY_SEPARATOR, $class );
        $class = array_pop( $class );
        $class = explode( '/', $class );
        $class = array_pop( $class );
        $class = explode( '\\', $class );
        $class = array_pop( $class );
        $class = trim( ucwords( str_replace( '_', ' ', strtolower( $class ) ) ) );

        $id = $entity->getId();

        $actions = array(
            '<li class="manage-row text-left" data-slug="' . $entity->getUser()->getUsername() . '"><a class="btn-manage" href="javascript:void(0);"><icon class="fa fa-cogs" title="Manage ' . $class . '"></i> Manage ' . $class . '</a></li>',
//            '<li class="view-row text-left"><a class="btn-view" href="javascript:void(0);"><icon class="fa fa-eye" title="View ' . $class . '"></i> View ' . $class . '</a></li>',
            '<li class="edit-row text-left"><a class="btn-edit" href="javascript:void(0);"><icon class="fa fa-edit" title="Edit ' . $class . '"></i> Edit ' . $class . '</a></li>',
            '<li class="remove-row text-left"><a class="btn-remove" href="javascript:void(0);"><icon class="fa fa-times red" title="Remove ' . $class . '"></i> Remove ' . $class . '</a></li>'
        );

        $actions = '<div class="btn-group"><button class="btn btn-xs dropdown-toggle" type="button" data-toggle="dropdown">Options <i class="fa fa-angle-down"></i></button><ul class="dropdown-menu pull-right" role="menu">' . implode( $actions ) . '</ul></div>';
        $return[ 'options' ] = $actions;
        $return[ 'actions' ] = $actions;

        return $return;
    }

    /**
     * @param $slug
     * @return null|object
     */
    public function locateAdvertiser( $slug )
    {
        $row = $this->findOneById( $slug );
        if( empty( $row ) ) {
            $user_repo = $this->getEntityManager()->getRepository( 'CoreSysUserBundle:User' );
            $user = $user_repo->findOneByUsername( $slug );
            if( empty( $user ) ) {
                $user = $user_repo->findOneByEmail( $slug );
                if( empty( $user ) ) {
                    return null;
                }
            }
            $row = $this->findOneBy( array( 'user' => $user ) );
        }

        return $row;
    }
}
