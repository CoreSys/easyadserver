<?php

namespace CoreSys\AdBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * CampaignRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CampaignRepository extends EntityRepository
{
    public function locateCampaign( $hash = null )
    {
        $check = array( 'id', 'name' );
        foreach( $check as $c ) {
            $data = $this->findOneBy( array( $c => $hash ) );
            if( !empty( $data ) ) {
                return $data;
            }
        }
        return null;
    }

    public function getCount( $active = null )
    {
        $q = $this->createQueryBuilder( 'c' )
            ->select( 'COUNT(c.id)');

        if( $active !== null ) {
            $active = $active === true;
            $q->where( 'c.active = :active' )
                ->setParameter( 'active', $active );
        }

        $q = $q->getQuery();

        return $q->getSingleScalarResult();
    }
}
