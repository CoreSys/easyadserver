<?php

namespace CoreSys\CampaignBundle\Controller;

use CoreSys\CampaignBundle\Entity\Campaign;
use CoreSys\CampaignBundle\Form\CampaignType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CoreSys\SiteBundle\Controller\AjaxController as BaseController;
use CoreSys\CampaignBundle\Entity\CampaignRepository;

/**
 * Class AdminController
 * @package CoreSys\CampaignBundle\Controller
 * @Route("/admin/ajax/campaigns", options={"expose"=true})
 */
class AdminAjaxController extends BaseController
{
    /**
     * @return CampaignRepository
     */
    public function getCRepo()
    {
        return $this->getRepo( 'CoreSysCampaignBundle:Campaign' );
    }

    /**
     * @Route("/data_tables", name="admin_ajax_datatables")
     * @Template()
     */
    public function dataTablesAction()
    {
        $repo = $this->getRepo( 'CoreSysCampaignBundle:Campaign' );
        $data = $this->processDatatables();
        $results = $this->getDatatablesResults( $repo, $data );
        $results[ 'data' ] = $data;

        $this->echoJsonResponse( $results, true );
        exit;
    }

    /**
     * @Route("/new", name="admin_ajax_campaigns_new", defaults={"id"=null})
     * @Route("/edit/{id}", name="admin_ajax_campaigns_edit", defaults={"id"=null})
     * @Template()
     */
    public function campaignFormAction( $id )
    {
        $id = intval( $id );
        $repo = $this->getCRepo();
        $campaign = $repo->findOneById( $id );
        $new = true;
        if( empty( $campaign ) ) {
            $action = $this->generateUrl( 'admin_ajax_campaigns_save_new' );
            $new = true;
            $title = 'Create New Campaign';
            $campaign = new Campaign();
        } else {
            $action = $this->generateUrl( 'admin_ajax_campaigns_save_campaign', array( 'id' => $campaign->getId() ) );
            $new = false;
            $title = 'Edit Campaign ' . $campaign->getName();
        }

        $form = $this->createForm( new CampaignType(), $campaign );

        return array( 'form' => $form->createView(), 'title' => $title, 'action' => $action, 'new' => $new );
    }

    /**
     * @Route("/save_new", name="admin_ajax_campaigns_save_new")
     * @Template()
     */
    public function saveNewCampaignAction()
    {
        $campaign = new Campaign();
        $form = $this->createForm( new CampaignType(), $campaign );
        $request = $this->get( 'request' );

        if( $request->isMethod( 'POST' ) ) {
            $form->handleRequest( $request );
            if( $form->isValid() ) {
                $campaign = $form->getData();
                $this->persist( $campaign );
                $this->flush();

                $data = $this->getCrepo()->getDataTablesEntityDataRow( $campaign );

                $this->echoJsonSuccess( 'Successfully added new campaign', $data );
            } else {
                $this->echoJsonError( 'There were form errors.' );
            }
        } else {
            $this->echoJsonError( 'No post detected' );
        }

        exit;
    }

    /**
     * @Route("/save_campaign/{id}", name="admin_ajax_campaigns_save_campaign", defaults={"id"=null})
     * @ParamConverter("campaign", class="CoreSysCampaignBundle:Campaign")
     * @Template()
     */
    public function saveCampaignAction( Campaign $campaign )
    {
        $form = $this->createForm( new CampaignType(), $campaign );
        $request = $this->get( 'request' );

        if( $request->isMethod( 'POST' ) ) {
            $form->handleRequest( $request );
            if( $form->isValid() ) {
                $campaign = $form->getData();
                $this->persist( $campaign );
                $this->flush();

                $data = $this->getCrepo()->getDataTablesEntityDataRow( $campaign );

                $this->echoJsonSuccess( 'Successfully saved campaign', $data );
            } else {
                $this->echoJsonError( 'There were form errors.' );
            }
        } else {
            $this->echoJsonError( 'No post detected' );
        }

        exit;
    }

    /**
     * @Route("/remove/{id}", name="admin_ajax_campaigns_remove_campaign", defaults={"id"=null})
     * @ParamConverter("campaign", class="CoreSysCampaignBundle:Campaign")
     * @Template()
     */
    public function removeCampaignAction( Campaign $campaign )
    {
        $name = $campaign->getName();
        $this->remove( $campaign );
        $this->flush();

        $this->echoJsonSuccess( 'Successfully removed campaign "' . $name  . '"' );
    }
}
