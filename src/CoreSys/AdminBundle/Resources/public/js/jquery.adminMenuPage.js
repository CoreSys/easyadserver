;(function($,undefined){
    $.fn.adminMenuPage = function(options) {
        var p = this,
            defaults = {
                checkable: false,
                entity: 'Menu',
                selected_count_selector: null,
                check_master_selector: null,
                check_row_selector: null,
                add_row_btn_selector: null,
                remove_rows_btn_selector: null,
                table_selector: '#menus-table',
                row_edit_btn_selector: null,
                row_remove_btn_selector: null,
                dt_display_length: 10,
                no_sort_columns: [0],
                new_modal_selector: null,
                edit_modal_selector: null,
                remove_row_url: null,
                add_row_url: null,
                edit_row_url: null,
                fnGetRowData: null,
                fnPopulateEditedRowData: null,
                aaSorting: [[ 5, "asc" ]],
                "aoColumns": [
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null
                ]
            };

        p.settings = {};
        p.page = null;

        function init()
        {
            p.settings = $.extend({}, defaults, options );
            p.page = new $.fn.adminBasicTablePage(p.settings);
        }

        init();

        return this;
    };
})(jQuery);

var page;
$(document).ready(function () {
    $('#menus-table tbody').sortable({
        placeholder: "ui-state-highlight",
        handle: '.handle'
    });

    $('.save-order').click(saveOrder);
    $('.toggle-active').click(function () {
        var id = $(this).attr('data-id');
        toggleActive($(this), id);
    });

    page = new $.fn.adminMenuPage();
    page = page.page;
});

function saveOrder() {
    var ids = [];
    $('#menus-table tbody tr').each(function () {
        var $tr = $(this),
            id = $tr.attr('data-id');
        ids.push(id);
    });

    $.ajax({
        url: Routing.generate('admin_ajax_save_menu_order'),
        type: 'POST',
        data: {'ids': ids },
        success: function (res) {
            res = JSON.parse(res);
            if (res.success) {
                toastr['success'](res.msg, 'Success');
                reorder();
            } else {
                toastr['error'](res.msg, 'Error');
            }
        }
    });
}

function reorder() {
    var pos = 0;
    $('#menus-table tbody tr').each(function () {
        var $tr = $(this);
        $tr.find('td.row-position').html(pos);
        pos++;
    });
}

function toggleActive(el, id) {
    $.ajax({
        url: Routing.generate('admin_ajax_toggle_menu_active') + '/' + id,
        success: function (res) {
            res = JSON.parse(res);
            if (res.success) {
                toastr['success'](res.msg, 'Success');
                if (res.data.active) {
                    el.find('i').removeClass('fa-times').removeClass('red').addClass('fa-check').addClass('green');
                } else {
                    el.find('i').removeClass('fa-check').removeClass('green').addClass('fa-times').addClass('red');
                }
            } else {
                toastr['error'](res.msg, 'Error');
            }
        }
    });
}