<?php

namespace CoreSys\AdBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CoreSys\SiteBundle\Controller\AdminController as BaseController;

/**
 * Class AdminPublishersController
 * @package CoreSys\AdBundle\Controller
 * @Route("/admin/ads/publishers")
 */
class AdminPublishersController extends BaseController
{
    /**
     * @Route("/", name="admin_ads_publishers_index")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }
}
