<?php

namespace CoreSys\UserBundle\FormModel;

use CoreSys\UserBundle\FormModel\NewThread;
use FOS\MessageBundle\Model\ParticipantInterface;

/**
 * Class AdminNewThread
 * @package CoreSys\UserBundle\FormModel
 */
class ModeratorNewThread extends NewThread
{
    /**
     * @var ParticipantInterface
     */
    protected $recipient;

    /**
     * @return \FOS\MessageBundle\Model\ParticipantInterface
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * @param ParticipantInterface $user
     * @return $this
     */
    public function setRecipient(ParticipantInterface $user = null)
    {
        $this->recipient = $user;
        return $this;
    }
}