<?php
namespace CoreSys\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\MessageBundle\Entity\Message as BaseMessage;

/**
 * @ORM\Table(name="user_message")
 * @ORM\Entity
 */
class Message extends BaseMessage
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(
     *   targetEntity="CoreSys\UserBundle\Entity\Thread",
     *   inversedBy="messages",
     *  cascade={"persist", "remove"}
     * )
     * @var ThreadInterface
     */
    protected $thread;

    /**
     * @ORM\ManyToOne(targetEntity="CoreSys\UserBundle\Entity\User")
     * @var ParticipantInterface
     */
    protected $sender;

    /**
     * @ORM\OneToMany(
     *   targetEntity="CoreSys\UserBundle\Entity\MessageMetadata",
     *   mappedBy="message",
     *   cascade={"all"}
     * )
     * @var MessageMetadata
     */
    protected $metadata;

    public function hoursAgo()
    {
        $now = new \DateTime();
        $then = $this->getCreatedAt();

        $diff = $now->diff( $then );
        $minutes = $diff->format( '%i' );
        $hours = $diff->format( '%h' );

        if( $hours <= 0 && $minutes >= 0 ) {
            if( $minutes <= 5 ) {
                return 'Just Now';
            } else {
                return $minutes . ' Mins Ago';
            }
        } else {
            return $hours . ' Hours ago';
        }
    }

    public function excerpt( $len = 50 )
    {
        $len = intval( $len );
        $body = $this->getBody();
        return substr( $body, 0, $len );
    }
}