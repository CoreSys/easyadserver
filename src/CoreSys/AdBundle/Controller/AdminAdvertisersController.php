<?php

namespace CoreSys\AdBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CoreSys\SiteBundle\Controller\AdminController as BaseController;

/**
 * Class AdminAdvertisersController
 * @package CoreSys\AdBundle\Controller
 * @Route("/admin/ads/advertisers")
 */
class AdminAdvertisersController extends BaseController
{
    /**
     * @Route("/", name="admin_ads_advertisers_index", defaults={"slug"=null})
     * @Route("/{slug}", name="admin_ads_advertisers_slug", defaults={"slug"=null})
     * @Template()
     */
    public function indexAction( $slug )
    {
        if( !empty( $slug ) ) {
            return $this->redirect( $this->generateUrl( 'admin_ads_advertisers_manage', array( 'slug' => $slug ) ) );
        }
        return array();
    }

    /**
     * @Route("/manage/{slug}", name="admin_ads_advertisers_manage", defaults={"slug"=null}, options={"expose"=true})
     * @Template()
     */
    public function manageAction( $slug )
    {
        $repo = $this->getRepo( 'CoreSysAdBundle:Advertiser' );
        $advertiser = $repo->locateAdvertiser( $slug );

        if( empty( $advertiser ) ) {
            $this->msgError( 'Could not locate advertiser' );
            return $this->redirect( $this->generateUrl( 'admin_ads_advertisers_index' ) );
        }

        return array( 'advertiser' => $advertiser );
    }
}
