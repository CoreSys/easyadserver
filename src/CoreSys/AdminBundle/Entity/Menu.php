<?php

namespace CoreSys\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Menu
 *
 * @ORM\Table(name="admin_menu")
 * @ORM\Entity(repositoryClass="CoreSys\AdminBundle\Entity\MenuRepository")
 */
class Menu
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=12)
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;

    /**
     * @var string
     *
     * @ORM\Column(name="template", type="string", length=255)
     */
    private $template;

    /**
     * @var string
     *
     * @ORM\Column(name="bundle", type="string", length=128)
     */
    private $bundle;

    /**
     * @var string
     *
     * @ORM\Column(name="package", type="string", length=128, nullable=true)
     */
    private $package;

    public function __construct()
    {
        $this->setActive( TRUE );
        $this->setPosition( 0 );
    }

    /**
     * Get Package
     *
     * @return mixed
     */
    public function getPackage()
    {
        return $this->package;
    }

    /**
     * Set Package
     *
     * @param mixed $package
     *
     * @return Menu
     */
    public function setPackage( $package = NULL )
    {
        $this->package = $package;

        return $this;
    }

    /**
     * Get Bundle
     *
     * @return mixed
     */
    public function getBundle()
    {
        return $this->bundle;
    }

    /**
     * Set Bundle
     *
     * @param mixed $bundle
     *
     * @return Menu
     */
    public function setBundle( $bundle = NULL )
    {
        $this->bundle = $bundle;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Menu
     */
    public function setType( $type )
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return Menu
     */
    public function setPosition( $position )
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Menu
     */
    public function setActive( $active )
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get template
     *
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Set template
     *
     * @param string $template
     *
     * @return Menu
     */
    public function setTemplate( $template )
    {
        $this->template = $template;

        return $this;
    }
}