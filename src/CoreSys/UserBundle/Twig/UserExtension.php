<?php

    /**
     * This file is part of the CoreSysUserBundle package.
     * (c) J&L Core Systems http://jlcoresystems.com | http://joshmccreight.com
     */

    namespace CoreSys\UserBundle\Twig;

    use CoreSys\SiteBundle\Twig\BaseExtension;
    use CoreSys\UserBundle\Entity\User;

    /**
     * Common user extensions to be used in conjunction with
     * the CoreSys Bundles
     *
     * Class UserExtension
     * @package CoreSys\SiteBundle\Twig
     */
    class UserExtension extends BaseExtension
    {

        /**
         * @var string
         */
        protected $name = 'user_extension';

        /**
         * @var
         */
        private $provider;

        /**
         * @return array
         */
        public function getFilters()
        {
            return array();
        }

        /**
         * @return \CoreSys\SiteBundle\Twig\repo|\Doctrine\ORM\EntityRepository|null
         */
        public function getRoleRepo()
        {
            return $this->getRepo( 'CoreSysUserBundle:Role' );
        }

        /**
         * @return array
         */
        public function getFunctions()
        {
            return array(
                'getUser'             => new \Twig_Function_Method( $this, 'getUser' ),
                'userCount'           => new \Twig_Function_Method( $this, 'getUsersCount' ),
                'userMonthCount'      => new \Twig_Function_Method( $this, 'getUserCountByMonth' ),
                'userLastYearCounts'  => new \Twig_Function_Method( $this, 'getUserLastYearCounts' ),
                'inboxThreads'        => new \Twig_Function_Method( $this, 'getInboxThreads' ),
                'sentThreads'         => new \Twig_Function_Method( $this, 'getSentThreads' ),
                'getThread'           => new \Twig_Function_Method( $this, 'getThread' ),
                'unreadMessagesCount' => new \Twig_Function_Method( $this, 'getUnreadMessagesCount' ),
                'sentMessagesCount'   => new \Twig_Function_Method( $this, 'getSentMessagesCount' ),
                'hasRole'             => new \Twig_Function_Method( $this, 'hasRole' ),
                'getConfig'           => new \Twig_Function_Method( $this, 'getConfig' ),
                'userConfig'          => new \Twig_Function_Method( $this, 'getUserConfig' ),
                'userGravatar'        => new \Twig_Function_Method( $this, 'getUserGravatar' )
            );
        }

        public function getUserGravatar( $user, $size = 64 )
        {
            if( empty( $user ) ) {
                $user = $this->getRepo( 'CoreSysUserBundle:User' )->locateUser( $user );
            }
            if( empty( $user ) ) {
                return null;
            }

            $grav = $this->get( 'core_sys_user.gravatar' );
            $url = $grav->getUserGravatar( $user, $size );

            return $url;
        }

        /**
         * @param null $setting
         * @param null $default
         *
         * @return null
         */
        public function getUserConfig( $setting = NULL, $default = NULL )
        {
            $config   = $this->getConfig();
            $function = ucwords( str_replace( '_', ' ', $setting ) );
            $function = 'get' . trim( str_replace( ' ', '', $function ) );
            if ( method_exists( $config, $function ) ) {
                return $config->$function();
            }

            return $default;
        }

        /**
         * @return mixed
         */
        public function getConfig()
        {
            return $this->getRepo( 'CoreSysUserBundle:Config' )->getConfig();
        }

        /**
         * @param null $role
         *
         * @return bool
         */
        public function hasRole( $role = NULL )
        {
            if ( empty( $role ) ) {
                return FALSE;
            }

            $user = $this->getUser();

            if ( empty( $user ) ) {
                return FALSE;
            }

            return $user->hasRoleSysRole( $role );
        }

        /**
         * @param null $slug
         *
         * @return null
         */
        public function getUser( $slug = NULL )
        {
            if ( empty( $slug ) ) {
                return parent::getUser();
            } else if ( $slug instanceof User ) {
                return $slug;
            }

            $repo = $this->getRepo( 'CoreSysUserBundle:User' );

            return $repo->locateUser( $slug );
        }

        /**
         * @param null $user
         *
         * @return int
         */
        public function getSentMessagesCount( $user = NULL )
        {
//        return 0;
            $user    = $this->getUser( $user );
            $manager = $this->get( 'core_sys_user.message_manager' );
            $threads = $manager->getSentThreads( $user );

            return count( $threads );
        }

        /**
         * @param null $user
         *
         * @return mixed
         */
        public function getUnreadMessagesCount( $user = NULL )
        {
//        return 0;
            $user    = $this->getUser( $user );
            $manager = $this->get( 'core_sys_user.message_manager' );

            return $manager->unreadCount( $user );
        }

        /**
         * @param null $threadId
         *
         * @return mixed
         */
        public function getThread( $threadId = NULL )
        {
            return $this->getProvider()->getThread( $threadId );
        }

        /**
         * @return mixed
         */
        public function getProvider()
        {
            if ( !empty( $this->provider ) ) {
                return $this->provider;
            }

            $this->provider = $this->get( 'fos_message.provider' );

            return $this->provider;
        }

        /**
         * @return mixed
         */
        public function getInboxThreads()
        {
            return $this->getProvider()->getInboxThreads();
        }

        /**
         * @return mixed
         */
        public function getSentThreads()
        {
            return $this->getProvider()->getSentThreads();
        }

        /**
         * @param null $active
         *
         * @return mixed
         */
        public function getUsersCount( $active = NULL )
        {
            $repo = $this->getUserRepo();

            return $repo->getUsersCount( $active );
        }

        /**
         * @return \CoreSys\SiteBundle\Twig\repo|\Doctrine\ORM\EntityRepository|null
         */
        public function getUserRepo()
        {
            return $this->getRepo( 'CoreSysUserBundle:User' );
        }

        /**
         * @param string $delim
         *
         * @return string
         */
        public function getUserLastYearCounts( $delim = ',' )
        {
            $return = array();
            for ( $i = 11; $i >= 0; $i-- ) {
                $month     = date( 'm' ) - $i;
                $count     = $this->getUserCountByMonth( $month );
                $return[ ] = $count;
            }

            return implode( $delim, $return );
        }

        /**
         * get the user count for a specific month
         *
         * @param int month
         * @param int year
         *
         * @retrun int
         */
        public function getUserCountByMonth( $month = NULL, $year = NULL )
        {
            if ( empty( $month ) ) $month = date( 'm' );
            if ( empty( $year ) ) $year = date( 'y' );

            $manager = $this->get( 'core_sys_user.user_manager' );
            $date    = new \DateTime();
            $date->setTimestamp( mktime( 0, 1, 1, $month, 1, $year ) );

            return $manager->getUsersCountForMonth( $date );
        }
    }