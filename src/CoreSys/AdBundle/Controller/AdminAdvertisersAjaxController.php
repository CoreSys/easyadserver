<?php

namespace CoreSys\AdBundle\Controller;

use CoreSys\AdvertiserBundle\Form\AdvertiserType;
use CoreSys\SiteBundle\Entity\Address;
use CoreSys\UserBundle\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CoreSys\SiteBundle\Controller\AjaxController as BaseController;
use CoreSys\AdBundle\Entity\Advertiser;
use CoreSys\UserBundle\Entity\User;
use Symfony\Component\Form\Form;

/**
 * Class AdminAdvertisersAjaxController
 * @package CoreSys\AdBundle\Controller
 * @Route("/admin/ajax/ads/advertisers", options={"expose"=true})
 */
class AdminAdvertisersAjaxController extends BaseController
{
    private $tform;

    /**
     * @Route("/data_tables", name="admin_ajax_ads_advertisers_data_tables")
     * @Template()
     */
    public function dataTablesAction()
    {
        $repo = $this->getRepo( 'CoreSysAdBundle:Advertiser' );
        $data = $this->processDatatables();
        $results = $this->getDatatablesResults( $repo, $data );
        $results[ 'data' ] = $data;

        $this->echoJsonResponse( $results, true );
        exit;
    }

    /**
     * @Route("/new", name="admin_ajax_ads_advertisers_new", defaults={"id"="null"})
     * @Template()
     */
    public function formAction( $id )
    {
        $id   = intval( $id );
        $user_repo = $this->getRepo( 'CoreSysUserBundle:User' );
        $adv_repo = $this->getRepo( 'CoreSysAdBundle:Advertiser' );

        $advertiser = $adv_repo->findOneById( $id );

        if( empty( $advertiser ) ) {
            // we need to create a new user and add an advertiser entity as well
            $user = new User();
            $new = true;

            $roles_repo = $this->getRepo( 'CoreSysUserBundle:Role' );
            $role = $roles_repo->findOneByName( 'advertiser' );
            $user->addSysRole( $role );

            $advertiser = new Advertiser();
            $advertiser->setUser( $user );

            $action = $this->generateUrl( 'admin_ajax_ads_advertisers_save_new' );
            $title = 'Create new Advertiser';
        }

        $form = $this->createForm( new AdvertiserType(), $advertiser );

        return array( 'form' => $form->createView(), 'new' => $new, 'title' => $title, 'action' => $action );
    }

    /**
     * @Route("/edit/{id}", name="admin_ajax_ads_advertisers_edit", defaults={"id"="null"})
     * @Template()
     */
    public function editFormAction( $id )
    {
        $id   = intval( $id );
        $user_repo = $this->getRepo( 'CoreSysUserBundle:User' );
        $adv_repo = $this->getRepo( 'CoreSysAdBundle:Advertiser' );

        $advertiser = $adv_repo->findOneById( $id );

        $new = false;
        $user = $advertiser->getUser();

        $roles_repo = $this->getRepo( 'CoreSysUserBundle:Role' );
        $role = $roles_repo->findOneByName( 'advertiser' );
        $user->addSysRole( $role );

        $action = $this->generateUrl( 'admin_ajax_advertisers_save_advertiser', array( 'id' => $advertiser->getId() ));
        $title = 'Edit Advertiser';

        $form = $this->createForm( new AdvertiserType(), $advertiser );

        return array( 'form' => $form->createView(), 'new' => $new, 'title' => $title, 'action' => $action );
    }

    function l( $item )
    {
        foreach( $item->all() as $k => $v ) {
            if( $v instanceof ArrayAccess ) {
                $this->l( $v );
            }
            echo $k . ' = ' . $v->getName() . '<br>';
        }
    }

    /**
     * @Route("/save_new", name="admin_ajax_ads_advertisers_save_new")
     * @Template()
     */
    public function saveNewAdvertiserAction()
    {
        $manager = $this->get( 'fos_user.user_manager' );

        $user = $manager->createUser();

        $advertiser = new Advertiser();
        $advertiser->setUser( $user );
        $advertiser->setBillingAddress( new Address() );
        $advertiser->setShippingAddress( new Address() );

        $form2 = $this->createForm( new AdvertiserType(), $advertiser );

//        $this->l( $form2->getRoot() );
//        exit;

        $form2->handleRequest( $this->get( 'request' ) );


//        exit;

        if ( $form2->isValid() ) {
            $data     = $form2->getData();
            $user     = $data->getUser();
            $username = $user->getUsername();

            if ( empty( $username ) ) {
                $this->echoJsonError( 'Must enter a username.' );
                exit;
            }
            $email = $user->getEmail();
            if ( empty( $email ) ) {
                $this->echoJsonError( 'Must enter a valid email address.' );
                exit;
            }
            $plainPassword = $user->getPlainPassword();
            if ( empty( $plainPassword ) || strlen( $plainPassword ) < 6 ) {
                $this->echoJsonError( 'Must enter a password of at least 6 characters in length.' );
                exit;
            }
        }

        $result = $this->saveUserForm( $form2 );

        if ( $result instanceof Advertiser ) {
            $advertiser = $result;

            $this->persistAndFlush( $advertiser );

            $repo = $this->getRepo( 'CoreSysAdBundle:Advertiser' );

            $user_data = $repo->getDataTablesEntityDataRow( $advertiser );

            $this->echoJsonSuccess( 'Successfully saved user', $user_data );

            $this->echoJsonSuccess( 'Successfully added new user/advertiser', $user_data );
        }
        else {
            $view = $form2->createView();
            $this->echoJsonError( $result );
        }

        exit;
    }

    /**
     * @Route("/save_advertiser/{id}", name="admin_ajax_advertisers_save_advertiser", defaults={"id"="null"})
     * @ParamConverter("advertiser", class="CoreSysAdBundle:Advertiser")
     * @Template()
     */
    public function saveAdvertiserAction( $advertiser )
    {
        $user = $advertiser->getUser();

        $form = $this->createForm( new AdvertiserType(), $advertiser );

        $result = $this->saveUserForm( $form );

        if ( $result instanceof Advertiser ) {
            $advertiser = $result;

            $repo = $this->getRepo( 'CoreSysAdBundle:Advertiser' );

//            $advertiser = $repo->findOneBy( array( 'user' => $user ) );

            $user_data = $repo->getDataTablesEntityDataRow( $advertiser );

            $this->echoJsonSuccess( 'Successfully saved user', $user_data );
        }
        else {
            $this->echoJsonError( $result );
        }

        exit;
    }

    private function saveUserForm( &$form, User &$user = NULL )
    {
        $request = $this->get( 'request' );
        $form->handleRequest( $request );

        if ( $form->isValid() ) {
            $advertiser = $form->getData();

            if( method_exists( $advertiser, 'prepersist' ) ) {
                $advertiser->prepersist();
            }

            $user = $advertiser->getUser();
            $fname = $user->getFirstName();
            $lname = $user->getLastName();
            if( empty( $fname ) || empty( $lname ) ) {
                return 'Users First and Last Names are requred';
            }

            $billing = $advertiser->getBillingAddress();
            $shipping = $advertiser->getShippingAddress();
            $same = $advertiser->getShippingSame();

            if( !empty( $billing ) ) {
                $billing_errors = $billing->valid( false );
                if( $billing_errors !== true ) {
                    return $billing_errors;
                }
            }

            if( !$same && !empty( $shipping ) ) {
                $shipping_errors = $shipping->valid( false );
                if( $shipping_errors !== true ) {
                    return $shipping_errors;
                }
            }

            $manager = $this->get( 'fos_user.user_manager' );

            $roles_repo = $this->getRepo( 'CoreSysUserBundle:Role' );
            $role = $roles_repo->findOneByName( 'advertiser' );
            $user->addSysRole( $role );

            $manager->updateUser( $user );

            $this->persistAndFlush( $advertiser );

            return $advertiser;
        }
        else {
//            $form = new Form();
            $errors = array();
            foreach( $form->getErrors() as $key => $error ) {
                $errors[] = $error;
            }
            return implode( '<br>', $errors );
        }
    }

    /**
     * @Route("/non_advertiser_users.json", name="admin_ajax_ads_advertisers_non_advertiser_users_json", options={"expose"=true})
     * @Template()
     */
    public function getNonAdvertiserUsersAction(  )
    {
        header( 'Content-Type: application/json' );
        $repo = $this->getRepo( "CoreSysUserBundle:User" );
        $users = $repo->getUsersWithoutSysRole( 'advertiser' );

        $return = array();
        foreach( $users as $user )
        {
            $return[] = $user->getUsername();
        }

        echo json_encode( $return );
        exit;
    }

    /**
     * @Route("/convert_user/{username}", name="admin_ajax_ads_advertisers_convert_user", options={"expose"=true})
     * @Template()
     */
    public function convertUserAction( $username )
    {
        $repo = $this->getRepo( 'CoreSysUserBundle:User' );
        $user = $repo->findOneByUsername( $username );
        $role_name = "advertiser";

        $roles_repo = $this->getRepo( 'CoreSysUserBundle:Role' );
        $role = $roles_repo->findOneByName( $role_name );

        $advertiser_repo = $this->getRepo( 'CoreSysAdBundle:Advertiser' );

        if( !empty( $user ) ) {
            $advertiser = $advertiser_repo->findOneBy( array( 'user' => $user ) );
            if( empty( $advertiser ) ) {
                // we can add an advertiser entity for this user
                $advertiser = new Advertiser();
                $advertiser->setUser( $user );
                $this->persistAndFlush( $advertiser );

                $user->addSysRole( $role );
                $this->persistAndFlush( $user );

                $data = $repo->getDataTablesEntityDataRow( $advertiser );

                $this->echoJsonSuccess( 'Successfully converted ' . $username . ' to an advertiser.', $data );
            } else {
                $this->echoJsonError( 'This user is already an advertiser.' );
            }
        } else {
            $this->echoJsonError( 'Could not locate user.' );
        }
    }

    /**
     * @Route("/remove_advertiser/{id}", name="admin_ajax_ads_advertisers_remove_advertiser", options={"expose"=true})
     * @ParamConverter("advertiser", class="CoreSysAdBundle:Advertiser")
     * @Template()
     */
    public function removeAdvertiserAction( Advertiser $advertiser )
    {
        $roles_repo = $this->getRepo( 'CoreSysUserBundle:Role' );
        $role = $roles_repo->findOneByName( 'advertiser' );

        $user = $advertiser->getUser();
        $user->removeSysRole( $role );
        $this->persist( $user );

        $advertiser->setUser( null );
        $this->remove( $advertiser );
        $this->flush();

        $this->echoJsonSuccess( 'Successfully removed advertiser ' . $user->getUsername() );
    }

    /**
     * @Route("/view_advertiser/{id}", name="admin_ajax_ads_advertisers_view_advertiser", options={"expose"=true})
     * @ParamConverter("advertiser", class="CoreSysAdBundle:Advertiser")
     * @Template()
     */
    public function viewAdvertiserAction( Advertiser $advertiser )
    {
        return array( 'advertiser' => $advertiser );
    }
}
