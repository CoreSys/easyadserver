<?php

namespace CoreSys\SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AddressMinimalType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('companyName', null, array('required' => false, 'attr' => array('data-postdesc' => '*Optional - Company Name', 'placeholder' => 'Company Name')))
            ->add('address1', null, array('required' => true, 'attr' => array('data-postdesc' => '*Optional - Address Line 1', 'placeholder' => 'Address Line 1')))
            ->add('address2', null, array('required' => false, 'attr' => array('data-postdesc' => '*Optional - Address Line 2', 'placeholder' => 'Address Line 2')))
            ->add('city', null, array('required' => true, 'attr' => array('data-postdesc' => 'City', 'placeholder' => 'City')))
            ->add('state', null, array('required' => true, 'attr' => array('data-postdesc' => 'State/Province', 'placeholder' => 'State/Province')))
            ->add('country', 'country', array('required' => true, 'attr' => array('data-postdesc' => 'Country', 'placeholder' => 'Country', 'class' => 'form-control')))
            ->add('zipCode', null, array('required' => true, 'attr' => array('data-postdesc' => 'Zip/Postal Code', 'placeholder' => 'Zip/Postal Code')))
            ->add('homePhone', null, array('required' => false, 'attr' => array('data-postdesc' => '*Optional - Home Phone Number', 'placeholder' => '(xxx)xxx-xxxx')))
            ->add('homePhoneExtension', null, array('required' => false, 'label' => 'Ext', 'attr' => array('data-postdesc' => '*Ext', 'placeholder' => 'xxx')))
            ->add('workPhone', null, array('required' => false, 'attr' => array('data-postdesc' => '*Optional - Work Phone Number', 'placeholder' => '(xxx)xxx-xxxx')))
            ->add('workPhoneExtension', null, array('required' => false, 'label' => 'Ext', 'attr' => array('data-postdesc' => '*Ext', 'placeholder' => 'xxx')))
            ->add('cellPhone', null, array('required' => false, 'attr' => array('data-postdesc' => '*Optional - Home Phone Number', 'placeholder' => '(xxx)xxx-xxxx')));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CoreSys\SiteBundle\Entity\Address'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'coresys_sitebundle_address_minimal';
    }
}
