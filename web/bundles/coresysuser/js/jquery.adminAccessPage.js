;(function($,undefined){
    $.fn.adminAccessPage = function(options) {
        var p = this,
            defaults = {
                checkable: true,
                entity: 'Access',
                selected_count_selector: '.selected-count',
                check_master_selector: 'thead tr .master-check',
                check_row_selector: 'td .row-check',
                add_row_btn_selector: '.add-access',
                remove_rows_btn_selector: '.remove-accesses',
                table_selector: '#accesses-table',
                row_edit_btn_selector: '.btn-edit',
                row_remove_btn_selector: '.btn-remove',
                dt_display_length: 10,
                new_modal_selector: '#new-modal',
                edit_modal_selector: '#edit-modal',
                no_sort_columns: [0,8],
                no_show_columns: [0],
                remove_row_url: Routing.generate( 'admin_ajax_users_access_remove'),
                add_row_url: Routing.generate('admin_ajax_users_access_new'),
                edit_row_url: Routing.generate('admin_ajax_users_access_edit'),
                fnGetRowData: getRowData,
                fnPopulateEditedRowData: populateEditedRowData
            };

        p.settings = {};
        p.page = null;

        function init()
        {
            p.settings = $.extend({}, defaults, options );
            p.page = new $.fn.adminBasicTablePage(p.settings);
        }

        function getRowData(data) {
            var rowData = [];

            var an = '<i class="fa fa-' + ( data.anonymous ? 'checkgreen' : 'times red' ) + '"></i>',
                ac = '<i class="fa fa-' + ( data.active ? 'check green' : 'times red' ) + '"></i>',
                actions = '<a class="btn btn-xs btn-info btn-edit" href="javascript:void(0)" title="Edit ' + p.settings.entity + '"><i class="fa fa-edit"></i> Edit</a><a class="btn btn-xs btn-danger btn-remove" href="javascript:void(0)" title="Remove ' + p.settings.entity + '"><i class="fa fa-trash-o"></i> Remove</a>';

            if(data.roles === null) {
                data.roles = 'N/A';
            }

            // create the data row
            rowData[0] = '<input type="checkbox" data-id="' + data.id  + '" class="row-check" value="1">';
            rowData[1] = data.id;
            rowData[2] = data.path;
            rowData[3] = data.roles;
            rowData[4] = data.host;
            rowData[5] = data.ip;
            rowData[6] = an;
            rowData[7] = ac;
            rowData[8] = actions;

            return rowData;
        };

        function populateEditedRowData( row, data ) {
            var $row = $(row);

            if(data.roles === null) {
                data.roles = 'N/A';
            }

            if(data.host === null) {
                data.host = 'N/A';
            }

            if(data.ip === null) {
                data.ip = 'N/A';
            }

            if(data.roles === null) {
                data.roles = 'N/A';
            }

            $row.find('td').eq(2).html( data.path );
            $row.find('td').eq(3).html( data.roles );
            $row.find('td').eq(4).html( data.host );
            $row.find('td').eq(5).html( data.ip );

            var an = 'fa fa-check green',
                ac = 'fa fa-check green';
            if( !data.anonymous ) {
                an = 'fa fa-times red';
            }
            if( !data.active ) {
                ac = 'fa fa-times red';
            }
            $row.find('td').eq(6).html( '<i class="' + an + '"></i>' );
            $row.find('td').eq(7).html( '<i class="' + ac + '"></i>' );
        };

        init();

        return this;
    };
})(jQuery);