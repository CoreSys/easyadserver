<?php

namespace CoreSys\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Doctrine\ORM\EntityRepository;

/**
 * Class BaseAdminController
 * @package CoreSys\SiteBundle\Controller
 */
class AjaxController extends BaseController
{
    /**
     * @var bool
     */
    protected $ajax = true;

    protected function processDatatables()
    {
        $request = $this->get( 'request' );

        $sEcho = $request->get('sEcho', null);
        $sEcho = intval( $sEcho );

        $iColumns = $request->get('iColumns', null);
        $sColumns = $request->get('sColumns', null);
        $aColumns = array();

        foreach( explode( ',', $sColumns ) as $idx => $col ) {
            $col = trim( $col );
            $aColumns[ $idx ] = array(
                'id' => $col,
                'searchable' => false,
                'sortable' => !empty( $col ),
                'regex' => false
            );
        }

        $iDisplayStart = $request->get('iDisplayStart', null);
        $iDisplayStart = intval( $iDisplayStart );

        $iDisplayLength = $request->get('iDisplayLength', null);
        $iDisplayLength = intval( $iDisplayLength );

        $mDataProp_0 = $request->get('mDataProp_0', null);
        $mDataProp_1 = $request->get('mDataProp_1', null);
        $mDataProp_2 = $request->get('mDataProp_2', null);
        $mDataProp_3 = $request->get('mDataProp_3', null);
        $mDataProp_4 = $request->get('mDataProp_4', null);
        $mDataProp_5 = $request->get('mDataProp_5', null);

        $sSearch = $request->get('sSearch', null);
        $search = strtolower( trim( $sSearch ) );

        $bRegex = $request->get('bRegex', null);
        $bRegex = $bRegex == 'true' || $bRegex == 1 || $bRegex == '1' || $bRegex === true;

        for( $idx = 0; $idx < count( $aColumns ); $idx++ ) {
            $aSearch_a = $request->get( 'aSearch_' . $idx, false );
            $bSearchable_a = $request->get( 'bSearchable_' . $idx, false );
            $bRegex_a = $request->get( 'bRegex_' . $idx, false );
            $bRegex_a = $bRegex_a == 'true' || $bRegex_a == 1 || $bRegex_a == '1' || $bRegex_a === true;
            $bSearchable_a = $bSearchable_a == 'true' || $bSearchable_a == 1 || $bSearchable_a == '1' || $bSearchable_a === true;
            $aColumns[ $idx ][ 'regex' ] = $bRegex_a;
            $aColumns[ $idx ][ 'searchable' ] = $bSearchable_a;
            $aColumns[ $idx ][ 'search' ] = $aSearch_a;

            $aSortable_a = $request->get( 'bSortable_' . $idx, false );
            $aSortable_a = $aSortable_a == 1 || $aSortable_a == '1' || $aSortable_a == 'true' || $aSortable_a === true;
            $aColumns[ $idx ][ 'sortable' ] = $aSortable_a;
        }

        $iSortCol_0 = $request->get('iSortCol_0', null);
        $iSortCol_0 = intval( $iSortCol_0 );

        $sSortDir_0 = $request->get('sSortDir_0', null);
        $sSortDir_0 = strtolower( trim( $sSortDir_0 ) );
        $sSortDir_0 = $sSortDir_0 == 'asc' ? 'asc' : 'desc';

        $iSortingCols = $request->get('iSortingCols', null);

        if( $iSortCol_0 >= count( $aColumns ) ) {
            $iSortCol_0 = 0;
        }

        while( $iSortCol_0 < count( $aColumns ) && $aColumns[ $iSortCol_0 ][ 'sortable' ] === false )
        {
            $iSortCol_0++;
        }

        $data = array(
            'columns' => $aColumns,
            'sortColumn' => $iSortCol_0,
            'sortDir' => $sSortDir_0,
            'searchString' => $sSearch,
            'regex' => $bRegex,
            'echo' => $sEcho,
            'columnsCount' => count( $aColumns ),
            'offset' => $iDisplayStart,
            'limit' => $iDisplayLength
        );

        return $data;
    }

    protected function getDatatablesResults( EntityRepository $repo, $data, $return_results = false )
    {
        if( method_exists( $repo, 'getDatatablesResults')) {
            return $repo->getDatatablesResults( $data, $return_results );
        }

        $q = $repo->createQueryBuilder('e')
            ->setMaxResults( intval( $data[ 'limit' ] ) )
            ->setFirstResult( intval( $data[ 'offset' ] ) );

        if( !empty( $data[ 'searchString' ] ) ) {
            $search_query = array();
            foreach( $data[ 'columns' ] as $col ) {
                if( $col[ 'searchable' ] ) {
                    $search_query[] = 'e.' . $col[ 'id' ] . ' LIKE :search';
                }
            }
            $search_query = '(' . implode( ' OR ', $search_query ) . ')';
            $q->where( $search_query )
                ->setParameter( 'search', '%' . $data[ 'searchString' ] . '%' );
        }

        $sort_col = $data[ 'columns' ][ $data[ 'sortColumn' ] ][ 'id' ];
        $sort_dir = $data[ 'sortDir' ];

        $q->orderBy( 'e.' . $sort_col, $sort_dir );

        $q = $q->getQuery();

        $results = $q->getResult();
        if( $return_results ) {
            return $results;
        }

        $return = array(
            'sEcho' => $data[ 'echo' ],
            'iTotalRecords' => count( $results ),
            'iTotalDisplayRecords' => $this->getDatatablesAllCount( $repo ),
            'aaData' => array()
        );

        $return[ 'sort_col' ] = $sort_col;
        $return[ 'sort_dir' ] = $sort_dir;

        foreach( $results as $idx => $entity ) {
            if( !isset( $return[ 'aaData' ][ $idx ] ) ) {
                $return[ 'aaData' ][ $idx ] = array();
            }

            if( empty( $data[ 'columns' ][ 0 ][ 'id' ] ) ) {
                $return[ 'aaData' ][ $idx ][0] = null;
            }

            foreach( $entity->getAdminTableArray() as $k => $v ) {
                $return[ 'aaData' ][ $idx ][ $k ] = $v;
            }

            if( empty( $data[ 'columns' ][ count( $data[ 'columns' ] ) - 1 ][ 'id'] ) )  {
                $return[ 'aaData' ][ $idx ][ count( $data[ 'columns' ]) -1] = null;
            }

            $class = get_class( $entity );
            $class = explode( DIRECTORY_SEPARATOR, $class );
            $class = array_pop( $class );
            $class = explode( '/', $class );
            $class = array_pop( $class );

            $id = $entity->getId();

            $actions = array(
                '<li class="view-row"><a href="javascript:clickViewRow(' . $id . ');"><icon class="glyphicon glyphicon-eye-open" title="View ' . $class . '"></i>&nbsp;View ' . $class . '</a>',
                '<li class="edit-row"><a href="javascript:clickEditRow(' . $id . ');"><icon class="glyphicon glyphicon-edit" title="Edit ' . $class . '"></i>&nbsp;Edit ' . $class . '</a>',
                '<li class="remove-row"><a href="javascript:clickRemoveRow(' . $id . ');"><icon class="glyphicon glyphicon-minus-sign" title="Remove ' . $class . '"></i>&nbsp;Remove ' . $class . '</a>'
            );

            $actions = '<div class="btn-group"><button class="btn btn-xs dropdown-toggle" type="button" data-toggle="dropdown">Actions <i class="fa fa-angle-down"></i></button><ul class="dropdown-menu pull-right" role="menu">' . implode( $actions ) . '</ul></div>';
            $return[ 'aaData' ][ $idx ][ 'actions' ] = $actions;
        }

        var_dump( $return[ 'aaData' ] );

        return $return;
    }

    protected function getDatatablesAllCount( EntityRepository $repo )
    {
        if( method_exists( $repo, 'getDatatablesAllCount' )) {
            return $repo->getDatatablesAllCount();
        }

        $q = $repo->createQueryBuilder( 'e' )
            ->select( 'COUNT(e.id)')
            ->getQuery();

        return $q->getSingleScalarResult();
    }
}
