<?php
/**
 * This file is part of the CoreSysUserBundle package.
 * (c) J&L Core Systems http://jlcoresystems.com | http://joshmccreight.com
 */

namespace CoreSys\UserBundle\Controller;

use CoreSys\UserBundle\Entity\Thread;
use CoreSys\UserBundle\Form\ConfigType;
use CoreSys\UserBundle\Form\ThreadType;
use CoreSys\UserBundle\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CoreSys\SiteBundle\Controller\BaseController;
use CoreSys\UserBundle\CoreSysUserEvents;
use CoreSys\UserBundle\Event\UserEvent;
use CoreSys\UserBundle\Entity\User;

/**
 * Class AdminController
 * @package CoreSys\UserBundle\Controller
 * @Route("/admin/users")
 */
class AdminController extends BaseController
{

    /**
     * @Route("/", name="admin_users_index")
     * @Template()
     */
    public function indexAction()
    {
        $user       = new User();
        $dispatcher = $this->get( 'event_dispatcher' );
        $event      = new UserEvent( $user, $this->get( 'request' ) );
        $dispatcher->dispatch( CoreSysUserEvents::ADMIN_VIEW_USER_INDEX, $event );

        return array();
    }

    /**
     * @Route("/config", name="admin_user_configuration")
     * @Template()
     */
    public function configAction()
    {
        $config = $this->getRepo( 'CoreSysUserBundle:Config' )->getConfig();
        $form = $this->createForm( new ConfigType(), $config );
        $request = $this->get( 'request' );
        if($request->isMethod('post')) {
            $form->handleRequest( $request );
            if($form->isValid()) {
                $config = $form->getData();
                $this->persistAndFlush($config);
                $this->msgSuccess( 'Successfully saved user configuration' );
                return $this->redirect( $this->generateUrl( 'admin_user_configuration' ) );
            } else {
                $this->msgError( 'Could not save user configuration' );
            }
        }

        return array( 'config' => $config, 'form' => $form->createView() );
    }

    /**
     * @Route("/not_found", name="admin_users_user_not_found")
     * @Template()
     */
    public function userNotFoundAction()
    {
        return array();
    }

    /**
     * @Route("/{id}", name="admin_users_user", defaults={"id"="null"})
     * @Template()
     */
    public function viewUserAction( $id )
    {
        $repo = $this->getRepo( 'CoreSysUserBundle:User' );
        $user = $repo->findOneById( $id );
        if ( empty( $user ) ) {
            $user = $repo->findOneByUsername( $id );
        }
        if ( empty( $user ) ) {
            return $this->forward( 'CoreSysUserBundle:Admin:userNotFound' );
        }

        $extra_view = NULL;

        $event = new UserEvent( $user, $this->get( 'request' ) );
        $this->dispatchEvent( $event, CoreSysUserEvents::ADMIN_VIEW_USER_VIEW );

        if ( $event->hasData() ) {
            $extra_view = $event->getViewStats();
        }


        return array( 'user' => $user, 'view_hook' => $extra_view );
    }

    /**
     * @Route("/view/{slug}", name="admin_users_view_user", defaults={"slug"=null}, options={"expose"=true})
     * @Template()
     */
    public function viewAction( $slug )
    {
        $repo = $this->getRepo( 'CoreSysUserBundle:User' );
        $user = $repo->locateUser( $slug );
        $manager = $this->get( 'fos_user.user_manager' );
        $mmanager = $this->get( 'core_sys_user.message_manager');

        if( empty( $user ) ) {
            return $this->redirect( $this->generateUrl( 'admin_users_user_not_found' ) );
        }

        $form = $this->createForm( new UserType(), $user );
        $request = $this->get( 'request' );
        if( $request->isMethod( 'POST' ) ) {
            $form->handleRequest( $request );
            if( $form->isValid() ) {
                $user = $form->getData();
                $manager->updateUser( $user );
                $this->msgSuccess( 'Successfully updated user information' );
                return $this->redirect( $this->generateUrl( 'admin_users_view_user', array( 'slug' => $user->getUsername() ) ) );
            } else {
                $this->msgError( 'Could not save user information' );
            }
        }

        $mform = $mmanager->getAdminNewThreadForm();

        return array( 'user' => $user, 'form' => $form->createView(), 'mform' => $mform->createView() );
    }
}
