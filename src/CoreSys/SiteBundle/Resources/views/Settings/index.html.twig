{% extends 'CoreSysThemeBundle::admin.html.twig' %}

{% block head_title %}
    System Settings
{% endblock head_title %}

{% block page_title %}
    System Settings
    <small>Site specific settings</small>
{% endblock page_title %}

{% block page_breadcrumbs %}
    <li>
        <a href="{{ path( 'admin_site_settings' ) }}">System Settings</a>
    </li>
{% endblock page_breadcrumbs %}

{% block head_page_level_css %}
    {{ parent() }}
    <link href="{{ plugin_asset( 'fancybox/source/jquery.fancybox.css' ) }}" rel="stylesheet"/>
    <link href="{{ plugin_asset( 'jquery-file-upload/css/jquery.fileupload-ui.css' ) }}" rel="stylesheet"/>
    <noscript><link rel="stylesheet" href="{{ plugin_asset( 'jquery-file-upload/css/jquery.fileupload-ui-noscript.css' ) }}"></noscript>
    <link href="{{ admin_theme_asset( 'css/pages/portfolio.css' ) }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset( 'bundles/coresyssite/css/adminSettings.css' ) }}" rel="stylesheet" type="text/css"/>

    <!-- BEGIN PLUGINS USED BY X-EDITABLE -->
    <link rel="stylesheet" type="text/css" href="{{ plugin_asset( 'select2/select2_metro.css' ) }}"/>
    <link rel="stylesheet" type="text/css" href="{{ plugin_asset( 'bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css' ) }}"/>
    <!-- END PLUGINS USED BY X-EDITABLE -->
{% endblock head_page_level_css %}

{% block page_level_js %}
    {{ parent() }}
    <script src="{{ plugin_asset( 'fancybox/source/jquery.fancybox.pack.js' ) }}"></script>

    {% include 'CoreSysMediaBundle:Default:fileUploadJsIncludes.html.twig' %}

    <script type="text/javascript" src="{{ plugin_asset( 'Isotope/isotope.min.js' ) }}"></script>

    <script type="text/javascript" src="{{ plugin_asset( 'select2/select2.min.js' ) }}"></script>
    <script type="text/javascript" src="{{ plugin_asset( 'bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js' ) }}"></script>

    <!-- BEGIN CORE PLUGINS -->
    <!--[if lt IE 9]>
    <script src="{{ plugin_asset( 'respond.min.js' ) }}"></script>
    <script src="{{ plugin_asset( 'excanvas.min.js' ) }}"></script>
    <![endif]-->
{% endblock page_level_js %}

{% block content %}
    <div class="row">
        <div class="col-md-12">
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i>System Settings
                    </div>
                    <div class="actions">
                        <a href="javascript:void(0)" class="btn green btn-sm save-settings display-hide"><i class="fa fa-check"></i> Save</a>
                    </div>
                </div>
                <div class="portlet-body">
                    {{ form_start( form, {'attr': {'id':'settings-form'}} ) }}
                    <div class="alert alert-danger display-hide">
                        <button class="close" data-close="alert"></button>
                        You have some form errors. Please check below.
                    </div>
                    <div class="tabbable-custom">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#gen" data-toggle="tab">General</a>
                            </li>
                            <li class="">
                                <a href="#tos" data-toggle="tab">Terms of Service</a>
                            </li>
                            <li class="">
                                <a href="#priv" data-toggle="tab">Privacy Policy</a>
                            </li>
                            <li class="">
                                <a href="#con" data-toggle="tab">Contacts</a>
                            </li>
                            <li class="">
                                <a href="#lag" data-toggle="tab">Logos &amp Graphics</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="gen">
                                {% include 'CoreSysSiteBundle:Settings:gen_tab.html.twig' %}
                            </div>
                            <div class="tab-pane" id="tos">
                                {% include 'CoreSysSiteBundle:Settings:tos_tab.html.twig' %}
                            </div>
                            <div class="tab-pane" id="priv">
                                {% include 'CoreSysSiteBundle:Settings:priv_tab.html.twig' %}
                            </div>
                            <div class="tab-pane" id="con">
                                {% include 'CoreSysSiteBundle:Settings:con_tab.html.twig' %}
                            </div>
                            <div class="tab-pane" id="lag">
                                {% include 'CoreSysSiteBundle:Settings:lag_tab.html.twig' %}
                            </div>
                        </div>
                    </div>
                    <div class="hidden" style="display:none">
                        {{ form_rest( form ) }}
                    </div>
                    {{ form_end( form ) }}
                </div>
            </div>
        </div>
    </div>
{% endblock content %}

{% block body_js %}
    {% include 'CoreSysMediaBundle:Default:fileUploadModal.html.twig' %}
    {% include 'CoreSysMediaBundle:Default:imageModalInfo.html.twig' with {'show_inner':false} %}
    <script src="{{ asset( 'bundles/coresysmedia/js/fileUpload.js') }}" type="application/javascript"></script>
    <script src="{{ asset( 'bundles/coresyssite/js/adminSettings.js') }}" type="application/javascript"></script>
    <script>
        $(document).ready(function(){
            var page = new $.fn.settingsPage();
        });
    </script>
{% endblock body_js %}