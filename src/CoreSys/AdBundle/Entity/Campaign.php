<?php

namespace CoreSys\AdBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CoreSys\CampaignBundle\Entity\Campaign as BaseCampaign;
use Doctrine\Common\Collections\ArrayCollection;
use CoreSys\AdBundle\Entity\Parameter;
use CoreSys\AdBundle\Entity\Advertiser;

/**
 * Campaign
 *
 * @ORM\Table(name="ad_campaign")
 * @ORM\Entity(repositoryClass="CoreSys\AdBundle\Entity\CampaignRepository")
 */
class Campaign extends BaseCampaign
{
    /**
     * @var Advertiser
     *
     * @ORM\ManyToOne(targetEntity="CoreSys\AdBundle\Entity\Advertiser", inversedBy="campaigns")
     * @ORM\JoinColumn(name="advertiser_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $advertiser;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Ad", mappedBy="campaign")
     */
    private $ads;
    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Parameter")
     * @ORM\JoinTable(name="ad_campaign_parameters",
     *      joinColumns={@ORM\JoinColumn(name="cid", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="pid", referencedColumnName="id")})
     */
    private $params;
    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Website", mappedBy="Campaign")
     */
    private $websites;
    /**
     * @var int
     *
     * @ORM\Column(name="conversions_count", type="integer", nullable=true)
     */
    private $conversions_count;
    /**
     * @var string
     *
     * @ORM\Column(name="pricing_model", type="string", length=32)
     */
    private $pricing_model;
    /**
     * @var float
     *
     * @ORM\Column(name="pricing_rate", type="float", nullable=true)
     */
    private $pricing_rate;
    /**
     * @var int
     *
     * @ORM\Column(name="pricing_impressions", type="integer", nullable=true)
     */
    private $pricing_impressions;
    /**
     * @var int
     *
     * @ORM\Column(name="priority", type="integer", nullable=true)
     */
    private $priority;
    /**
     * @var int
     *
     * @ORM\Column(name="weight", type="integer", nullable=true)
     */
    private $weight;

    /**
     * Construct a new instance of this campaign
     */
    public function __construct()
    {
        parent::__construct();
        $this->setAds(new ArrayCollection());
        $this->setConversionsCount(0);
        $this->setPricingImpressions(-1);
        $this->setPricingModel('cpm');
        $this->setPricingImpressions(-1);
        $this->setPriority(0);
        $this->setWeight(0);
    }

    /**
     * @param Website $website
     * @return $this
     */
    public function addWebsite(Website $website)
    {
        if (!$this->websites->contains($website)) {
            $this->websites->add($website);
        }
        return $this;
    }

    /**
     * @param Website $website
     * @return $this
     */
    public function removeWebsite(Website $website)
    {
        if ($this->websites->contains($website)) {
            $this->websites->removeElement($website);
        }
        return $this;
    }

    /**
     * @param Ad $ad
     * @return $this
     */
    public function addAd(Ad $ad)
    {
        if (!$this->ads->contains($ad)) {
            $this->ads->add($ad);
        }
        return $this;
    }

    /**
     * @param Ad $ad
     * @return $this
     */
    public function removeAd(Ad $ad)
    {
        if ($this->ads->contains($ad)) {
            $this->ads->removeElement($ad);
        }
        return $this;
    }

    /**
     * Get the number of ads in this campaign
     *
     * @return int
     */
    public function getAdsCount()
    {
        return count($this->getAds());
    }

    /**
     * @return ArrayCollection
     */
    public function getAds()
    {
        return $this->ads;
    }

    /**
     * @param $ads
     * @return $this
     */
    public function setAds($ads)
    {
        $this->ads = $ads;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param $params
     * @return $this
     */
    public function setParams($params)
    {
        $this->params = $params;
        return $this;
    }

    /**
     * @param Parameter $param
     * @return $this
     */
    public function addParam(Parameter $param)
    {
        if (!$this->params->contains($param)) {
            $this->params->add($param);
        }
        return $this;
    }

    /**
     * @param Parameter $param
     * @return $this
     */
    public function removeParam(Parameter $param)
    {
        if ($this->params->contains($param)) {
            $this->params->removeElement($param);
        }
        return $this;
    }

    /**
     * @return int
     */
    public function getConversionsCount()
    {
        return intval($this->conversions_count);
    }

    /**
     * @param int $conversions_count
     */
    public function setConversionsCount($conversions_count = 0)
    {
        $this->conversions_count = intval($conversions_count);
    }

    /**
     * @return int
     */
    public function getPricingImpressions()
    {
        return intval($this->pricing_impressions);
    }

    /**
     * @param int $pricing_impressions
     */
    public function setPricingImpressions($pricing_impressions)
    {
        $this->pricing_impressions = intval($pricing_impressions);
    }

    /**
     * @return string
     */
    public function getPricingModel()
    {
        return $this->pricing_model;
    }

    /**
     * @param string $pricing_model
     */
    public function setPricingModel($pricing_model = null)
    {
        $this->pricing_model = $pricing_model;
    }

    /**
     * @return float
     */
    public function getPricingRate()
    {
        return floatval($this->pricing_rate);
    }

    /**
     * @param float $pricing_rate
     */
    public function setPricingRate($pricing_rate = 0)
    {
        $this->pricing_rate = floatval($pricing_rate);
    }

    /**
     * @return int
     */
    public function getPriority()
    {
        return intval($this->priority);
    }

    /**
     * @param int $priority
     */
    public function setPriority($priority = 0)
    {
        $this->priority = intval($priority);
    }

    /**
     * @return int
     */
    public function getWeight()
    {
        return intval($this->weight);
    }

    /**
     * @param int $weight
     */
    public function setWeight($weight = 0)
    {
        $this->weight = intval($weight);
    }

    /**
     * @return ArrayCollection
     */
    public function getAllowedWebsites()
    {
        $websites = new ArrayCollection();
        foreach ($this->getWebsites() as $website) {
            if ($website->getAllowed()) {
                $websites->add($website);
            }
        }
        return $websites;
    }

    /**
     * @return ArrayCollection
     */
    public function getWebsites()
    {
        return $this->websites;
    }

    /**
     * Get all allowed websites
     *
     * @param $websites
     * @return $this
     */
    public function setWebsites($websites)
    {
        $this->websites = $websites;
        return $this;
    }

    /**
     * Get all disallowed websites
     *
     * @return ArrayCollection
     */
    public function getDisAllowedWebsites()
    {
        $websites = new ArrayCollection();
        foreach ($this->getWebsites() as $website) {
            if (!$website->getAllowed()) {
                $websites->add($website);
            }
        }
        return $websites;
    }
}
