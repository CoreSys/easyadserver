<?php

namespace CoreSys\SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AddressFullType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', null, array('required' => true))
            ->add('lastName', null, array('required' => true))
            ->add('companyName', null, array('required' => true))
            ->add('address1', null, array('required' => true))
            ->add('address2', null, array('required' => true))
            ->add('city', null, array('required' => true))
            ->add('state', null, array('required' => true))
            ->add('country', null, array('required' => true))
            ->add('zipCode', null, array('required' => true))
            ->add('emailAddress', null, array('required' => true))
            ->add('homePhone', null, array('required' => true))
            ->add('homePhoneExtension', null, array('required' => true))
            ->add('workPhone', null, array('required' => true))
            ->add('workPhoneExtension', null, array('required' => true))
            ->add('cellPhone', null, array('required' => true))
            ->add('cellPhoneExtension', null, array('required' => true))
            ->add('type', null, array('required' => true))
            ->add('active', 'checkbox', array('required' => false));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CoreSys\SiteBundle\Entity\Address'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'coresys_sitebundle_address_full';
    }
}
