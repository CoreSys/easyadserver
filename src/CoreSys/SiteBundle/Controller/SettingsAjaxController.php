<?php

namespace CoreSys\SiteBundle\Controller;

use CoreSys\MediaBundle\Entity\Image;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Doctrine\ORM\EntityRepository;

/**
 * Class BaseAdminController
 * @package CoreSys\SiteBundle\Controller
 * @Route("/admin/settings/ajax", options={"expose"=true})
 */
class SettingsAjaxController extends AjaxController
{
    /**
     * @Route("/process_image", name="admin_settings_ajax_process_image")
     * @Template()
     */
    public function processImageAction()
    {
        $request = $this->get( 'request' );
        $filename = $request->get( 'filename', $request->get( 'file', null ) );
        if( empty( $filename ) ) {
            $this->echoJsonError( 'Could not determine filename' );
            exit;
        }

        $filename = strtolower( $filename );
        $filename = preg_replace( '/[^A-Za-z0-9\.-]+/', '', $filename );

        $manager = $this->get( 'core_sys_media.image_manager' );
        $result = $manager->processUploadedFile( $filename );

        if( $result instanceof Image ) {

            $config = $this->getSiteSettings();
            $config->addImage( $result );
            $this->persist( $result );
            $this->persist( $config );
            $this->flush();

            $data = $result->toArray();

            $this->echoJsonSuccess( 'success', $data );

        } elseif( $result === false ) {
            $this->echoJsonError( 'Unknown error' );
        } else {
            $this->echoJsonError( 'error', $result );
        }

        exit;
    }

    /**
     * @Route("/removeSiteImage/{id}", name="admin_site_settings_ajax_remove_image", defaults={"id"=null})
     * @ParamConverter("image", class="CoreSysMediaBundle:Image")
     * @Template()
     */
    public function removeImageAction( Image $image )
    {
        $data = $image->toArray();

        $settings = $this->getSiteSettings();
        $settings->removeImage( $image );
        $this->persistAndFlush( $settings );

        $manager = $this->get( 'core_sys_media.image_manager' );
        $manager->removeImage( $image );

        $this->remove( $image );
        $this->flush();

        $this->echoJsonSuccess( 'success', $data );
        exit;
    }

    /**
     * @todo create edit option for images, such that we can edit the slug and title
     * @todo create ability to sort, reorder, and remove images from settings
     * @todo have the autoupload, on successfull image processing, add the image to the final listings table
     * @todo implement the site gallery list
     * @todo have a way to display to the user what code to inset in order to user the site image
     */
}
