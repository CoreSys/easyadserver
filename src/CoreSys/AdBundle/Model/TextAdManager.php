<?php

namespace CoreSys\AdBundle\Model;

use CoreSys\SiteBundle\Model\BaseManager;
use CoreSys\AdBundle\Model\BaseAdManager;

class TextAdManager extends BaseAdManager
{
    protected $ad_type = 'text';
}