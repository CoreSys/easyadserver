<?php

namespace CoreSys\CampaignBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CoreSys\SiteBundle\Controller\AdminController as BaseController;

/**
 * Class AdminController
 * @package CoreSys\CampaignBundle\Controller
 * @Route("/admin/campaigns")
 */
class AdminController extends BaseController
{
    /**
     * @Route("/", name="admin_campaigns_index")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }
}
