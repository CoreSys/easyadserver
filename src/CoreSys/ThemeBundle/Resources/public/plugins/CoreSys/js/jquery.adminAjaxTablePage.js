$.fn.dataTableExt.oApi.fnReloadAjax = function ( oSettings, sNewSource, fnCallback, bStandingRedraw )
{
    // DataTables 1.10 compatibility - if 1.10 then versionCheck exists.
    // 1.10s API has ajax reloading built in, so we use those abilities
    // directly.
    if ( $.fn.dataTable.versionCheck ) {
        var api = new $.fn.dataTable.Api( oSettings );

        if ( sNewSource ) {
            api.ajax.url( sNewSource ).load( fnCallback, !bStandingRedraw );
        }
        else {
            api.ajax.reload( fnCallback, !bStandingRedraw );
        }
        return;
    }

    if ( sNewSource !== undefined && sNewSource !== null ) {
        oSettings.sAjaxSource = sNewSource;
    }

    // Server-side processing should just call fnDraw
    if ( oSettings.oFeatures.bServerSide ) {
        this.fnDraw();
        return;
    }

    this.oApi._fnProcessingDisplay( oSettings, true );
    var that = this;
    var iStart = oSettings._iDisplayStart;
    var aData = [];

    this.oApi._fnServerParams( oSettings, aData );

    oSettings.fnServerData.call( oSettings.oInstance, oSettings.sAjaxSource, aData, function(json) {
        /* Clear the old information from the table */
        that.oApi._fnClearTable( oSettings );

        /* Got the data - add it to the table */
        var aData =  (oSettings.sAjaxDataProp !== "") ?
            that.oApi._fnGetObjectDataFn( oSettings.sAjaxDataProp )( json ) : json;

        for ( var i=0 ; i<aData.length ; i++ )
        {
            that.oApi._fnAddData( oSettings, aData[i] );
        }

        oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();

        that.fnDraw();

        if ( bStandingRedraw === true )
        {
            oSettings._iDisplayStart = iStart;
            that.oApi._fnCalculateEnd( oSettings );
            that.fnDraw( false );
        }

        that.oApi._fnProcessingDisplay( oSettings, false );

        /* Callback user function - for event handlers etc */
        if ( typeof fnCallback == 'function' && fnCallback !== null )
        {
            fnCallback( oSettings );
        }
    }, oSettings );
};

;(function ($, undefined) {
    $.fn.adminAjaxTablePage = function (options) {
        var t = this,
            $this = $(this),
            defaults = {
                checkable: true,
                entity: 'Role',
                selected_count_selector: '.selected-count',
                check_master_selector: 'thead tr .master-check',
                check_row_selector: 'td .row-check',
                add_row_btn_selector: '.add-role',
                remove_rows_btn_selector: '.remove-roles',
                onToggleRemoveRowsBtn: null,
                table_selector: '#roles-table',
                row_edit_btn_selector: '.btn-edit',
                row_remove_btn_selector: '.btn-remove',
                row_view_btn_selector: '.btn-view',
                row_manage_btn_selector: '.btn-manage',
                dt_display_length: 10,
                new_modal_selector: '#new-modal',
                edit_modal_selector: '#edit-modal',
                view_modal_selector: '#view-modal',
                remove_row_url: Routing.generate( 'admin_ajax_users_role_remove'),
                add_row_url: Routing.generate('admin_ajax_users_roles_new'),
                edit_row_url: Routing.generate('admin_ajax_users_roles_edit'),
                manage_row_url: null,
                view_row_url: null,
                no_sort_columns: [0],
                dataTables_aoColumns: [],
                aSort: [],
                dataTables_ajax_url: null,
                fnGetRowData: null,
                fnPopulateEditedRowData: null,
                dataTables_aoColumnFilter: [],
                no_rows_template: '<div class="well"><div class="row"><div class="col-md-12 no-rows text-center"><h3>There are currently no Access Rights</h3></div></div></div>',
                onViewBtnClick: null,
                onDrawCallback: null
            };

        t.settings = {};
        t.addRowBtn = null;
        t.removeRowsBtn = null;
        t.table = null;
        t.datatable = null;
        t.checkMaster = null;
        t.newModal = null;
        t.editModal = null;
        t.viewModal = null;
        t.responsiveHelper = null;

        t.getDataTable = function()
        {
            return t.datatable;
        }

        function init() {
            t.settings = $.extend({}, defaults, options);
            
            t.addRowBtn = $(t.settings.add_row_btn_selector);
            t.addRowBtn.click(addNewRowClick)
            t.removeRowsBtn = $(t.settings.remove_rows_btn_selector);
            t.removeRowsBtn.hide();
            t.removeRowsBtn.removeClass('display-hide');
            t.removeRowsBtn.click(removeRowsClick);
            t.newModal = $(t.settings.new_modal_selector);
            t.editModal = $(t.settings.edit_modal_selector);
            t.viewModal = $(t.settings.view_modal_selector);

            initTable();
            postInitTable();

            initCheckable();
            t.checkSelectedRows(true);
            resetRowOptions();

            initDatatables();

            $('body').on('hidden.bs.modal', '.modal', function () {
                $(this).removeData('bs.modal');
                clearModals();
            });

            checkRowCounts();
        }

        function clearModals()
        {
            $(t.settings.new_modal_selector).children().remove();
            $(t.settings.new_modal_selector).html('');
            $(t.settings.edit_modal_selector).children().remove();
            $(t.settings.edit_modal_selector).html('');
            $(t.settings.view_modal_selector).children().remove();
            $(t.settings.view_modal_selector).html('');
        }

        function resetRowOptions() {
            detectResponsiveExpanderClick();
            registerRowEditBtnClicks();
            registerRowRemoveBtnClicks();
            registerRowViewBtnClicks();
            registerRowManageBtnClicks();
            registerCheckboxUniforms();
        }

        function detectResponsiveExpanderClick()
        {
            t.table.find('tbody tr').each(function(){
                var ex = $(this).find('.responsiveExpander');
                if(ex.length > 0 && ex !== undefined && ex !== null) {
                    ex.click(function(){
                        setTimeout(function(){
                            resetRowOptions();
                        },250);
                    });
                }
            });
        }

        function registerCheckboxUniforms()
        {
            if(t.settings.checkable) {
                $(':checkbox').not('.toggle').each(function(){
                    var $this = $(this);
                    if($.fn.uniform) {
                        $this.uniform();
                    }
                });
            }
        }

        function registerRowEditBtnClicks() {
            t.table.find('tbody').find('tr').each(function () {
                t.getRowDataId( $(this) );
                var $btn = $(this).find(t.settings.row_edit_btn_selector);
                registerRowEditBtnClick($btn);
            });
        }

        function checkRowCounts()
        {
            var rows = t.table.find('tbody').find('tr'),
                count = rows.length;

            if(count > 0) {
                $('.no-rows').remove();
            }
        }

        function registerRowEditBtnClick($btn) {
            if ($btn) {
                var id = t.getRowDataId( $btn.closest('tr') );
                $btn.unbind('click');
                $btn.click(function (e) {
                    e.preventDefault();
                    var id = t.getRowDataId( $btn.closest('tr') );
                    t.editRow(id);
                });
            }
        }

        function registerRowRemoveBtnClicks() {
            t.table.find('tbody').find('tr').each(function () {
                t.getRowDataId( $(this) );
                var $btn = $(this).find(t.settings.row_remove_btn_selector);
                registerRowRemoveBtnClick($btn);
            });
        }

        function registerRowViewBtnClicks() {
            t.table.find('tbody').find('tr').each(function(){
                t.getRowDataId( $(this) );
                var $btn = $(this).find(t.settings.row_view_btn_selector);
                registerRowViewBtnClick($btn);
            });
        }

        function registerRowRemoveBtnClick($btn) {
            if ($btn) {
                $btn.unbind('click');
                $btn.click(function (e) {
                    e.preventDefault();
                    var id = t.getRowDataId( $btn.closest('tr') );
                    var msg = t.settings.remove_message || 'Are you sure you want to remove this ' + t.settings.entity + '?';
                    bootbox.confirm( msg, function(res){
                        if(res) {
                            removeRow(id);
                        }
                    });
                });
            }
        }

        function registerRowManageBtnClicks()
        {
            t.table.find('tbody').find('tr').each(function(){
                t.getRowDataId( $(this) );
                var $btn = $(this).find(t.settings.row_manage_btn_selector);
                registerRowManageBtnClick($btn);
            });
        }

        function registerRowManageBtnClick($btn)
        {
            if($btn) {
                $btn.unbind('click');
                $btn.click(function(e){
                    e.preventDefault();
                    var id = t.getRowDataId( $btn.closest( 'tr' ) );
                    window.location = t.settings.manage_row_url + '/' + id;
                });
            }
        }

        function registerRowViewBtnClick($btn) {
            if( $btn ) {
                $btn.unbind('click');
                $btn.click(function(e){
                    e.preventDefault();
                    var id = t.getRowDataId( $btn.closest('tr') );
                    if(typeof t.settings.onViewBtnClick == 'function') {
                        if(!t.settings.onViewBtnClick(e,$btn,id)) {
                            return;
                        }
                    }
                    t.viewRow( id );
                });
            }
        }

        function removeRow( id ) {
            $.ajax({
                url: t.settings.remove_row_url + '/' + id,
                data: {'id':id},
                type: 'POST',
                success:function(res) {
                    if(typeof res != 'object' ) res = JSON.parse(res);
                    if(res.success) {
                        toastr['success']('Successfully removed ' + t.settings.entity, 'Removed');
                        var $row = t.getRowById( id),
                            row = $row[0];

                        t.datatable.fnDeleteRow(row);

                        resetRowOptions();
                        t.forceUncheckAll()

                        checkRowCounts();
                    } else {
                        toastr['error'](res.msg, 'Error');
                    }
                }
            });
        }

        function initCheckable() {
            if (t.settings.checkable) {
                t.checkMaster = t.table.find(t.settings.check_master_selector);
                if (t.checkMaster.length < 0) {
                    t.settings.checkable = false;
                } else {
                    t.checkMaster.change(checkMasterClick);
                }

                initRowSelects();
            }
        };

        t.resetRowSelects = function() {
            initRowSelects();
            checkRowCounts();
        };

        function initRowSelects() {
            if (!t.settings.checkable) {
                return;
            }

            t.table.find('tbody').find('tr').each(function () {
                initRowSelect($(this));
            });
        }

        function initRowSelect($row) {
            var $cb = $row.find(t.settings.check_row_selector);
            if ($cb.length > 0) {
                $cb.unbind('change');
                $cb.change(function () {
                    var checked = $(this).prop('checked');
                    if (checked) {
                        t.selectRow($row);
                    } else {
                        t.unselectRow($row);
                    }
                    t.checkSelectedRows();
                });
            }
        }

        function checkMasterClick(e) {
            e.preventDefault();
            t.checkToggleAll();
        };

        function initTable() {
            t.table = $(t.settings.table_selector);
            if(t.table.length && t.table !== undefined && t.table !== null) {
                t.table.closest('.table-responsive').removeClass('table-responsive');
            }
            t.table.trigger( 'init-table', [t] );
        };

        function postInitTable() {
            t.table.find('thead th.display-hide').each(function(){
                $(this).removeClass('display-hide');
            });
            t.table.find('tbody td.display-hide').each(function(){
                $(this).removeClass('display-hide');
            });
            t.table.trigger( 'post-init-table', [t] );
        };

        function resetUniform()
        {
            t.table.trigger( 'reset-uniform', [t] );
            if(t.settings.checkable) {
                t.table.find('tbody').find('tr').each(function(){
                    var cb = $(this).find(':checkbox');
                    if(cb.length > 0) {
                        cb.each(function(){
                            $(this).uniform();
                        });
                    }
                });
            }
        }

        function initDatatables() {
            t.table.trigger( 'pre-init-datatables', [t] );
            t.table.dataTable({
                "sDom": "<'row'<'col-md-6 col-sm-6 col-xs-6'l><'col-md-6 col-sm-6 col-xs-6'f>r>t<'row paginator'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", //default layout without horizontal scroll(remove this setting to enable horizontal scroll for the table)
                "aLengthMenu": [
                    [5, 10, 25, 50, 100, -1],
                    [5, 10, 25, 50, 100, "All"] // change per page values here
                ],
                "sPaginationType": 'bootstrap',
                "iDisplayLength": t.settings.dt_display_length,
                "bAutoWidth": false,
                "aoColumnDefs": [
                    {"bSortable": false, aTargets: t.settings.no_sort_columns}
                ],
                "fnPreDrawCallback": function() {
                    getReponsiveHelper();
                },
                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    var r = getReponsiveHelper();
                    r.createExpandIcon(nRow);
                },
                "fnDrawCallback": function(oSettings) {
                    var r = getReponsiveHelper();
                    r.respond();
                    setTimeout(function(){
                        resetRowOptions();
                        initRowSelects();
                    },500);

                    if(typeof t.settings.onDrawCallback == 'function') {
                        t.settings.onDrawCallback(t);
                    }
                },
                "aaSorting": t.settings.aSort,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": t.settings.dataTables_ajax_url,
                "sServerMethod": "POST",
                "aoColumns": t.settings.dataTables_aoColumns
            });
            t.datatable = t.table.dataTable();

            if(t.settings.dataTables_aoColumnFilter.length > 0) {
                t.datatable.columnFilter({
                    "aoColumns": t.settings.dataTables_aoColumnFilter
                });
            }

            var id = t.table.attr('id') + '_wrapper',
                tw = $('#' + id);
            tw.find('.dataTables_filter input').addClass('form-control');
            tw.find('.dataTables_length select').addClass('form-control input-small');
            tw.find('.dataTables_length select').select2();
            tw.find('.dataTables_length select').change(resetRowOptions);
            tw.find('.dataTables_length select').change(resetRowOptions);
            tw.find('.dataTables_length select').change(t.forceUncheckAll);
            tw.find('.pagination').click(resetRowOptions);
            tw.find('.pagination').click(t.forceUncheckAll);
            t.table.find('thead').click(resetRowOptions);

            // lets re-style the datatables_filter input
            var filter_wrapper = tw.find('.dataTables_filter'),
                label = filter_wrapper.find('label'),
                input = filter_wrapper.find('input'),
                $ii = $('<div class="input-icon" />'),
                $icon = $('<i class="fa fa-search"></i>');

            $ii.addClass('pull-right');
            $ii.append( $icon).append(input);
            filter_wrapper.append($ii);
            label.remove();

            // re-style the selection count
            var length_wrapper = tw.find('.dataTables_length'),
                label = length_wrapper.find('label');
            label.children().each(function(){
                length_wrapper.append($(this));
            });
            label.remove();

            $(window).resize(function(){
                resetRowOptions();
                initRowSelects();
            });

            t.table.trigger( 'post-init-datatables', [t] );
        };

        function getReponsiveHelper()
        {
            if(t.responsiveHelper === null) {
                t.responsiveHelper = new ResponsiveDatatablesHelper(t.table, {tablet: 1024, phone: 480}, {
                    hideEmptyColumnsInRowDetail: true,
                    doNotShowColumns: t.settings.no_sort_columns
                });
            }
            return t.responsiveHelper;
        }

        function removeRowsClick(e) {
            t.table.trigger( 'pre-remove-rows-click', [t,e] );
            e.preventDefault();
            bootbox.confirm('Are you sure you want to remove these ' + t.settings.entity + '(s)?',function(res){
                if(res) {
                    t.removeRows();
                }
            });
            t.table.trigger( 'post-remove-rows-click', [t,e] );
        };

        function addNewRowClick(e) {
            t.table.trigger( 'pre-add-new-row-click', [t,e] );
            e.preventDefault();
            t.addNewRow();
            t.table.trigger( 'post-add-new-row-click', [t,e] );
        };

        t.checkSelectedRows = function (repeat) {
            repeat = repeat === true;

            if (t.getSelectedRowsCount() > 0) {
                t.removeRowsBtn.show();
                if(typeof t.settings.onToggleRemoveRowsBtn == 'function') {
                    t.settings.onToggleRemoveRowsBtn(t,true);
                }
                t.table.trigger('toggle_remove_rows_btn', [t,true]);
            } else {
                t.removeRowsBtn.hide();
                if(typeof t.settings.onToggleRemoveRowsBtn == 'function') {
                    t.settings.onToggleRemoveRowsBtn(t,false);
                }
                t.table.trigger('toggle_remove_rows_btn', [t,false]);
            }
            if (repeat) {
                setTimeout(function () {
                    t.checkSelectedRows(true);
                }, 2000);
            }
        };

        t.getSelectedRowsCount = function () {
            var rows = t.getSelectedRows();
            $(t.settings.selected_count_selector).html(rows.length);
            t.table.trigger( 'get_selected_rows_count', [t,rows.length] );
            return rows.length;
        };

        t.getSelectedRows = function () {
            return $rows = t.table.find('tbody').find('tr.selected:visible');
        };

        t.getSelectedRowsIds = function() {
            var ids = [];
            t.getSelectedRows().each(function(){
                var id = t.getRowDataId($(this));
                ids.push(id);
            });
            t.table.trigger( 'get_selected_rows_ids', [t,ids] );
            return ids;
        };

        t.forceUncheckAll = function () {
            t.checkMaster.prop('checked', false);
            if ($.uniform) {
                $.uniform.update(t.checkMaster);
            }
            t.checkMaster.trigger('change');
        }

        t.checkToggleAll = function () {
            if (!t.settings.checkable) {
                return;
            }

            var checked = t.checkMaster.prop('checked');
            if (checked) {
                t.checkAllRows();
            } else {
                t.uncheckAllRows();
            }

            t.checkSelectedRows();
        };

        t.checkAllRows = function () {
            if (!t.settings.checkable) {
                return;
            }
            t.table.find('tbody').find('tr').each(function () {
                var $cb = $(this).find('.row-check');
                if ($cb.attr('type') == 'checkbox') {
                    $cb.prop('checked', true);
                    if ($.uniform) {
                        $.uniform.update($cb);
                    }
                }
                t.selectRow($(this));
            });
        };

        t.uncheckAllRows = function () {
            if (!t.settings.checkable) {
                return;
            }
            t.table.find('tbody').find('tr').each(function () {
                var $cb = $(this).find('.row-check');
                if ($cb.attr('type') == 'checkbox') {
                    $cb.prop('checked', false);
                    if ($.uniform) {
                        $.uniform.update($cb);
                    }
                }
                t.unselectRow($(this));
            });
        };

        t.selectRow = function ($row) {
            if (!$row.hasClass('selected') && $row.attr('data-id') !== undefined) {
                $row.addClass('selected');
                var $cb = $row.find('.row-check');
                if ($cb.length > 0) {
                    $cb.prop('checked', true);
                    if ($.uniform) {
                        $.uniform.update($cb);
                    }
                }
            }
        };

        t.unselectRow = function ($row) {
            if ($row.hasClass('selected')) {
                $row.removeClass('selected');
                var $cb = $row.find('.row-check');
                if ($cb.length > 0) {
                    $cb.prop('checked', false);
                    if ($.uniform) {
                        $.uniform.update($cb);
                    }
                }
            }
        }

        t.removeRows = function () {
            var selected = t.getSelectedRows();
            $.each(selected,function(ix,row){
                var $row = $(row),
                    id = t.getRowDataId($row);
                if(id !== null && id) {
                    removeRow(id);
                }
            });
        };

        t.addNewRow = function () {
            t.newModal.modal({'remote': t.settings.add_row_url });
        };

        t.editRow = function(id) {
            t.editModal.modal({'remote': t.settings.edit_row_url + '/' + id});
        };

        t.viewRow = function(id) {
            t.viewModal.modal({'remote': t.settings.view_row_url + '/' + id});
        };

        t.addedNewRow = function(res, status ) {
            if(typeof res != 'object' ) res = JSON.parse(res);
            var data = res.data;

            if(!res.success) {
                toastr['error'](res.msg, 'Error');
                return;
            }

            var rowData = t.getRowData( data );

            // add the row to the table
            t.datatable.fnAddData( rowData );

            setTimeout(function(){
                resetRowOptions();
                t.forceUncheckAll();
            },750);

            toastr['success']('Successfully added new ' + t.settings.entity, 'Added');

            checkRowCounts();
        };

        t.reInitDatatables = function()
        {
            t.datatable.fnDestroy();
            initDatatables();
        };

        t.getRowData = function(data) {
            if(t.settings.fnGetRowData !== undefined) {
                if(typeof t.settings.fnGetRowData == 'function') {
                    return t.settings.fnGetRowData( data );
                }
            }

            var rowData = [];

            var sw = '<i class="fa fa-' + ( data.switch ? 'checkgreen' : 'times red' ) + '"></i>',
                ac = '<i class="fa fa-' + ( data.active ? 'checkgreen' : 'times red' ) + '"></i>',
                actions = '<a class="btn btn-xs btn-info btn-edit" href="javascript:void(0)" title="Edit ' + t.settings.entity + '"><i class="fa fa-edit"></i> Edit</a><a class="btn btn-xs btn-danger btn-remove" href="javascript:void(0)" title="Remove ' + t.settings.entity + '"><i class="fa fa-trash-o"></i> Remove</a>';

            // create the data row
            rowData[0] = '<input type="checkbox" data-id="' + data.id  + '" class="row-check" value="1">';
            rowData[1] = data.id;
            rowData[2] = data.name;
            rowData[3] = data.role;
            rowData[4] = data.parents;
            rowData[5] = data.users;
            rowData[6] = sw;
            rowData[7] = ac;
            rowData[8] = actions;

            return rowData;
        };


        t.getRowDataId = function(row) {
            var $row = $(row),
                did = $row.find('[data-id]');

            if($row.attr('data-id') !== undefined) {
                return $row.attr('data-id');
            }

            if(did.length > 0) {
                did = did[0]
                did = $(did);
                $row.attr('data-id', did.attr('data-id'));
                return did.attr('data-id');
            }

            return null;
        };

        t.getRowById = function(id) {
            var $row = null,
                did = t.table.find('tbody').find('[data-id=' + id + ']');

            if(did.length > 0) {
                did = did[0];
                did = $(did);

                if(did.is('tr')) {
                    return did;
                }

                $row = did.closest('tr');
                $row.attr('data-id',id);
            }

            return $row;
        }

        t.editedRow = function(res, status) {
            if(typeof res != 'object' ) res = JSON.parse(res);
            var data = res.data,
                $row = t.getRowById(data.id);

            t.populateEditedRowData( $row, data );

            if(toastr) {
                toastr['success']('Successfully saved ' + t.settings.entity,'Saved ' + t.settings.entity);
            }
        };

        t.populateEditedRowData = function( row, data ) {
            if(t.settings.fnPopulateEditedRowData !== undefined) {
                if(typeof t.settings.fnPopulateEditedRowData == 'function'
                    || typeof t.settings.fnPopulateEditedRowData == 'object') {
                    return t.settings.fnPopulateEditedRowData( row, data );
                }
            }

            var $row = $(row);

            $row.find('td').eq(2).html( data.name );
            $row.find('td').eq(3).html( data.role );
            $row.find('td').eq(4).html( data.parents );
            $row.find('td').eq(5).html( data.users );

            var sw = 'fa fa-check green';
            if( !data.switch ) {
                sw = 'fa fa-times red';
            }
            $row.find('td').eq(6).html( '<i class="' + sw + '"></i>' );

            var ac = 'fa fa-check green';
            if( !data.active ) {
                ac = 'fa fa-times red';
            }
            $row.find('td').eq(7).html( '<i class="' + ac + '"></i>' );

            $row.find('td').eq(2).css('color',data.color + ' !important');
        };

        init();

        return t;
    }
})(jQuery);