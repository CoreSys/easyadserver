<?php
/**
 * Created by PhpStorm.
 * User: jmccreig
 * Date: 3/7/14
 * Time: 2:41 PM
 */

namespace CoreSys\AdminBundle\Entity;

use Symfony\Component\Filesystem\Filesystem;

/**
 * Class Cron
 * @package CoreSys\AdminBundle\Entity
 */
class Cron
{

    /**
     * @var string
     */
    protected $minute = '*';

    /**
     * @var string
     */
    protected $hour = '*';

    /**
     * @var string
     */
    protected $dayOfMonth = '*';

    /**
     * @var string
     */
    protected $month = '*';

    /**
     * @var string
     */
    protected $dayOfWeek = '*';

    /**
     * @var string
     */
    protected $command;

    /**
     * @var string
     */
    protected $logFile;

    /**
     * @var string
     */
    protected $logSize;

    /**
     * @var string
     */
    protected $errorFile;

    /**
     * @var string
     */
    protected $errorSize;

    /**
     * @var string
     */
    protected $lastRunTime;

    /**
     * @var string
     */
    protected $status;

    /**
     * @var string
     */
    protected $comment;

    /**
     * @var bool
     */
    protected $isSuspended = FALSE;

    /**
     * Parses a cron line into a Cron instance
     *
     * @static
     *
     * @param $cron string The cron line
     *
     * @return Cron
     */
    public static function parse( $cron )
    {
        if ( substr( $cron, 0, 12 ) == '#suspended: ' ) {
            $cron        = substr( $cron, 12 );
            $isSuspended = TRUE;
        }

        $parts = \explode( ' ', $cron );

        $command = \implode( ' ', \array_slice( $parts, 5 ) );

        // extract comment
        if ( \strpos( $command, '#' ) ) {
            list( $command, $comment ) = \explode( '#', $command );
            $comment = \trim( $comment );
        }

        // extract error file
        if ( \strpos( $command, '2>' ) ) {
            list( $command, $errorFile ) = \explode( '2>', $command );
            $errorFile = \trim( $errorFile );
        }

        // extract log file
        if ( \strpos( $command, '>' ) ) {
            list( $command, $logFile ) = \explode( '>', $command );
            $logFile = \trim( $logFile );
        }

        // compute last run time, and file size
        $lastRunTime = NULL;
        $logSize     = NULL;
        $errorSize   = NULL;
        if ( isset( $logFile ) && \file_exists( $logFile ) ) {
            $lastRunTime = \filemtime( $logFile );
            $logSize     = \filesize( $logFile );
        }
        if ( isset( $errorFile ) && \file_exists( $errorFile ) ) {
            $lastRunTime = \max( $lastRunTime ? : 0, \filemtime( $errorFile ) );
            $errorSize   = \filesize( $errorFile );
        }

        // compute status
        $status = 'error';
        if ( $logSize === NULL && $errorSize === NULL ) {
            $status = 'unknown';
        } else if ( $errorSize === NULL || $errorSize == 0 ) {
            $status = 'success';
        }

        // create cron instance
        $cron = new self();
        $cron->setMinute( $parts[ 0 ] );
        $cron->setHour( $parts[ 1 ] );
        $cron->setDayOfMonth( $parts[ 2 ] );
        $cron->setMonth( $parts[ 3 ] );
        $cron->setDayOfWeek( $parts[ 4 ] );
        $cron->setCommand( \trim( $command ) );
        $cron->setLastRunTime( $lastRunTime );
        $cron->setLogSize( $logSize );
        $cron->setErrorSize( $errorSize );
        $cron->setStatus( $status );
        if ( isset( $isSuspended ) ) {
            $cron->setSuspended( $isSuspended );
        }
        if ( isset( $comment ) ) {
            $cron->setComment( $comment );
        }
        if ( isset( $logFile ) ) {
            $cron->setLogFile( $logFile );
        }
        if ( isset( $errorFile ) ) {
            $cron->setErrorFile( $errorFile );
        }

        return $cron;
    }

    /**
     * Sets the value of isSuspended
     *
     * @param boolean $suspended status
     *
     * @return Cron
     */
    public function setSuspended( $isSuspended = TRUE )
    {
        if ( $this->isSuspended != $isSuspended ) {
            $this->isSuspended = $isSuspended;
        }

        return $this;
    }

    /**
     * Get Command
     *
     * @return string
     */
    public function getCommand()
    {
        return $this->command;
    }

    /**
     * Set Command
     *
     * @param string $command
     */
    public function setCommand( $command = NULL )
    {
        $this->command = $command;

        return $this;
    }

    /**
     * Get Comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set Comment
     *
     * @param string $comment
     */
    public function setComment( $comment = NULL )
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get DayOfMonth
     *
     * @return string
     */
    public function getDayOfMonth()
    {
        return $this->dayOfMonth;
    }

    /**
     * Set DayOfMonth
     *
     * @param string $dayOfMonth
     */
    public function setDayOfMonth( $dayOfMonth = NULL )
    {
        $this->dayOfMonth = $dayOfMonth;

        return $this;
    }

    /**
     * Get DayOfWeek
     *
     * @return string
     */
    public function getDayOfWeek()
    {
        return $this->dayOfWeek;
    }

    /**
     * Set DayOfWeek
     *
     * @param string $dayOfWeek
     */
    public function setDayOfWeek( $dayOfWeek = NULL )
    {
        $this->dayOfWeek = $dayOfWeek;

        return $this;
    }

    /**
     * Get ErrorFile
     *
     * @return string
     */
    public function getErrorFile()
    {
        return $this->errorFile;
    }

    /**
     * Set ErrorFile
     *
     * @param string $errorFile
     */
    public function setErrorFile( $errorFile = NULL )
    {
        $this->errorFile = $errorFile;

        return $this;
    }

    /**
     * Get ErrorSize
     *
     * @return string
     */
    public function getErrorSize()
    {
        return $this->errorSize;
    }

    /**
     * Set ErrorSize
     *
     * @param string $errorSize
     */
    public function setErrorSize( $errorSize = NULL )
    {
        $this->errorSize = $errorSize;

        return $this;
    }

    /**
     * Get Hour
     *
     * @return string
     */
    public function getHour()
    {
        return $this->hour;
    }

    /**
     * Set Hour
     *
     * @param string $hour
     */
    public function setHour( $hour = NULL )
    {
        $this->hour = $hour;

        return $this;
    }

    /**
     * Get LastRunTime
     *
     * @return string
     */
    public function getLastRunTime()
    {
        return $this->lastRunTime;
    }

    /**
     * Set LastRunTime
     *
     * @param string $lastRunTime
     */
    public function setLastRunTime( $lastRunTime = NULL )
    {
        $this->lastRunTime = $lastRunTime;

        return $this;
    }

    /**
     * Get LogFile
     *
     * @return string
     */
    public function getLogFile()
    {
        return $this->logFile;
    }

    /**
     * Set LogFile
     *
     * @param string $logFile
     */
    public function setLogFile( $logFile = NULL )
    {
        $this->logFile = $logFile;

        return $this;
    }

    /**
     * Get LogSize
     *
     * @return string
     */
    public function getLogSize()
    {
        return $this->logSize;
    }

    /**
     * Set LogSize
     *
     * @param string $logSize
     */
    public function setLogSize( $logSize = NULL )
    {
        $this->logSize = $logSize;

        return $this;
    }

    /**
     * Get Minute
     *
     * @return string
     */
    public function getMinute()
    {
        return $this->minute;
    }

    /**
     * Set Minute
     *
     * @param string $minute
     */
    public function setMinute( $minute = NULL )
    {
        $this->minute = $minute;

        return $this;
    }

    /**
     * Get Month
     *
     * @return string
     */
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * Set Month
     *
     * @param string $month
     */
    public function setMonth( $month = NULL )
    {
        $this->month = $month;

        return $this;
    }

    /**
     * Get Status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set Status
     *
     * @param string $status
     */
    public function setStatus( $status = NULL )
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Transforms the cron instance into a cron line
     *
     * @return string
     */
    public function __toString()
    {
        $cronLine = '';
        if ( $this->isSuspended() ) {
            $cronLine .= '#suspended: ';
        }

        $cronLine .= $this->getExpression() . ' ' . $this->command;
        if ( '' != $this->logFile ) {
            $cronLine .= ' > ' . $this->logFile;
        }
        if ( '' != $this->errorFile ) {
            $cronLine .= ' 2> ' . $this->errorFile;
        }
        if ( '' != $this->comment ) {
            $cronLine .= ' #' . $this->comment;
        }

        return $cronLine;
    }

    /**
     * @return bool
     */
    public function isSuspended()
    {
        return $this->getIsSuspended();
    }

    /**
     * Get IsSuspended
     *
     * @return boolean
     */
    public function getIsSuspended()
    {
        return $this->isSuspended;
    }

    /**
     * Set IsSuspended
     *
     * @param boolean $isSuspended
     */
    public function setIsSuspended( $isSuspended = NULL )
    {
        $this->isSuspended = $isSuspended;

        return $this;
    }

    /**
     * @return string
     */
    function getExpression()
    {
        return \sprintf( '%s %s %s %s %s', $this->minute, $this->hour, $this->dayOfMonth, $this->month, $this->dayOfWeek );
    }
}