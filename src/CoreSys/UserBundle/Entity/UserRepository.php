<?php
/**
 * This file is part of the CoreSysUserBundle package.
 * (c) J&L Core Systems http://jlcoresystems.com | http://joshmccreight.com
 */

namespace CoreSys\UserBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\AST\Join;

/**
 * UserRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class UserRepository extends EntityRepository
{

    public function getDatatablesQueryBuilder( &$data )
    {
        $q = $this->createQueryBuilder( 'e' )
            ->setMaxResults( intval( $data[ 'limit' ]))
            ->setFirstResult( intval( $data[ 'offset']));

        $search_roles = false;

        if( !empty( $data[ 'searchString' ] ) ) {
            $search_query = array();
            $search_roles = false;
            foreach( $data[ 'columns' ] as $col ) {
                if( $col[ 'searchable' ] ) {
                    $col[ 'id' ] = preg_replace('/_(.?)/e',"strtoupper('$1')", $col['id'] );
                    if($col[ 'id' ] != 'roles' ) {
                        $search_query[] = 'e.' . $col[ 'id' ] . ' LIKE :search';
                    } else if( $col[ 'id' ] == 'roles' ) {
                        $search_roles = true;
                    }
                }
            }

            if( $search_roles ) {
                $q->leftJoin( 'e.sys_roles', 'r', 'WITH', 'r.name LIKE \'%' . $data[ 'searchString' ] . '%\'');
                $search_query[] = 'r.name LIKE :search';
            }

            $search_query = '(' . implode( ' OR ', $search_query ) . ')';
            $q->where( $search_query )
                ->setParameter( 'search', '%' . $data[ 'searchString' ] . '%' );
        }

        $sort_col = $data[ 'columns' ][ $data[ 'sortColumn' ] ][ 'id' ];
        $sort_dir = $data[ 'sortDir' ];

        $sort_col = preg_replace('/_(.?)/e',"strtoupper('$1')",$sort_col);

        $q->orderBy( 'e.' . $sort_col, $sort_dir );

        return $q;
    }

    public function getDatatablesQuery( &$data ) {
        $q = $this->getDatatablesQueryBuilder( $data );
        $q = $q->getQuery();

        return $q;
    }

    public function getDatatablesResults( &$data, $return_results = false )
    {
        $q = $this->getDatatablesQuery( $data );

        $results = $q->getResult();

        if( $return_results ) {
            return $results;
        }

        $return = array(
            'sEcho' => $data[ 'echo' ],
            'iTotalRecords' => count( $results ),
            'iTotalDisplayRecords' => $this->getDatatablesAllCount( $data ),
            'aaData' => array()
        );

        foreach( $results as $idx => $entity ) {
            $entity_data = $this->getDataTablesEntityDataRow( $entity );

            if( !isset( $return[ 'aaData' ][ $idx ] ) ) {
                $return[ 'aaData' ][ $idx ] = array();
            }

            if( empty( $data[ 'columns' ][ 0 ][ 'id' ] ) ) {
                $return[ 'aaData' ][ $idx ][0] = null;
            }

            foreach( $entity_data as $k => $v ) {
                $return[ 'aaData' ][ $idx ][ $k ] = $v;
            }

            if( empty( $data[ 'columns' ][ count( $data[ 'columns' ] ) - 1 ][ 'id'] ) )  {
                $return[ 'aaData' ][ $idx ][ count( $data[ 'columns' ]) -1] = null;
            }
        }

        return $return;
    }

    public function getDataTablesEntityDataRow( &$entity )
    {
        $return = array();

        foreach( $entity->getAdminTableArray() as $k => $v ) {
            $return[ $k ] = $v;
        }

        if( $return[ 'active' ] === true) {
            $return[ 'active' ] = '<i class="fa fa-check green" title="Active"></i>';
            $return[ 'status' ] = '<i class="fa fa-check green" title="Active"></i>';
        } else {
            $return[ 'active' ] = '<i class="fa fa-times red" title="InActive"></i>';
            $return[ 'status' ] = '<i class="fa fa-times red" title="InActive"></i>';
        }

        $class = get_class( $entity );
        $class = explode( DIRECTORY_SEPARATOR, $class );
        $class = array_pop( $class );
        $class = explode( '/', $class );
        $class = array_pop( $class );
        $class = explode( '\\', $class );
        $class = array_pop( $class );
        $class = trim( ucwords( str_replace( '_', ' ', strtolower( $class ) ) ) );

        $id = $entity->getId();

        $actions = array(
            '<li class="view-row text-left" data-username="' . $entity->getUsername() . '" data-id="' . $id . '"><a class="btn-view" href="javascript:void(0);"><icon class="fa fa-eye" title="View ' . $class . '"></i> View ' . $class . '</a></li>',
            '<li class="edit-row text-left" data-id="' . $id . '"><a class="btn-edit" href="javascript:void(0);"><icon class="fa fa-edit" title="Edit ' . $class . '"></i> Edit ' . $class . '</a></li>',
            '<li class="remove-row text-left" data-id="' . $id . '"><a class="btn-remove" href="javascript:void(0);"><icon class="fa fa-times red" title="Remove ' . $class . '"></i> Remove ' . $class . '</a></li>'
        );

        $actions = '<div class="btn-group" data-id="' . $id . '"><button class="btn btn-xs dropdown-toggle" type="button" data-toggle="dropdown">Options <i class="fa fa-angle-down"></i></button><ul class="dropdown-menu pull-right" role="menu">' . implode( $actions ) . '</ul></div>';
        $return[ 'options' ] = $actions;
        $return[ 'actions' ] = $actions;

        return $return;
    }

    public function getDatatablesAllCount( $data )
    {
        $q = $this->getDatatablesQueryBuilder( $data );
        $q->select( 'COUNT(e.id)' );
        $q = $q->getQuery();

        $q->setFirstResult( null );
        $q->setMaxResults( null );

        $results = $q->getResult();

        return $q->getSingleScalarResult();
    }

    public function getUsersWithRole( Role $role = NULL )
    {
        if ( empty( $role ) ) {
            return $this->fetchAll();
        }

        $q = $this->createQueryBuilder( 'u' )
                  ->leftJoin( 'u.sys_roles', 'r' )
                  ->andWhere( 'r.id = :rid' )
                  ->setParameter( 'rid', $role->getId() )
                  ->getQuery();

        return $q->getResult();
    }

    public function getNewUsers( $days = 7 )
    {
        $date = new \DateTime();
        $date = $date->sub( new \DateInterval( 'P' . intval( $days ) . 'D' ) );
        $q    = $this->createQueryBuilder( 'u' )
                     ->where( 'u.created_at >= :date' )
                     ->setParameter( 'date', $date )
                     ->orderBy( 'u.created_at', 'desc' )
                     ->getQuery();

        return $q->getResult();
    }

    public function getUsers( $active = NULL )
    {
        $q = $this->createQueryBuilder( 'u' )
                  ->orderBy( 'u.created_at', 'desc' );

        if ( $active === TRUE || $active == FALSE ) {
            $q->where( 'u.enabled = :active' )
              ->setParameter( 'active', $active );
        }

        $q = $q->getQuery();

        return $q->getResult();
    }

    public function getUsersCount( $active = NULL, $from = NULL, $to = NULL )
    {
        $q = $this->createQueryBuilder( 'u' )
                  ->select( 'COUNT(u.id)' );

        if ( !empty( $active ) ) {
            $active = $active === TRUE;
            $q->where( 'u.enabled = :enabled' )
              ->setParameter( 'enabled', $active );
        }

        if ( $from instanceof \DateTime ) {
            $q->where( 'u.created_at >= :from' )
              ->setParameter( 'from', $from );
        }

        if ( $to instanceof \DateTime ) {
            $q->where( 'u.created_at <= :to' )
              ->setParameter( 'to', $to );
        }

        $q = $q->getQuery();

        return $q->getSingleScalarResult();
    }

    public function search( $sort = 'created_at', $sort_dir = 'desc', $offset = 0, $length = 50, $search_string = NULL, $search_col = NULL, $from = NULL, $to = NULL, Role $role = NULL )
    {
        $sort     = !empty( $sort ) ? $sort : 'created_at';
        $sort_dir = $sort_dir == 'asc' ? 'asc' : 'desc';

        $from = $from instanceof \DateTime ? $from : NULL;
        $to   = $to instanceof \DateTime ? $to : NULL;
        if ( empty( $from ) || empty( $to ) ) {
            $from = $to = NULL;
        }

        $q = $this->createQueryBuilder( 'u' )
                  ->orderBy( 'u.' . $sort, $sort_dir )
                  ->setFirstResult( $offset )
                  ->setMaxResults( $length );

        if ( !empty( $from ) ) {
            $q->where( 'u.created_at >= :from AND u.created_at <= :to' )
              ->setParameters( array( 'from' => $from, 'to' => $to ) );
        }

        if ( !empty( $search_string ) ) {
            $q->where( '(u.username LIKE :search OR u.email LIKE :search)' )
              ->setParameter( 'search', '%' . $search_string . '%' );
        }

        if ( !empty( $role ) ) {
            $role_ids = array();
            $this->addRoleIdsToSearch( $role_ids, $role );
            $rids = '(' . implode( ',', $role_ids ) . ')';
            $q->leftJoin( 'u.sys_roles', 'r' )
              ->andWhere( 'r.id IN ' . $rids );
        }

        $q = $q->getQuery();

        return $q->getResult();
    }

    public function searchCount( $search_string = NULL, $search_col = NULL, $from = NULL, $to = NULL, Role $role = NULL )
    {
        $q = $this->createQueryBuilder( 'u' )
                  ->select( 'COUNT(u.id)' );

        $from = $from instanceof \DateTime ? $from : NULL;
        $to   = $to instanceof \DateTime ? $to : NULL;
        if ( empty( $from ) || empty( $to ) ) {
            $from = $to = NULL;
        }
        if ( !empty( $from ) ) {
            $q->where( 'u.created_at >= :from AND u.created_at <= :to' )
              ->setParameters( array( 'from' => $from, 'to' => $to ) );
        }

        if ( !empty( $search_string ) ) {
            $q->where( '(u.username LIKE :search OR u.email LIKE :search)' )
              ->setParameter( 'search', '%' . $search_string . '%' );
        }

        if ( !empty( $role ) ) {
            $role_ids = array();
            $this->addRoleIdsToSearch( $role_ids, $role );
            $rids = '(' . implode( ',', $role_ids ) . ')';
            $q->leftJoin( 'u.sys_roles', 'r' )
              ->andWhere( 'r.id IN ' . $rids );
        }

        $q = $q->getQuery();

        return $q->getSingleScalarResult();
    }

    public function addRoleIdsToSearch( &$ids, Role $role = NULL )
    {
        if ( empty( $role ) ) return;
        $ids = is_array( $ids ) ? $ids : array();

        if ( !in_array( $role->getId(), $ids ) ) {
            $ids[ ] = $role->getId();
        }

        foreach ( $role->getChildren() as $child ) {
            $this->addRoleIdsToSearch( $ids, $child );
        }
    }

    public function getUsersCountBetween( \DateTime $from, \DateTime $to )
    {
        $q = $this->createQueryBuilder( 'u' )
                  ->select( 'COUNT(u.id)' )
                  ->where( 'u.created_at >= :from AND u.created_at <= :to' )
                  ->setParameters( array( 'from' => $from, 'to' => $to ) )
                  ->getQuery();

        return $q->getSingleScalarResult();
    }

    public function getUsersWithoutSysRole( $role_name = null )
    {
        $results = array();
        foreach( $this->findAll() as $user )
        {
            if( !$user->hasSysRole( $role_name ) ) {
                $results[] = $user;
            }
        }

        return $results;
    }

    public function locateUser( $slug = null )
    {
        $row = $this->findOneById( $slug );
        if( empty( $row ) ) {
            $row = $this->findOneByUsername( $slug );
            if( empty( $row ) ) {
                $row = $this->findOneByEmail( $slug );
            }
        }
        return $row;
    }
}
