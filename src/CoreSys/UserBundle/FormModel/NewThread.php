<?php

namespace CoreSys\UserBundle\FormModel;

use CoreSys\UserBundle\FormModel\AbstractMessage;
use FOS\MessageBundle\Model\ParticipantInterface;

/**
 * Class AdminNewThread
 * @package CoreSys\UserBundle\FormModel
 */
class NewThread extends AbstractMessage
{
    /**
     * @var string
     */
    protected $subject;

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->getSubject();
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param null $subject
     * @return $this
     */
    public function setSubject($subject = null)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @param null $title
     * @return $this
     */
    public function setTitle($title = null)
    {
        return $this->setSubject($title);
    }
}