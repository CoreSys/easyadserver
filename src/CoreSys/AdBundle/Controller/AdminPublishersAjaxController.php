<?php

namespace CoreSys\AdBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CoreSys\SiteBundle\Controller\AjaxController as BaseController;

/**
 * Class AdminPublishersAjaxController
 * @package CoreSys\AdBundle\Controller
 * @Route("/admin/ajax/ads/publishers")
 */
class AdminPublishersAjaxController extends BaseController
{

}
