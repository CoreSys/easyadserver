<?php

namespace CoreSys\AdminBundle\Controller;

use CoreSys\AdminBundle\Entity\Cron;
use CoreSys\AdminBundle\Form\CronType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CoreSys\SiteBundle\Controller\AdminController as BaseController;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Serializer;

/**
 * Class AdminController
 * @package CoreSys\AdminBundle\Controller
 * @Route("/admin")
 */
class AdminController extends BaseController
{
    /**
     * @Route("/", name="admin_index")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }

    /**
     * @Route("/menus", name="admin_menus")
     * @Template()
     */
    public function menusAction()
    {
        $repo = $this->getRepo( 'CoreSysAdminBundle:Menu' );
        $menus = $repo->findAll();

        return array( 'menus' => $menus );
    }

    /**
     * @Route("/cronManager", name="admin_cron_management")
     * @Template()
     */
    public function cronManagerAction()
    {
        $cm = $this->get( 'core_sys_admin.cron_manager' );;

        $form = $this->createForm( new CronType(), new Cron() );
        $request = $this->get( 'request' );
        if( $request->isMethod( 'POST' ) ) {
            $form->handleRequest( $request );
            if( $form->isValid() ) {
                $cron = $form->getData();
                $cm->addCron($cron);
                $this->msgSuccess('Successfully added new Cron');
                return $this->redirect( $this->generateUrl( 'admin_cron_management' ) );
            } else {
                $this->msgError( 'Could not add Cron' );
            }
        }

        return array(
            'crons' => $cm->getCrons(),
            'raw' => $cm->getRaw( true ),
            'form' => $form->createView()
        );
    }

    /**
     * @Route("/cronManager/edit/{id}", name="admin_cron_management_edit", defaults={"id"=null})
     * @Template()
     */
    public function cronEditAction($id)
    {
        $cm = $this->get( 'core_sys_admin.cron_manager' );
        $crons = $cm->getCrons();
        $form = $this->createForm( new CronType(), $crons[$id]);

        $request = $this->get( 'request' );
        if( $request->isMethod('POST')) {
            $form->handleRequest( $request );
            if($form->isValid()) {
                $cron = $form->getData();
                $cm->write();
                $this->msgSuccess( 'Saved cron' );
                $this->echoJsonSuccess( 'Successfully saved cron' );
            } else {
                $this->echoJsonError( 'Could not save cron' );
            }
        }

        $action = $this->generateUrl( 'admin_cron_management_edit', array( 'id' => $id ) );
        $redirect = $this->generateUrl( 'admin_cron_management' );

        return array( 'form' => $form->createView(), 'action' => $action, 'redirect' => $redirect );
    }

    /**
     * @Route("/cronManager/wakeup/{id}", name="admin_cron_management_wakeup", defaults={"id"=null})
     * @Template()
     */
    public function cronWakeupAction($id)
    {
        $cm = $this->get( 'core_sys_admin.cron_manager' );
        $crons = $cm->getCrons();
        $cron = $crons[$id];
        $cron->setSuspended(false);
        $cm->write();

        $this->msgSuccess( 'Successful wakeup of cron' );
        return $this->redirect( $this->generateUrl( 'admin_cron_management' ) );
    }

    /**
     * @Route("/cronManager/suspend/{id}", name="admin_cron_management_suspend", defaults={"id"=null})
     * @Template()
     */
    public function cronSuspendAction($id)
    {
        $cm = $this->get( 'core_sys_admin.cron_manager' );
        $crons = $cm->getCrons();
        $cron = $crons[$id];
        $cron->setSuspended(true);
        $cm->write();

        $this->msgSuccess( 'Successfully suspended cron' );
        return $this->redirect( $this->generateUrl( 'admin_cron_management' ) );
    }

    /**
     * @Route("/cronManager/remove/{id}", name="admin_cron_management_remove", defaults={"id"=null})
     * @Template()
     */
    public function cronRemoveAction($id)
    {
        $cm = $this->get( 'core_sys_admin.cron_manager' );
        $crons = $cm->getCrons();
        $cm->removeCron($id);

        $this->msgSuccess( 'Successfully removed cron' );
        return $this->redirect( $this->generateUrl( 'admin_cron_management' ) );
    }

    /**
     * @Route("/cronManager/file/{id}/{type}", name="admin_cron_management_file", defaults={"id"=null,"type"=null})
     * @Template()
     */
    public function cronFileAction($id,$type)
    {
        $cm = $this->get( 'core_sys_admin.cron_manager' );
        $crons = $cm->getCrons();
        $cron = $crons[$id];

        $file = $type == 'log' ? $cron->getLogFile() : $cron->getErrorFile();
        $content = file_get_contents( $file );

        $content = preg_replace( "/[\r\n]+/", "<br>", $content );
        $content = preg_replace( "/[\t]/", "&nbsp;&nbsp;&nbsp;&nbsp;", $content );
        $content = str_replace( ' ', '&nbsp;', $content );

        return array( 'file' => $file, 'content' => $content );
    }
}
