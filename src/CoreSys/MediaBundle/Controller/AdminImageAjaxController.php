<?php

namespace CoreSys\MediaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CoreSys\SiteBundle\Controller\AjaxController as BaseAjaxController;
use CoreSys\MediaBundle\Entity\Image;

/**
 * Class AdminImageAjaxController
 * @package CoreSys\MediaBundle\Controller
 *
 * @Route("/admin/media/image/ajax", options={"expose"=true})
 */
class AdminImageAjaxController extends BaseAjaxController
{
    /**
     * @Route("/remove/{id}", name="admin_media_ajax_image_remove", defaults={"id"=null})
     * @ParamConverter("image", class="CoreSysMediaBundle:Image")
     * @Template()
     */
    public function removeImageAction( Image $image )
    {
        $data = $image->toArray();

        $manager = $this->get( 'core_sys_media.image_manager' );
        $manager->removeImage( $image );
        $this->remove( $image );
        $this->flush();

        $this->echoJsonSuccess( 'success', $data );
        exit;
    }

    /**
     * @Route("/view/{id}", name="admin_media_ajax_image_view", defaults={"id"=null})
     * @ParamConverter("image", class="CoreSysMediaBundle:Image")
     * @Template("CoreSysMediaBundle:Default:imageModalInfoInner.html.twig")
     */
    public function viewInfoAction( Image $image )
    {
        return array( 'image' => $image );
    }

    /**
     * @Route("/updateColumn", name="admin_media_ajax_image_update_column")
     * @Template()
     */
    public function updateImageColumnAction()
    {
        $request = $this->get( 'request' );
        $repo = $this->getRepo( 'CoreSysMediaBundle:Image' );
        $id = $request->get( 'pk' );
        $image = $repo->findOneById( $id );
        if( empty( $image ) ) {
            $this->set500Halt( 'Could not locate image' );
        }

        $name = $request->get( 'name');
        $id = $image->getId();
        $remove = array( 'image-' . $id . '-', 'image-' . $id );
        foreach( $remove as $rem ) {
            $name = str_replace( $rem, '', $name );
        }

        $function = 'set' . str_replace( ' ', '', ucwords( str_replace( '_', ' ', strtolower( trim( $name ) ) )) );
        if( method_exists( $image, $function ) ) {
            $value = $request->get( 'value' );

            if( $name == 'active' ) {
                $value = $value == 1 || $value == '1' || $value == 'true' || $value === true;
            }
            $image->$function( $value );
            $this->persistAndFlush( $image );

            $data = $image->toArray();
            $data[ 'posts' ] = $request->request->all();
            $data[ 'posts' ][ 'name' ] = $name;
            $data[ 'posts' ][ 'function' ] = $function;
            $data[ 'posts' ][ 'end_value' ] = $value;

            $this->echoJsonSuccess( 'Success', $data );
            exit;
        }

        $this->set500Halt( 'Unknown column ' . $name );
        exit;
    }
}
