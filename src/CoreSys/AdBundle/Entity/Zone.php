<?php

namespace CoreSys\AdBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CoreSys\AdBundle\Entity\Website;

/**
 * Zone
 *
 * @ORM\Table(name="ad_zone")
 * @ORM\Entity(repositoryClass="CoreSys\AdBundle\Entity\ZoneRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Zone
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64)
     */
    private $name;
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;
    /**
     * @var string
     *
     * @ORM\Column(name="size", type="string", length=64, nullable=true)
     */
    private $size;
    /**
     * @var integer
     *
     * @ORM\Column(name="width", type="integer", nullable=true)
     */
    private $width;
    /**
     * @var integer
     *
     * @ORM\Column(name="height", type="integer", nullable=true)
     */
    private $height;
    /**
     * @var boolean
     *
     * @ORM\Column(name="chained", type="boolean", nullable=true)
     */
    private $chained;
    /**
     * @var Zone
     *
     * @ORM\OneToOne(targetEntity="Zone", mappedBy="parent_zone")
     */
    private $chained_zone;
    /**
     * @var Zone
     *
     * @ORM\OneToOne(targetEntity="Zone", inversedBy="chained_zone")
     * @ORM\JoinColumn(name="parent_zone_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    private $parent_zone;
    /**
     * @var Website
     *
     * @ORM\ManyToOne(targetEntity="Website", inversedBy="zones")
     * @ORM\JoinColumn(name="website_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $website;

    /**
     *
     */
    public function __construct()
    {
        $this->setCreatedAt(new \DateTime());
        $this->setUpdatedAt(new \DateTime());
        $this->setWidth(null);
        $this->setHeight(null);
        $this->setSize(null);
        $this->setDescription(null);
        $this->setChained(false);
    }

    /**
     * @return Website
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * @param Website $website
     * @return $this
     */
    public function setWebsite(Website $website)
    {
        $this->website = $website;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getChained()
    {
        return $this->chained === true;
    }

    /**
     * @param boolean $chained
     */
    public function setChained($chained = true)
    {
        $this->chained = $chained === true;
    }

    /**
     * @return \CoreSys\AdBundle\Entity\Zone
     */
    public function getParentZone()
    {
        return $this->parent_zone;
    }

    /**
     * @param \CoreSys\AdBundle\Entity\Zone $parent_zone
     */
    public function setParentZone($parent_zone = null)
    {
        $this->parent_zone = $parent_zone;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Zone
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Zone
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Zone
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Zone
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get size
     *
     * @return string
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set size
     *
     * @param string $size
     * @return Zone
     */
    public function setSize($size = null)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get width
     *
     * @return integer
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set width
     *
     * @param integer $width
     * @return Zone
     */
    public function setWidth($width = null)
    {
        if (!empty($width)) {
            $width = intval($width);
        }
        $this->width = $width;

        return $this;
    }

    /**
     * Get height
     *
     * @return integer
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set height
     *
     * @param integer $height
     * @return Zone
     */
    public function setHeight($height = null)
    {
        if (!empty($height)) {
            $height = intval($height);
        }
        $this->height = $height;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function prepersist()
    {
        $chained = $this->getChainedZone();
        if (!empty($chained) && $chained instanceof Zone) {
            $this->setChained(true);
        } else {
            $this->setChained(false);
        }
    }

    /**
     * @return \CoreSys\AdBundle\Entity\Zone
     */
    public function getChainedZone()
    {
        return $this->chained_zone;
    }

    /**
     * @param \CoreSys\AdBundle\Entity\Zone $chained_zone
     */
    public function setChainedZone(Zone $chained_zone)
    {
        $this->chained_zone = $chained_zone;
    }
}
