<?php

namespace CoreSys\PublisherBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use CoreSys\UserBundle\Entity\User;
use CoreSys\AdBundle\Entity\Campaign;

/**
 * Publisher
 *
 * @ORM\Table(name="publisher")
 * @ORM\Entity(repositoryClass="CoreSys\PublisherBundle\Entity\PublisherRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Publisher extends User
{
    /**
     * @var string
     */
    protected $type = 'publisher';

    /**
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

}
