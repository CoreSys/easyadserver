<?php

namespace CoreSys\SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ConfigType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('siteTheme', 'entity', array('required' => true, 'class' => 'CoreSysThemeBundle:Theme', 'attr' => array('data-postdesc' => 'The site front theme', 'class' => 'form-control select2me22')))
            ->add('adminTheme', 'entity', array('required' => true, 'class' => 'CoreSysThemeBundle:Theme', 'attr' => array('data-postdesc' => 'The site admin theme', 'class' => 'form-control select2me22')))
            ->add('siteName', null, array('required' => true, 'attr' => array('data-postdesc' => 'The Site Name', 'data-error-msg' => 'Please enter a Site Name')))
            ->add('siteTitle', null, array('required' => true, 'attr' => array('data-postdesc' => 'The Site Title', 'data-error-msg' => 'Please enter a Site Title')))
            ->add('siteSlogan', null, array('required' => true, 'attr' => array('data-postdesc' => 'The Site Slogan', 'data-error-msg' => 'Please enter a Site Slogan')))
            ->add('siteKeywords', null, array('required' => true, 'attr' => array('data-postdesc' => 'The Site Keywords', 'data-error-msg' => 'Please enter Site Keywords')))
            ->add('siteDescription', null, array('required' => true, 'attr' => array('data-postdesc' => 'The Site Description', 'rows' => 7, 'data-error-msg' => 'Please enter a Site Description')))
            ->add('siteAdminEmail', 'email', array('required' => true, 'attr' => array('data-postdesc' => 'The Site Administrator Email Address', 'data-error-msg' => 'Please enter a valid Administrator Email Address')))
            ->add('siteAdminEmailShow', 'checkbox', array('required' => false, 'attr' => array('class' => 'toggle', 'data-on' => 'success', 'data-off' => 'default')))
            ->add('siteWebmasterEmail', 'email', array('required' => true, 'attr' => array('data-postdesc' => 'The Site Webmaster Email Address', 'data-error-msg' => 'Please enter a valid Webmaster Email Address')))
            ->add('siteWebmasterEmailShow', 'checkbox', array('required' => false, 'attr' => array('class' => 'toggle', 'data-switch' => '')))
            ->add('siteSupportEmail', 'email', array('required' => true, 'attr' => array('data-postdesc' => 'The Site Support Email Address', 'data-error-msg' => 'Please enter a valid Support Email Address')))
            ->add('siteSupportEmailShow', 'checkbox', array('required' => false, 'attr' => array('class' => 'toggle', 'data-switch' => '')))
            ->add('termsOfUseTitle', null, array('required' => true, 'attr' => array('data-postdesc' => 'The Sites Terms of Use Title', 'data-error-msg' => 'Please enter a Terms of Use title')))
            ->add('termsOfUse', null, array('required' => true, 'attr' => array('data-postdesc' => 'The Sites Terms of Use', 'rows' => 12, 'data-error-msg' => 'Please enter Terms of Use')))
            ->add('privacyPolicyTitle', null, array('required' => true, 'attr' => array('data-postdesc' => 'The Sites Privacy Policy Title', 'data-error-msg' => 'Please enter a privacy policy title')))
            ->add('privacyPolicy', null, array('required' => true, 'attr' => array('data-postdesc' => 'The Sites Privacy Policy', 'rows' => 12, 'data-error-msg' => 'Please enter a privacy policy')))
            ->add('logo_file_one','file', array('required'=> false, 'label' => 'Default Logo', 'attr' => array( 'data-postdesc' => 'The default logo for the site' ) ) )
            ->add('logo_file_two','file', array('required'=> false, 'label' => 'Alternate Logo', 'attr' => array( 'data-postdesc' => 'The alternate logo for the site' ) ) )
            ->add('enable_google_analytics', 'checkbox', array( 'required' => false, 'attr' => array( 'data-postdesc2' => 'Enable Google Analytics' ) ) )
            ->add('google_analytics_key', null, array( 'required' => false, 'attr' => array( 'data-postdesc' => 'The google analytics key' ) ) );
//
//        $builder->addEventListener(FormEvents::POST_SUBMIT, function(FormEvent $event){
//
//        });
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CoreSys\SiteBundle\Entity\Config'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'coresys_sitebundle_config';
    }
}
