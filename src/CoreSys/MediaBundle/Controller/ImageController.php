<?php

namespace CoreSys\MediaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CoreSys\MediaBundle\Entity\Image;
use CoreSys\SiteBundle\Controller\BaseController;

/**
 * Class ImageController
 * @package CoreSys\MediaBundle\Controller
 * @Route("/media/image")
 */
class ImageController extends BaseController
{
//    /**
//     * @Route("/show/o/{slug}", name="media_image_original", defaults={"slug"=null})
//     * @Template()
//     */
//    public function showImageOriginalAction( $slug )
//    {
//        exit;
//    }
//
//    /**
//     * @Route("/show/xxxx/{slug}", name="media_image_xxxxlarge", defaults={"slug"=null})
//     * @Template()
//     */
//    public function showImageXXXXLargeAction( $slug )
//    {
//        exit;
//    }
//
//    /**
//     * @Route("/show/xxx/{slug}", name="media_image_xxxlarge", defaults={"slug"=null})
//     * @Template()
//     */
//    public function showImageXXXLargeAction( $slug )
//    {
//        exit;
//    }
//
//    /**
//     * @Route("/show/xx/{slug}", name="media_image_xxlarge", defaults={"slug"=null})
//     * @Template()
//     */
//    public function showImageXXLargeAction( $slug )
//    {
//        exit;
//    }
//
//    /**
//     * @Route("/show/x/{slug}", name="media_image_xlarge", defaults={"slug"=null})
//     * @Template()
//     */
//    public function showImageXLargeAction( $slug )
//    {
//        exit;
//    }
//
//    /**
//     * @Route("/show/l/{slug}", name="media_image_large", defaults={"slug"=null})
//     * @Template()
//     */
//    public function showImageLargeAction( $slug )
//    {
//        exit;
//    }
//
//    /**
//     * @Route("/show/m/{slug}", name="media_image_medium", defaults={"slug"=null})
//     * @Template()
//     */
//    public function showImageMediumAction( $slug )
//    {
//        exit;
//    }
//
//    /**
//     * @Route("/show/s/{slug}", name="media_image_small", defaults={"slug"=null})
//     * @Template()
//     */
//    public function showImageSmallAction( $slug )
//    {
//        exit;
//    }
//
//    /**
//     * @Route("/show/thumb/{slug}", name="media_image_thumb", defaults={"slug"=null})
//     * @Template()
//     */
//    public function showImageThumbAction( $slug )
//    {
//        exit;
//    }
//
//    /**
//     * @Route("/show/tiny/{slug}", name="media_image_tiny", defaults={"slug"=null})
//     * @Template()
//     */
//    public function showImageTinyAction( $slug )
//    {
//        exit;
//    }
//
//    /**
//     * @Route("/show/teenie/{slug}", name="media_image_teenie", defaults={"slug"=null})
//     * @Template()
//     */
//    public function showImageTeenieAction( $slug )
//    {
//        exit;
//    }

    /**
     * @Route("/show/{size}/{slug}", name="media_image_show", defaults={"size"="original","slug"=null})
     * @Route("/show/original/{slug}", name="media_image_original", defaults={"size"="original", "slug"=null})
     * @Route("/show/xxxx/{slug}", name="media_image_xxxxlarge", defaults={"size"="xxxxlarge", "slug"=null})
     * @Route("/show/xxx/{slug}", name="media_image_xxxlarge", defaults={"size"="xxxlarge", "slug"=null})
     * @Route("/show/xx/{slug}", name="media_image_xxlarge", defaults={"size"="xxlarge", "slug"=null})
     * @Route("/show/x/{slug}", name="media_image_xlarge", defaults={"size"="xlarge", "slug"=null})
     * @Route("/show/l/{slug}", name="media_image_large", defaults={"size"="large", "slug"=null})
     * @Route("/show/m/{slug}", name="media_image_medium", defaults={"size"="medium", "slug"=null})
     * @Route("/show/s/{slug}", name="media_image_small", defaults={"size"="small", "slug"=null})
     * @Route("/show/thumb/{slug}", name="media_image_thumb", defaults={"size"="thumb", "slug"=null})
     * @Route("/show/tiny/{slug}", name="media_image_tiny", defaults={"size"="tiny", "slug"=null})
     * @Route("/show/teenie/{slug}", name="media_image_teenie", defaults={"size"="teenie", "slug"=null})
     * @Template()
     */
    public function showImageAction( $size, $slug )
    {
        exit;
    }
}
