<?php

namespace CoreSys\AdBundle\Twig;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use CoreSys\SiteBundle\Twig\BaseExtension;
use CoreSys\AdBundle\Entity\Ad;

/**
 * Common site extensions to be used in conjunction with
 * the CoreSys Bundles
 *
 * Class SiteExtension
 * @package CoreSys\AdBundle\Twig
 */
class AdExtension extends BaseExtension
{

    /**
     * @var string
     */
    protected $name = 'ad_extension';


    /**
     * @return array
     */
    public function getFilters()
    {
        return array();
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return array(
            'adCampaignsCount' => new \Twig_Function_Method($this, 'getAdCampaignsCount'),
        );
    }

    public function getAdCampaignsCount( $active = null )
    {
        $repo = $this->getRepo( 'CoreSysAdBundle:Campaign' );
        $count = $repo->getCount( $active );
        return $count;
    }


}