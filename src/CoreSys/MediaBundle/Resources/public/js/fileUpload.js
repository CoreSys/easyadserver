(function($,undefined){
    $.fn.CSFileUpload = function(element,options)
    {
        var csfu = this,
            element = element,
            $element = $(element),
            defaults = {
                url: 'javascript:void(0)',
                disableImageResize: /Android(?!.*Chrome)|Opera/.test(window.navigator.userAgent),
                maxFileSize: 50000000,
                acceptedFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
                redirect: null,
                autoUpload: false,
                xhrFields: {withCredentials: true},
                maxChunkSize: 500000,
                sequentialUploads: true
            };

        csfu.settings = {};
        csfu.fileupload = null;

        function init()
        {
            csfu.settings = $.extend({}, defaults, options);
            initFileUploader();
        }

        function initFileUploader()
        {
            csfu.fileupload = $element.fileupload(csfu.settings);

            if(typeof csfu.settings.onDone == 'function')
            {
                $element.bind('fileuploaddone', csfu.settings.onDone);
            }
        }

        init();
    };

    $.fn.csFileUpload = function(options) {
        return $(this).each(function(){
            var $t = $(this);
            if($t.data('csfileupload') === undefined) {
                var csfileupload = new $.fn.CSFileUpload($t,options);
                $t.data('csfileupload', csfileupload);
            }
        });
    };
})(jQuery);