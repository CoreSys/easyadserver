<?php

namespace CoreSys\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class AdminController
 * @package CoreSys\SiteBundle\Controller
 */
class AdminController extends BaseController
{
    /**
     * @var string
     */
    protected $type = 'admin';
    /**
     * @var bool
     */
    protected $ajax = false;
}
