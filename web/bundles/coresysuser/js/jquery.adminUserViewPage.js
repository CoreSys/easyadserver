;
(function ( $, undefined ) {
    $.fn.adminUserViewPage = function ( options ) {
        var p = this,
            defaults = {
                checkable: true,
                entity: 'Message',
                selected_count_selector: '.selected-count',
                check_master_selector: 'thead tr .master-check',
                check_row_selector: 'td .row-check',
                add_row_btn_selector: '.add-message',
                remove_rows_btn_selector: '.remove-messages',
                mark_rows_as_read_btn_selector: '.messages-mark-as-read',
                mark_rows_as_unread_btn_selector: '.messages-mark-as-unread',
                onToggleRemoveRowsBtn: onToggleRemoveRowsBtn,
                table_selector: '#messages-table',
                row_edit_btn_selector: '.btn-edit',
                row_view_btn_selector: '.btn-view',
                row_remove_btn_selector: '.btn-remove',
                dt_display_length: 10,
                new_modal_selector: '#new-modal',
                edit_modal_selector: '#edit-modal',
                view_modal_selector: '#view-modal',
                no_sort_columns: [0, 7],
                aSort: [
                    [6, 'desc']
                ],
                dataTables_aoColumns: [
                    {"sName": 'check', "bSortable": false, "bSearchable": false, "mData": 'check'},
                    {"sName": 'read', "bSearchable": false, "mData": 'read'},
                    {"sName": 'from', "mData": 'from'},
                    {"sName": 'to', "mData": 'to'},
                    {"sName": 'subject', "mData": 'subject'},
                    {"sName": 'msgs', "mData": 'msgs' },
                    {"sName": 'created_at', "bSearchable": false, "mData": 'created_at'},
                    {"sName": 'actions', "bSortable": false, "bSearchable": false, "mData": 'actions'}
                ],
                remove_row_url: Routing.generate( 'admin_ajax_user_messages_remove_thread' ),
                add_row_url: null, //Routing.generate('admin_ajax_users_new'),
                edit_row_url: null, //Routing.generate('admin_ajax_users_edit'),
                view_row_url: null, //Routing.generate('admin_ajax_users_view'),
                dataTables_ajax_url: Routing.generate( 'admin_ajax_user_messages_inbox_data_tables_slug' ),
                fnGetRowData: getRowData,
                fnPopulateEditedRowData: populateEditedRowData,
                onViewBtnClick: null,
                onDrawCallback: onDrawCallback,
                user_slug: null,
                compose_url: Routing.generate( 'admin_ajax_user_messages_new_admin_thread' ),
                reply_url: Routing.generate( 'admin_ajax_user_messages_reply_admin_thread' ),
                removeAllTrashSelector: 'span.remove-all-trash',
                undelete_url: Routing.generate( 'admin_ajax_user_messages_undelete_thread' )
            };

        p.settings = {};
        p.user_slug = null;
        p.markAsReadBtn = null;
        p.markAsUnReadBtn = null;
        p.page = null;
        p.viewModal = null;
        p.composeBtn = null;
        p.composeModal = null;
        p.removeAllTrashBtn = null;

        function init() {
            p.settings = $.extend( {}, defaults, options );
            p.user_slug = p.settings.user_slug || null;
            if ( p.user_slug !== null ) {
                p.settings.dataTables_ajax_url += '/' + p.user_slug;
                p.settings.remove_row_url += '/' + p.user_slug;
            }
            p.markAsReadBtn = $( p.settings.mark_rows_as_read_btn_selector );
            p.markAsUnReadBtn = $( p.settings.mark_rows_as_unread_btn_selector );
            p.page = new $.fn.adminAjaxTablePage( p.settings );
//            p.page.datatable.fnSort( [ [4,'desc'] ] );
            p.viewModal = $( '#view-message-modal' );

            p.markAsReadBtn.click( function ( e ) {
                markRowsAsRead( null );
            } );
            p.markAsUnReadBtn.click( function ( e ) {
                markRowsAsUnRead( null );
            } );

            p.page.removeRowsBtn.unbind( 'click' );
            p.page.removeRowsBtn.click( function ( e ) {
                removeRows( null );
            } );

            $( 'body' ).on( 'hidden.bs.modal', '.modal', function () {
                p.viewModal.children().remove();
                p.viewModal.html( '' );
                p.composeModal.children().remove();
                p.composeModal.html( '' );
            } );

            $( 'li.mailbox-switcher' ).click( function ( e ) {
                e.preventDefault();
                $( 'li.mailbox-switcher' ).removeClass( 'active' );
                $( this ).addClass( 'active' );
                switchMailbox( $( this ) );
            } );

            p.composeBtn = $( 'li.compose-btn' );
            p.composeModal = $( '#compose-modal' );

            p.composeBtn.click( showComposeModal );

            p.removeAllTrashBtn = $( p.settings.removeAllTrashSelector );
            p.removeAllTrashBtn.click( removeAllTrash );
        }

        function removeAllTrash( e ) {
            e.preventDefault();
            bootbox.confirm( 'Are you sure you want to empty the trash?', function ( res ) {
                if ( res === true ) {
                    var url = Routing.generate( 'admin_ajax_user_messages_empty_trash' );
                    if ( p.user_slug !== null && p.user_slug !== undefined ) {
                        url = Routing.generate( 'admin_ajax_user_messages_empty_trash_slug', {'slug': p.user_slug} );
                    }
                    $.ajax( {
                        url: url,
                        success: function ( res ) {
                            res = typeof res == 'object' ? res : JSON.parse( res );
                            if ( res.success ) {
                                toastr['success']( 'Successfully emptied trash', 'Success' );
                                p.refresh();
                            } else {
                                toastr['error']( 'Error emptying trash', 'Error' );
                            }
                        }
                    } );
                }
            } );
        }

        function showComposeModal( e ) {
            e.preventDefault();
            p.composeModal.modal( {'remote': p.settings.compose_url} );
        }

        function showReplyModal( tid ) {
            var url = p.settings.reply_url + '/' + tid;
            if ( p.user_slug !== undefined && p.user_slug !== null ) {
                url += '/' + p.user_slug;
            }
            p.composeModal.modal( {'remote': url} )
        }

        p.refresh = function () {
            p.page.reInitDatatables();
        };

        function switchMailbox( li ) {
            var $li = $( li ),
                url = $li.attr( 'data-url' ),
                title = $li.find( 'a[data-title]' ).attr( 'data-title' );

            if ( p.user_slug !== null ) {
                url += '/' + p.user_slug;
            }

            p.page.settings.dataTables_ajax_url = url;
            p.page.reInitDatatables();
            $( '.mailbox-title' ).html( title );
            $li.closest( '.tab-pane' ).attr( 'data-mailbox', $li.attr( 'data-type' ) );
            p.page.table.attr( 'data-mailbox', $li.attr( 'data-type' ) );
        }

        function markRowsAsRead( ids ) {
            if ( ids === undefined || ids === null ) {
                var ids = p.page.getSelectedRowsIds();
            }

            if ( p.user_slug !== null ) {
                var url = Routing.generate( 'admin_ajax_user_messages_mark_threads_as_read_slug', {'slug': p.user_slug} );
            } else {
                var url = Routing.generate( 'admin_ajax_user_messages_mark_threads_as_read' );
            }

            $.ajax( {
                url: url,
                type: 'POST',
                data: {'ids': ids},
                success: function ( res ) {
                    res = JSON.parse( res );
                    if ( res.success ) {
                        toastr['success']( res.msg, 'Success' );
                        $.each( res.data.ids, function ( idx, id ) {
                            var $row = p.page.table.find( 'tbody tr[data-id=' + id + ']' );
                            if ( $row.length > 0 ) {
                                if ( $row.hasClass( 'unread' ) ) {
                                    decrementInboxCount();
                                }
                                $row.removeClass( 'unread' ).addClass( 'read' );
                                $row.find( 'td' ).eq( 1 ).html( '<i class="icon-mail-read"></i>' );
                            }
                        } );
                    } else {
                        toastr['error']( res.msg, 'Error' );
                    }
                }
            } );
        }

        function markRowsAsUnRead( ids ) {
            if ( ids === undefined || ids === null ) {
                var ids = p.page.getSelectedRowsIds();
            }

            if ( p.user_slug !== null ) {
                var url = Routing.generate( 'admin_ajax_user_messages_mark_threads_as_unread_slug', {'slug': p.user_slug} );
            } else {
                var url = Routing.generate( 'admin_ajax_user_messages_mark_threads_as_unread' );
            }

            $.ajax( {
                url: url,
                type: 'POST',
                data: {'ids': ids},
                success: function ( res ) {
                    res = JSON.parse( res );
                    if ( res.success ) {
                        toastr['success']( res.msg, 'Success' );
                        $.each( res.data.ids, function ( idx, id ) {
                            var $row = p.page.table.find( 'tbody tr[data-id=' + id + ']' );
                            if ( $row.length > 0 ) {
                                if ( $row.hasClass( 'read' ) ) {
                                    addInboxCount();
                                }
                                $row.addClass( 'unread' ).removeClass( 'read' );
                                $row.find( 'td' ).eq( 1 ).html( '<i class="icon-mail-unread"></i>' );
                            }
                        } );
                    } else {
                        toastr['error']( res.msg, 'Error' );
                    }
                }
            } );
        }

        function markRowAsRead( row ) {
            var $row = $( row ),
                id = $row.attr( 'data-id' ),
                ids = [];
            ids.push( id );
            markRowsAsRead( ids );
        }

        function markRowAsUnRead( row ) {
            var $row = $( row ),
                id = $row.attr( 'data-id' ),
                ids = [];
            ids.push( id );
            markRowsAsUnRead( ids );
        }

        function onToggleRemoveRowsBtn( t, show ) {
            if ( show ) {
                p.markAsReadBtn.show();
                p.markAsUnReadBtn.show();
            } else {
                p.markAsReadBtn.hide();
                p.markAsUnReadBtn.hide();
            }
        }

        function addInboxCount() {
            var cnt = getInboxCount();
            cnt++;
            updateInboxCounts( cnt );
        }

        function decrementInboxCount() {
            var cnt = getInboxCount();
            cnt--;
            updateInboxCounts( cnt );
        }

        function updateInboxCounts( cnt ) {
            cnt = parseInt( cnt );
            cnt = cnt <= 0 ? 0 : cnt;
            $( '.inbox-count' ).each( function () {
                $( this ).html( cnt );
            } );
        }

        function getInboxCount() {
            var cnt = null;
            $( '.inbox-count' ).each( function () {
                if ( cnt === null ) {
                    cnt = parseInt( $( this ).text() );
                }
            } );

            return cnt !== null ? cnt : 0;
        }

        function undeleteRow( tid ) {
            if ( tid === undefined || tid === null ) {
                toastr['error']( 'No thread selected', 'Error' );
            }
            bootbox.confirm( 'Are you sure?', function () {
                var url = p.settings.undelete_url;

                if ( p.user_slug !== null ) {
                    url += '/' + p.user_slug;
                }

                url += '/' + tid;

                $.ajax( {
                    url: url,
                    data: {'id': tid},
                    type: 'post',
                    dataType: 'json',
                    success: function ( res ) {
                        res = typeof res == 'object' ? res : JSON.parse( res );
                        if ( res.success ) {
                            toastr['success']( res.msg, 'Success' );
                            var $row = p.page.table.find('tbody tr[data-id=' + tid + ']');
                                row = $row[0],
                                idx = p.page.datatable.fnGetPosition(row);
                            p.page.datatable.fnDeleteRow(idx);
                        } else {
                            toastr['error']( res.msg, 'Error' );
                        }
                    }
                } );
            } );
        }

        function removeRows( ids ) {
            if ( ids === undefined || ids === null ) {
                var ids = p.page.getSelectedRowsIds();
            }

            if ( p.user_slug !== null ) {
                var url = Routing.generate( 'admin_ajax_user_messages_remove_thread_slug', {'slug': p.user_slug} );
            } else {
                var url = Routing.generate( 'admin_ajax_user_messages_remove_thread' );
            }

            $.ajax( {
                url: url,
                type: 'POST',
                data: {'ids': ids},
                success: function ( res ) {
                    res = JSON.parse( res );
                    if ( res.success ) {
                        toastr['success']( res.msg, 'Success' );
                        $.each( res.data.ids, function ( idx, id ) {
                            var $row = p.page.table.find( 'tbody tr[data-id=' + id + ']' );
                            if ( $row.length > 0 ) {
                                var read = $row.hasClass( 'read' ),
                                    pos = p.page.datatable.fnGetPosition( $row[0] );
                                p.page.datatable.fnDeleteRow( pos );
                                if ( !read ) {
                                    decrementInboxCount();
                                }
                            }
                        } );
                    } else {
                        toastr['error']( res.msg, 'Error' );
                    }
                }
            } );
        }

        function viewMessage( slug, tid, mid ) {
            var url = Routing.generateUrl( '', {'slug': slug, 'tid': tid} );
            p.viewModal.modal( {'remote': url} );
        }

        function onDrawCallback( t ) {
            // since this is separate from the normal page operations
            // we have a few things to do here
            t.table.find( 'tbody tr' ).each( function () {
                var $row = $( this ),
                    read = $row.find( '[data-read]' ).attr( 'data-read' ) == 1;
                $row.addClass( read ? 'read' : 'unread' );

                var read = $row.find( '.btn-mark-as-read' ),
                    unread = $row.find( '.btn-mark-as-unread' ),
                    remove = $row.find( '.btn-remove' ),
                    reply = $row.find( '.btn-reply' ),
                    undelete = $row.find( '.btn-undelete' );

                read.unbind( 'click' );
                read.click( function ( e ) {
                    markRowAsRead( $row );
                } );

                unread.unbind( 'click' );
                unread.click( function ( e ) {
                    markRowAsUnRead( $row );
                } );

                remove.unbind( 'click' );
                remove.click( function ( e ) {
                    removeRow( $row );
                } );

                $row.dblclick( function () {
                    console.log( 'Double clicked row' );
                } );

                reply.unbind( 'click' );
                reply.click( function ( e ) {
                    var tid = $row.attr( 'data-id' );
                    showReplyModal( tid );
                } );

                undelete.unbind( 'click' );
                undelete.click( function ( e ) {
                    var tid = $row.attr( 'data-id' );
                    undeleteRow( tid );
                } );
            } );
        };

        function removeRow( row ) {
            var $row = $( row ),
                id = $row.attr( 'data-id' ),
                ids = [];
            ids.push( id );
            removeRows( ids );
        }

        function getRowData( data ) {
            return data;
        };

        function populateEditedRowData( row, data ) {
            p.page.datatable.fnReloadAjax();
            p.page.resetRowSelects();
        };

        init();

        return p;
    };
})( jQuery );