<?php
/**
 * This file is part of the CoreSysUserBundle package.
 * (c) J&L Core Systems http://jlcoresystems.com | http://joshmccreight.com
 */

namespace CoreSys\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\Validator\Constraint\UserPassword;
use FOS\MessageBundle\FormType\NewThreadMessageFormType as BaseType;

/**
 * Class ThreadType
 * @package CoreSys\UserBundle\Form
 */
class ThreadType extends BaseType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('recipient', 'entity', array(
                'label' => 'To',
                'class' => 'CoreSysUserBundle:User',
                'attr' => array('class' => 'form-control select2', 'data-postdesc' => 'Who you are sending this message to')
            ))
            ->add('subject', 'text')
            ->add('body', 'textarea')
            ->add('createdBy', 'entity', array(
                'label' => 'From',
                'class' => 'CoreSysUserBundle:User',
                'attr' => array('class' => 'form-control select2', 'data-postdesc' => 'Who is this message coming from?')
            ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'intention'  => 'new_thread',
        ));
    }

    public function getName()
    {
        return 'core_sys_user_message_new_thread';
    }
}