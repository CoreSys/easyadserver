<?php

namespace CoreSys\MediaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Image
 *
 * @ORM\Table(name="media_image")
 * @ORM\Entity(repositoryClass="CoreSys\MediaBundle\Entity\ImageRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Image
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=128)
     */
    private $title;
    /**
     * @var integer
     *
     * @ORM\Column(name="views", type="integer")
     */
    private $views;
    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;
    /**
     * @var string
     *
     * @ORM\Column(name="filename", type="string", length=255)
     */
    private $filename;
    /**
     * @var string
     *
     * @ORM\Column(name="ext", type="string", length=12)
     */
    private $ext;
    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer")
     */
    private $position;
    /**
     * @var string
     *
     * @ORM\Column(name="header", type="string", length=64)
     */
    private $header;
    /**
     * @var boolean
     *
     * @ORM\Column(name="apply_watermark", type="boolean")
     */
    private $applyWatermark;
    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=32)
     */
    private $type;
    /**
     * @var array
     *
     * @ORM\Column(name="sizes", type="array")
     */
    private $sizes;
    /**
     * @var integer
     *
     * @ORM\Column(name="width", type="integer")
     */
    private $width;
    /**
     * @var integer
     *
     * @ORM\Column(name="height", type="integer")
     */
    private $height;
    /**
     * @var float
     *
     * @ORM\Column(name="ratio", type="float")
     */
    private $ratio;
    /**
     * @var string
     *
     * @ORM\Column(name="sub_folder", type="string", length=255,nullable=true)
     */
    private $subFolder;
    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255)
     */
    private $slug;
    /**
     * @var mixed
     */
    private $file;
    /**
     * @var string
     *
     * @ORM\Column(name="public_base", type="string", length=128, nullable=true)
     */
    private $public_base;

    /**
     *
     */
    public function __construct()
    {
        $this->setCreatedAt(new \DateTime());
        $this->setPosition(0);
        $this->setViews(0);
        $this->setActive(true);
        $this->setApplyWatermark( true );
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param mixed $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get views
     *
     * @return integer
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * Set views
     *
     * @param integer $views
     * @return Image
     */
    public function setViews($views)
    {
        $this->views = $views;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Image
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return Image
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get header
     *
     * @return string
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * Set header
     *
     * @param string $header
     * @return Image
     */
    public function setHeader($header)
    {
        $this->header = $header;

        return $this;
    }

    /**
     * Get applyWatermark
     *
     * @return boolean
     */
    public function getApplyWatermark()
    {
        return $this->applyWatermark;
    }

    /**
     * Set applyWatermark
     *
     * @param boolean $applyWatermark
     * @return Image
     */
    public function setApplyWatermark($applyWatermark)
    {
        $this->applyWatermark = $applyWatermark;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Image
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get width
     *
     * @return integer
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set width
     *
     * @param integer $width
     * @return Image
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Get height
     *
     * @return integer
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set height
     *
     * @param integer $height
     * @return Image
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get ratio
     *
     * @return float
     */
    public function getRatio()
    {
        return $this->ratio;
    }

    /**
     * Set ratio
     *
     * @param float $ratio
     * @return Image
     */
    public function setRatio($ratio)
    {
        $this->ratio = $ratio;

        return $this;
    }

    /**
     * Get subFolder
     *
     * @return string
     */
    public function getSubFolder()
    {
        return $this->subFolder;
    }

    /**
     * Set subFolder
     *
     * @param string $subFolder
     * @return Image
     */
    public function setSubFolder($subFolder)
    {
        $this->subFolder = $subFolder;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Image
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return bool
     */
    public function isJpg()
    {
        $ext = $this->getExt();
        $ext = strtolower(trim($ext));
        return $ext == 'jpg' || $ext == 'jpeg';
    }

    /**
     * Get ext
     *
     * @return string
     */
    public function getExt()
    {
        return $this->ext;
    }

    /**
     * Set ext
     *
     * @param string $ext
     * @return Image
     */
    public function setExt($ext)
    {
        $this->ext = strtolower(trim($ext));

        return $this;
    }

    /**
     * @return bool
     */
    public function isGif()
    {
        $ext = $this->getExt();
        $ext = strtolower(trim($ext));
        return $ext == 'gif';
    }

    /**
     * @return bool
     */
    public function isPng()
    {
        $ext = $this->getExt();
        $ext = strtolower(trim($ext));
        return $ext == 'png';
    }

    /**
     * @return bool
     */
    public function isBmp()
    {
        $ext = $this->getExt();
        $ext = strtolower(trim($ext));
        return $ext == 'bmp' || $ext == 'wbmp';
    }

    /**
     * @param null $base
     */
    public function getAbsoluteDatedFolder($base = null)
    {
    }

    public function getRelativeDatedFolder()
    {
        return $this->getDatedFolder(null);
    }

    public function getDatedFolder($base = null)
    {
        if (substr($base, -1) == DIRECTORY_SEPARATOR) {
            $base = substr($base, 0, strlen($base) - 1);
        }

        $date = $this->getCreatedAt();
        $folders = array(
            $date->format('Y'),
            $date->format('m'),
            $date->format('d'),
            $date->format('H')
        );

        $folder = $base;
        foreach ($folders as $f) {
            $folder .= DIRECTORY_SEPARATOR . $f;
        }

        return $folder;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Image
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getSizeFile($size, $base = null)
    {
        $size = strtolower(trim($size));
        if ($size == 'original' || $size == 'orig') {
            $file = $this->getDatedFolder($base) . DIRECTORY_SEPARATOR . $this->getFilename();
            return $file;
        }

        $valid = $this->getSizes(false);
        if (!in_array($size, $valid)) {
            return null;
        }

        $file = $this->getSizeFolder($size, $base) . DIRECTORY_SEPARATOR . $this->getFilename();

        return $file;
    }

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set filename
     *
     * @param string $filename
     * @return Image
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get sizes
     *
     * @param boolean $full
     * @return array
     */
    public function getSizes($full = true)
    {
        if (!$full) {
            $return = array();
            foreach ($this->sizes as $key => $width) {
                $return[] = $key;
            }
            return $return;
        }
        return $this->sizes;
    }

    /**
     * Set sizes
     *
     * @param array $sizes
     * @return Image
     */
    public function setSizes($sizes)
    {
        $this->sizes = $sizes;

        return $this;
    }

    public function getSizeFolder($size, $base = null)
    {
        $folder = $this->getDatedFolder($base);
        if (empty($size)) {
            return $folder;
        }

        $size = strtolower(trim($size));
        $valid = $this->getSizes(false);
        if (!in_array($size, $valid)) {
            return $folder;
        }

        $folder .= DIRECTORY_SEPARATOR . $size;

        return $this->verifyFolder($folder);
    }

    public function verifyFolder($folder = null)
    {
        if (!empty($folder)) {
            @mkdir($folder);
            @chmod($folder, 0777);
        }
        return $folder;
    }

    /**
     * @return mixed
     */
    public function getPublicBase()
    {
        return $this->public_base;
    }

    /**
     * @param mixed $public_base
     */
    public function setPublicBase($public_base = null)
    {
        $this->public_base = $public_base;
    }

    public function hasSize($size = null)
    {
        $size = strtolower(trim($size));
        if (empty($size) || $size == 'orig' || $size == 'original') {
            return true;
        }
        $sizes = $this->getSizes(false);
        return in_array($size, $sizes);
    }

    /**
     * @ORM\PrePersist
     */
    public function prepersist()
    {
        $title = $this->getTitle();
        if (empty($title)) {
            $title = $this->getFilename();
            $this->setTitle($title);
        }

        $header = $this->getHeader();
        if( empty( $header ) ) {
            $header = 'Content-Type: ' . $this->getType();
            $this->setHeader( $header );
        }

        $slug = $this->getSlug();
        if( empty( $slug ) ) {
            $slug = $this->getFilename();
            $slug = str_replace( '.' . $this->getExt(), '', $slug );
            $this->setSlug( $slug );
        }
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Image
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    public function getSizeUrl( $size = null )
    {
        $file = 'media_files/photos/';
        $file .= $this->getSizeFile( $size, null );
        $file = str_replace( '\\', '/', $file );
        $file = str_replace( '//', '/', $file );

        return $file;
    }

    public function getUrl( $size = null, $prefix = null )
    {
        return $prefix . $this->getSizeUrl( $size );
    }

    public function toArray()
    {
        $return = array(
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'type' => $this->getType(),
            'filename' => $this->getFilename(),
            'sizes' => $this->getSizes(),
            'active' => $this->getActive()
        );

        $sizes = $this->getSizes( false );
        $sizes[] = 'original';

        foreach( $sizes as $size ) {
            $url = $this->getSizeUrl( $size );
            $return[ $size ] = $url;
        }

        return $return;
    }

    public function getSizeDimensions( $size = null )
    {
        if( empty( $size ) || $size == 'original' || $size == 'orig') {
            return array( 'width' => $this->getWidth(), 'height' => $this->getHeight() );
        }
        $sizes = $this->getSizes();
        if( isset( $sizes[ $size ] ) ) {
            $ratio = $this->getRatio();
            $width = $sizes[ $size ];
            $height = intval( $width * $ratio );
            return array( 'width' => $width, 'height' => $height );
        }

        return null;
    }

    public function getSizesWithDimensions( $reverse = true )
    {
        $return = array(
            'original' => $this->getSizeDimensions( 'original' )
        );
        $ratio = $this->getRatio();

        $sizes = $this->getSizes();
        if($reverse) {
            $sizes = array_reverse( $sizes );
        }

        foreach( $sizes as $size => $width )
        {
            $dims = $this->getSizeDimensions( $size );
            if( !empty( $dims ) ) {
                $return[ $size ] = $dims;
            }
        }

        return $return;
    }
}
