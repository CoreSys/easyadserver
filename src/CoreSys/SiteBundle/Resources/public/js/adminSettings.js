;(function($,undefined){
    $.fn.settingsPage = function(options) {
        var t = this,
            $this = $(this),
            defaults = {
                form_selector: '#settings-form',
                save_selector: '.save-settings',
                remove_selector: '.remove-selected',
                image_item_selector: '.image-item',
                image_list_selector: '#images-list',
                isotope_container_selector: '#images-list',
                isotope_item_selector: '.image-item',
                isotope_layout: 'fitRows',
                image_view_modal_selector: '#imageinfo',
                fancybox_selector: '.fancybox',
                fileupload_selector: '#fileupload'
            },
            removeCheckOverride = false;

        t.settings = {};
        t.form = null;
        t.errorEl = null;
        t.imageList = null;
        t.isotope_container = null;
        t.image_view_modal = null;
        t.removeButton = null;
        t.fileupload = null;

        function init() {
            t.settings = $.extend({},defaults,options);
            t.form = $(t.settings.form_selector);
            t.imageList = $(t.settings.image_list_selector);
            t.isotope_container = $(t.settings.isotope_container_selector);
            t.image_view_modal = $(t.settings.image_view_modal_selector);
            t.removeButton = $(t.settings.remove_selector);
            t.fileupload = $(t.settings.fileupload_selector);

            if(typeof loadPostDesc == 'function' ) {
                loadPostDesc('auto',true);
                loadDropTabs();
            }
            t.loadFormIcons();
            t.initFormValidation();
            t.initSaveButton();
            t.registerImageListClicks();
            t.initIsotope();
            t.registerViewClicks();
            t.initFancybox();
            t.registerSelectClicks();
            t.checkSelectedImages();

            t.removeButton.click(t.removeSelected);

            t.initCSFileUpload();

            $('body').on('hidden.bs.modal', '.modal', function () {
                $(this).removeData('bs.modal');
            });

            setTimeout(function(){t.initIsotope();},750);

            if($(t.settings.isotope_container_selector).is(':hidden')) {
                $('a[href="#lag"]').click(function(){
                    t.initIsotope();
                });
            }
        }

        t.initCSFileUpload = function()
        {
            $('#fileupload').fileupload({
                url: Routing.generate( 'admin_site_settings_upload_image' ),
                sequentialUploads: false,
                disableImageResize: /Android(?!.*Chrome)|Opera/.test(window.navigator.userAgent),
                maxFileSize: 50000000,
                acceptedFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
                redirect: null,
                autoUpload: false,
                xhrFields: {withCredentials: true},
                maxChunkSize: 1500000,
                disableImageResize: true
            }).bind('fileuploaddone', function(e,data) {
                    setTimeout(function(){
                        var name = data.jqXHR.responseJSON.files[0].name;

                        $.ajax({
                            url: Routing.generate( 'admin_settings_ajax_process_image'),
                            type: 'POST',
                            data: {'file': name},
                            success:function(res) {
                                if(res != '' && res !== undefined && res !== null) {
                                    res = JSON.parse(res);
                                    if(res.success) {
                                        t.addImageItem( res.data );
                                    } else {
                                        toaster['error'](res.msg,'Error');
                                    }
                                }
                            },
                            failed: function(e) {

                            }
                        });
                    },1000);
                });
        };

        t.addImageItem = function(image) {
            var $container = $('#images-list'),
                $wrapper = $('<div class="image-item" data-id="' + image.id + '" />'),
                $a = $('<a class="fancybox" rel="site-images" title="' + image.title + '" href="' + AssetsHelper.getUrl( image.original ) + '" />'),
                $img = $('<img data-placement="top" title="' + image.title + '" data-original-title="' + image.id + '" class="img-responsive tooltips img-rounded img-' + image.id + '" src="' + AssetsHelper.getUrl( image.thumb ) + '" alt="' + image.title + '" style="margin: 0 auto" />'),
                $tr = $('<div class="detail tr" />'),
                $btngrp = $('<div class="btn-group" />'),
                $btn = $('<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" />'),
                $btnicon = $('<i class="fa fa-cogs" />'),
                $dd = $('<ul class="dropdown-menu pull-right" />'),
                $liview = $('<li><a title="View/Edit Image Information" data-action="view" data-id="' + image.id + '" class="" href="javascript:void(0);"><i class="fa fa-search"></i> View/Edit Image Info</a></li>'),
                $liedit = $('<li><a title="Edit Image Information" data-action="edit" data-id="' + image.id + '" class="" href="javascript:void(0);"><i class="fa fa-edit"></i> Edit Image Info</a></li>'),
                $lirem = $('<li><a title="Remove this Image" data-action="remove" data-id="' + image.id + '" class="" href="javascript:void(0);"><i class="fa fa-trash-o"></i> Remove Image</a></li>'),
                $tl = $('<div class="detail tl" title="Select this Image"><a data-action="select" data-id="' + image.id + '" class="btn btn-warning" href="javascript:void(0);"><i class="fa fa-check"></i></a></div>');

            $wrapper.append( $a).append( $tr).append( $tl);
            $a.append( $img);
            $tr.append( $btngrp);
            $btngrp.append($btn).append($dd);
            $btn.append($btnicon);
            $dd.append($liview).append($lirem);
            $container.append($wrapper);

            t.registerImageRemoveClick( $wrapper.find('[data-action="remove"]'));
            t.registerViewClick( $wrapper.find('[data-action="view"]'));

            $container.isotope( 'appended', $wrapper );
            t.registerSelectClick( $tl.find('a') );
            t.initFancybox();
            t.checkSelectedImages();
        };

        t.removeSelected = function()
        {
            bootbox.confirm("Are you sure you want to remove these images?", function(result){
                if(result === true || result == 'ok') {
                    var selected = t.getSelectedImages();
                    removeCheckOverride = true;
                    $.each(selected,function(ix,item){
                        var $item = $(item),
                            $del = $item.find('[data-action=remove]');
                        if($del.length > 0) {
                            $del.click();
                        }
                    });
                    removeCheckOverride = false;
                    t.checkSelectedImages();
                }
            });
        };

        t.checkSelectedImages = function()
        {
            if(t.countSelectedImages() > 0) {
                t.removeButton.show();
            }else {
                t.removeButton.hide();
            }
        };

        t.countSelectedImages = function()
        {
            var selected = t.getSelectedImages();
            return selected.length;
        };

        t.getSelectedImages = function()
        {
            return t.imageList.find(t.settings.isotope_item_selector + '.selected');
        };

        t.registerSelectClicks = function()
        {
            $('[data-action="select"]').each(function(){
                t.registerSelectClick($(this));
            });
        };

        t.registerSelectClick = function(target)
        {
            target.unbind( 'click' );
            target.click(function(){
                var item = $(this).closest('.image-item');
                if(item.hasClass('selected')) {
                    item.removeClass('selected');
                    $(this).addClass('btn-warning').removeClass('btn-danger');
                    $(this).find('i').removeClass('fa-minus').addClass('fa-check');
                    $(this).parent().attr('title', 'Select this Image');
                } else {
                    item.addClass('selected');
                    $(this).addClass('btn-danger').removeClass('btn-warning');
                    $(this).find('i').removeClass('fa-check').addClass('fa-minus');
                    $(this).parent().attr('title', 'UnSelect this Image');
                }
                t.checkSelectedImages();
            });
        };

        t.initFancybox = function()
        {
            $(t.settings.fancybox_selector).fancybox();
        };

        t.registerViewClicks = function()
        {
            t.imageList.find('[data-action="view"]').each(function(){
                t.registerViewClick( $(this) );
            });
        };

        t.registerViewClick = function(target)
        {
            var id = target.attr( 'data-id' );
            target.unbind('click');
            target.click(function(e){
                e.preventDefault();
                t.image_view_modal.modal({remote: Routing.generate('admin_media_ajax_image_view',{'id':id})});
            });
        };

        t.registerImageListClicks = function()
        {
            t.registerRemoveClicks();
        };

        t.registerRemoveClicks = function()
        {
            t.imageList.find('[data-action="remove"]').each(function(){
                t.registerImageRemoveClick( $(this) );
            });
        };

        t.registerImageRemoveClick = function(target)
        {
            var id = target.attr('data-id');
            target.unbind( 'click' );
            target.click(function(e){
                e.preventDefault();
                if(removeCheckOverride) {
                    t.removeImage( id );
                } else {
                    bootbox.confirm("Are you sure you want to remove this image?", function(res){
                        if(res === true) {
                            t.removeImage(id);
                        }
                    });
                }
            });
        };

        t.removeImage = function(id)
        {
            $.ajax({
                url: Routing.generate( 'admin_site_settings_ajax_remove_image', {'id':id}),
                success: function(res) {
                    res = JSON.parse(res);
                    if(res.success) {
                        toastr['success']('Successfully removed image', 'Success' );
                        var id = res.data.id;
                        $('.image-item[data-id=' + id + ']').remove();
                        t.initIsotope();
                        t.initFancybox();
                        t.checkSelectedImages();
                    }
                }
            });
        };

        t.initIsotope = function()
        {
            setTimeout(function(){
                t.isotope_container.isotope({
                    itemSelector: t.settings.isotope_item_selector,
                    layoutMode: t.settings.isotope_layout
                });
            },750);
        };

        t.initSaveButton = function()
        {
            $(t.settings.save_selector).click(function(e){
                e.preventDefault();
                t.form.submit();
            }).removeClass('display-hide');
        };

        t.initFormValidation = function()
        {
            t.errorEl = t.form.find('.alert-danger');

            t.form.validate({
                errorElement: 'span',
                errorClass: 'help-block',
                focusInvalid: false,
                ignore: '',
                invalidHandler: function(event,validator) {
                    var errors = '';
                    $.each(validator.errorList,function(ix,item){
                        var $item = $(item.element),
                            error = $item.attr( 'data-error-msg' );

                        if(error.length) {
                            if(errors.length) {
                                errors += '<br>' + error;
                            } else {
                                errors = error;
                            }
                        }
                    });
                    toastr['error'](errors, 'Form Errors');
                },
                errorPlacement: function(error,element) {
                    var icon = $(element).parent('.input-icon').children('i');
                    icon.css('color','red');
                    icon.removeClass('fa-check').addClass('fa-warning');
                    icon.attr('data-original-title', error.text()).tooltip({'container':'body'});
                },
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error').removeClass('has-success').removeClass('has-warning');
                },
                success: function (label, element) {
                    var icon = $(element).parent('.input-icon').children('i');
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    icon.removeClass("fa-warning").addClass("fa-check");
                    icon.css('color','green');
                },
                submitHandler: function (form) {
                    toastr['success']('All form fields are valid','Valid Form');
                    form.submit();
                }
            });
        };

        t.loadFormIcons = function()
        {
            t.form.find('input').each(function(){
                var $this = $(this),
                    $parent = $this.parent();
                if($this.attr('data-skip-icon') === undefined) {
                    if($this.attr('type') !== 'checkbox') {
                        $parent.addClass('input-icon right');
//                        var $icon = $parent.find('.fa');
//                        if($icon.length) {
//                            $icon = $('<i class="fa" />');
//                            $this.before($icon);
//                        }
                        var caddon = $parent.parent().find('.checkbox-addon');
                        if(caddon.length > 0) {
                            $parent.prepend(caddon);
                            $parent.append($parent.find('.input-group-addon').not('.checkbox-addon'));
                            $parent.find('.fa-envelope').css('top','0px');
                        }
                    }
                }
            });
        };

        init();
        return this;
    };

    var _oldShow = $.fn.show;

    $.fn.show = function(speed, oldCallback) {
        return $(this).each(function() {
            var obj         = $(this),
                newCallback = function() {
                    if ($.isFunction(oldCallback)) {
                        oldCallback.apply(obj);
                    }
                    obj.trigger('afterShow');
                };

            // you can trigger a before show if you want
            obj.trigger('beforeShow');

            // now use the old function to show the element passing the new callback
            _oldShow.apply(obj, [speed, newCallback]);
        });
    }
})(jQuery);