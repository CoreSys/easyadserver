<?php
/**
 * This file is part of the CoreSysUserBundle package.
 * (c) J&L Core Systems http://jlcoresystems.com | http://joshmccreight.com
 */

namespace CoreSys\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use CoreSys\NoteBundle\Entity\Note;
use FOS\MessageBundle\Model\ParticipantInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="CoreSys\UserBundle\Entity\UserRepository")
 * @ORM\HasLifecycleCallbacks
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"user" = "User"})
 */
class User extends BaseUser implements ParticipantInterface, EquatableInterface
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @var string
     */
    protected $type = 'user';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    protected $created_at;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updated_at;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=32, nullable=true)
     */
    protected $first_name;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=32, nullable=true)
     */
    protected $last_name;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="CoreSys\NoteBundle\Entity\Note")
     * @ORM\OrderBy({"created_at"="DESC"})
     */
    protected $notes;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Role", inversedBy="users")
     * @ORM\JoinTable(name="user_roles")
     */
    protected $sys_roles;

    /**
     * @var text
     *
     * @ORM\Column(name="bio", type="text", nullable=true)
     */
    protected $bio;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_gravatar", type="boolean", nullable=true)
     */
    protected $use_gravatar;

    /**
     * @var gravatar_email
     * @ORM\Column(name="gravatar_email", type="string", length=128, nullable=true)
     */
    protected $gravatar_email;

    /**
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->setCreatedAt( new \DateTime() );
        $this->setUpdatedAt( new \DateTime() );
        $this->setNotes( new ArrayCollection() );
        $this->setLastLogin( new \DateTime() );
        $this->setSysRoles( new ArrayCollection() );
        $this->setUseGravatar( FALSE );
    }

    /**
     * @return text
     */
    public function getBio()
    {
        return $this->bio;
    }

    /**
     * @param null $bio
     *
     * @return $this
     */
    public function setBio( $bio = NULL )
    {
        $this->bio = $bio;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param Role $role
     *
     * @return $this
     */
    public function addSysRole( Role $role = NULL )
    {
        if ( !$this->sys_roles->contains( $role ) ) {
            $this->sys_roles->add( $role );
        }

        return $this;
    }

    /**
     * @param Role $role
     *
     * @return $this
     */
    public function removeSysRole( Role $role = NULL )
    {
        if ( $this->sys_roles->contains( $role ) ) {
            $this->sys_roles->removeElement( $role );
        }

        return $this;
    }

    /**
     * Get updated_at
     *
     * @param string $format
     *
     * @return \DateTime
     */
    public function getUpdatedAt( $format = NULL )
    {
        if ( !empty( $format ) ) {
            return $this->updated_at->format( $format );
        }

        return $this->updated_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     *
     * @return User
     */
    public function setUpdatedAt( $updatedAt )
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * @param bool $active
     *
     * @return $this
     */
    public function setActive( $active = TRUE )
    {
        $this->setEnabled( $active );

        return $this;
    }

    /**
     * @return bool
     */
    public function getActive()
    {
        // first off, active really means that this user is both enabled
        // and has a non expired subscription
        $enabled = $this->enabled === TRUE;

        return $enabled;
//        $active = $this->getActiveSubscription();
        if ( empty( $active ) ) {
            return FALSE;
        }

        $expired = $active->getExpired();
        if ( $expired ) {
            return FALSE;
        }

        // active subscription
        return $enabled;
    }

    /**
     * @return ArrayCollection
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * @param $notes
     *
     * @return $this
     */
    public function setNotes( $notes )
    {
        if ( empty( $notes ) ) {
            $notes = new ArrayCollection();
        }
        $this->notes = $notes;

        return $this;
    }

    /**
     * @param Note $note
     *
     * @return $this
     */
    public function addNote( Note $note )
    {
        if ( !$this->notes->contains( $note ) ) {
            $this->notes->add( $note );
        }

        return $this;
    }

    /**
     * @param Note $note
     *
     * @return $this
     */
    public function removeNote( Note $note )
    {
        if ( $this->notes->contains( $note ) ) {
            $this->notes->removeElement( $note );
        }

        return $this;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function prepersist()
    {
        // lets sync up the sys roles to the user roles
        $sys_roles = $this->getSysRoles();

        try {
            foreach ( $sys_roles as $role ) {
                $rname = $role->getRoleName();
                $this->addRole( $rname );
            }
        } catch ( \Exception $e ) {
            $sys_roles = array();
        }

//        print_r( $this->getRoles() );

        // lets remove any roles which are no longer part of this user
        foreach ( $this->getRoles() as $role ) {
            if ( FALSE == ( $sys_role = $this->hasSysRole( $role ) ) ) {
                // this role is not in the sys roles
                $this->removeRole( $role );
            }
        }

        if ( $this->getUseGravatar() === TRUE ) {
            $gemail = $this->getGravatarEmail();
            if ( empty( $gemail ) ) {
                $email = $this->getEmail();
                $this->setGravatarEmail( $email );
            }
        }

    }

    /**
     * @return ArrayCollection
     */
    public function getSysRoles()
    {
        if ( empty( $this->sys_roles ) ) {
            $this->sys_roles = array();
        }

        return $this->sys_roles;
    }

    /**
     * @param $roles
     *
     * @return $this
     */
    public function setSysRoles( $roles )
    {
        $this->sys_roles = $roles;

        return $this;
    }

    /**
     * @param null $role_name
     *
     * @return bool
     */
    public function hasSysRole( $role_name = NULL )
    {
        if ( $role_name instanceof Role ) {
            return $this->hasSpecificSysRole( $role_name );
        }
        foreach ( $this->getSysRoles() as $sys_role ) {
            if ( $sys_role->getName() == $role_name || $sys_role->getRoleName() == $role_name ) {
                return $sys_role;
            } else {
                $res = $this->parentHasSysRole( $sys_role, $role_name );
                if ( $res !== FALSE ) {
                    return $res;
                }
            }
        }

        return FALSE;
    }

    /**
     * @param $role_name
     *
     * @return bool
     */
    public function hasSpecificSysRole( $role_name )
    {
        if ( $role_name instanceof Role ) {
            return $this->hasRoleSysRole( $role_name );
        }

        foreach ( $this->getSysRoles() as $sys_role ) {
            if ( $sys_role->getName() == $role_name || $sys_role->getRoleName() == $role_name ) {
                return $sys_role;
            }
        }

        return FALSE;
    }

    /**
     * @param null $role
     *
     * @return bool
     */
    public function hasRoleSysRole( $role = NULL )
    {
        if ( empty( $role ) || !$role instanceof Role ) {
            return FALSE;
        }

        foreach ( $this->getSysRoles() as $r ) {
            $id1 = $r->getId();
            $id2 = $role->getId();
            if ( $id1 == $id2 ) {
                return TRUE;
            } else {
                // does the role have a parent that matches?
                if ( $r->hasParentId( $id2 ) ) {
                    return TRUE;
                }
            }
        }

        return FALSE;
    }

    /**
     * @param Role $role
     * @param null $role_name
     *
     * @return bool
     */
    public function parentHasSysRole( Role $role, $role_name = NULL )
    {
        $parents = $role->getParents();
        if ( empty( $parents ) || count( $parents ) == 0 ) {
            return FALSE;
        }

        foreach ( $parents as $parent ) {
            if ( $parent->getName() == $role_name || $parent->getRoleName() == $role_name ) {
                return $parent;
            } else {
                $res = $this->parentHasSysRole( $parent, $role_name );
                if ( !empty( $res ) ) {
                    return $res;
                }
            }
        }

        return FALSE;
    }

    /**
     * Get UseGravatar
     *
     * @return boolean
     */
    public function getUseGravatar()
    {
        return $this->use_gravatar;
    }

    /**
     * Set UseGravatar
     *
     * @param boolean $use_gravatar
     */
    public function setUseGravatar( $use_gravatar = NULL )
    {
        $this->use_gravatar = $use_gravatar;

        return $this;
    }

    /**
     * Get GravatarEmail
     *
     * @return mixed
     */
    public function getGravatarEmail()
    {
        return $this->gravatar_email;
    }

    /**
     * Set GravatarEmail
     *
     * @param mixed $gravatar_email
     */
    public function setGravatarEmail( $gravatar_email = NULL )
    {
        $this->gravatar_email = $gravatar_email;

        return $this;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->getFirstName() . ' ' . $this->getLastName();
    }

    /**
     * Get first_name
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * Set first_name
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName( $firstName )
    {
        $this->first_name = $firstName;

        return $this;
    }

    /**
     * Get last_name
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Set last_name
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName( $lastName )
    {
        $this->last_name = $lastName;

        return $this;
    }

    /**
     * The equality comparison should neither be done by referential equality
     * nor by comparing identities (i.e. getId() === getId()).
     *
     * However, you do not need to compare every attribute, but only those that
     * are relevant for assessing whether re-authentication is required.
     *
     * Also implementation should consider that $user instance may implement
     * the extended user interface `AdvancedUserInterface`.
     *
     * @param UserInterface $user
     *
     * @return Boolean
     */
    public function isEqualTo( UserInterface $user )
    {
        return md5( $user->getUsername() ) == md5( $this->getUsername() ) &&
               md5( serialize( $user->getRoles() ) ) == md5( serialize( $this->getRoles() ) );
    }

    /**
     * @return array
     */
    public function getAdminTableArray()
    {
        $roles = array();
        foreach ( $this->getSysRoles() as $role ) {
            $roles[ ] = '<span class="label label-default" style="margin:1px;background-color:' . $role->getColor() . ' !important">' . ucwords( $role->getName() ) . '</span>';
        }
        $roles = implode( $roles );

        return array(
            'id'         => $this->getId(),
            'username'   => $this->getUsername(),
            'login'      => $this->getUsername(),
            'email'      => $this->getEmail(),
            'last_login' => $this->getLastLogin()->format( 'M d, Y g:i a' ),
            'created_at' => $this->getCreatedAt()->format( 'M d, Y g:i a' ),
            'created'    => $this->getCreatedAt()->format( 'M d, Y g:i a' ),
            'check'      => '<input type="checkbox" value="' . $this->getId() . '" data-id="' . $this->getId() . '" class="row-check" />',
            'active'     => $this->getEnabled(),
            'status'     => $this->getEnabled(),
            'roles'      => $roles
        );
    }

    /**
     * @param null $format
     *
     * @return \DateTime|string
     */
    public function getLastLogin( $format = NULL )
    {
        $last = $this->lastLogin;
        if ( empty( $last ) ) {
            $last = new \DateTime();
            $time = mktime( 0, 0, 0, 1, 1, 2000 );
            $last->setTimestamp( $time );
        }

        if ( !empty( $format ) ) {
            return $last->format( $format );
        }

        return $last;
    }

    /**
     * Get created_at
     *
     * @param string $format
     *
     * @return \DateTime
     */
    public function getCreatedAt( $format = NULL )
    {
        if ( !empty( $format ) ) {
            return $this->created_at->format( $format );
        }

        return $this->created_at;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     *
     * @return User
     */
    public function setCreatedAt( $createdAt )
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * @return bool
     */
    public function getEnabled()
    {
        return $this->enabled;
    }
}
