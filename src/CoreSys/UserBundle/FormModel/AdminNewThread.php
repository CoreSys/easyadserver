<?php

namespace CoreSys\UserBundle\FormModel;

use CoreSys\UserBundle\FormModel\ModeratorNewThread;
use FOS\MessageBundle\Model\ParticipantInterface;

/**
 * Class AdminNewThread
 * @package CoreSys\UserBundle\FormModel
 */
class AdminNewThread extends ModeratorNewThread
{
    /**
     * @var ParticipantInterface
     */
    protected $createdBy;

    /**
     * @return \FOS\MessageBundle\Model\ParticipantInterface
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param ParticipantInterface $user
     * @return $this
     */
    public function setCreatedBy(ParticipantInterface $user = null)
    {
        $this->createdBy = $user;
        return $this;
    }
}