var loadPostDesc = function(parent,checkboxes) {
    checkboxes = checkboxes || false;
    parent = parent || 'auto';

    var $elements = $('[data-postdesc]');
    if( !checkboxes ) {
        $elements = $('[data-postdesc]').not(':checkbox');
    }
    $elements.each(function(){
        var $this = $(this),
            $pd = $('<div class="help-block" />'),
            pd = $this.attr('data-postdesc');
        $pd.html(pd);

        var useParent = parent,
            dParent = false;

        if(parent === 'auto') {
            var $p = $this.parent(),
                $addon = $p.find('.input-group-addon');

            useParent = $addon.length > 0;

            if($p.hasClass('btn-file')) {
                useParent = true;
                dParent = true;
            } else {
                dParent = false;
            }
        }

        if(useParent) {
            if(dParent) {
                $this.parent().parent().after( $pd );
            } else {
                $this.parent().after( $pd );
            }
        } else {
            $this.after( $pd );
        }
        $this.removeAttr('data-postdesc');
    });
};

(function($,undefined){
    var csBootboxConfirm = function( msg, trueCallback, falseCallback ) {
        if(bootbox) {
            bootbox.confirm(msg,function(res){
                if(res === true || res == 'ok') {
                    if(typeof trueCallback == 'function') {
                        trueCallback();
                    }
                } else {
                    if(typeof falseCallback == 'function') {
                        falseCallback();
                    }
                }
            });
        }
    }
})(jQuery);

function loadDropTabs()
{
    if($.fn.tabdrop) {
        $('.nav-pills, .nav-tabs').tabdrop();
    }
}

function loadSelect2()
{
    $('select.select2').select2();
}

$(document).ready(function(){
    loadDropTabs();
    loadPostDesc();
    loadSelect2();
});