<?php

namespace CoreSys\ThemeBundle\Twig;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use CoreSys\SiteBundle\Twig\BaseExtension;
use CoreSys\ThemeBundle\Entity\Theme;

/**
 * Common site extensions to be used in conjunction with
 * the CoreSys Bundles
 *
 * Class SiteExtension
 * @package CoreSys\ThemeBundle\Twig
 */
class ThemeExtension extends BaseExtension
{

    /**
     * @var string
     */
    protected $name = 'theme_extension';

    private $generator;

    public function __construct(EntityManager $entity_manager, ContainerInterface $container, Session $session, UrlGeneratorInterface $generator)
    {
        parent::__construct($entity_manager, $container, $session); // TODO: Change the autogenerated stub
        $this->generator = $generator;
    }


    /**
     * @return array
     */
    public function getFilters()
    {
        return array();
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return array(
            'themeAsset' => new \Twig_Function_Method($this, 'getThemeAsset'),
            'theme_asset' => new \Twig_Function_Method($this, 'getThemeAsset'),
            'plugin_asset' => new \Twig_Function_Method($this, 'getPluginThemeAsset'),
            'admin_theme_asset' => new \Twig_Function_Method($this, 'getAdminThemeAsset' ),
            'getThemes' => new \Twig_Function_Method($this, 'getThemes'),
            'currentTheme' => new \Twig_Function_Method($this, 'getCurrentTheme' ),
            'currentThemeName' => new \Twig_Function_Method($this,'getCurrentThemeName')
        );
    }

    public function getCurrentTheme()
    {
        $repo = $this->getRepo( 'CoreSysThemeBundle:Theme' );
        foreach( $repo as $theme ) {
            if( $theme->getIsDefault() ) {
                return $theme;
            }
        }

        return null;
    }

    public function getCurrentThemeName( $default = 'Metronic' )
    {
        $theme = $this->getCurrentTheme();
        if( !empty( $theme ) ) {
            return $theme->getName();
        }
        return $default;
    }

    public function getPluginThemeAsset( $asset )
    {
        $path = 'bundles/coresystheme/plugins/' . $asset;
        return $this->getContainer()->get('templating.helper.assets')->getUrl($path,null);
    }

    public function getAdminThemeAsset( $asset, $theme = 'Metronic' )
    {
        $theme = strtolower( trim( $theme ) );
        $path = 'bundles/coresystheme/' . $theme . '/admin/' . $asset;
        return $this->getContainer()->get('templating.helper.assets')->getUrl($path,null);
    }

    public function getThemeAsset($asset, $theme = 'default')
    {
        $path = 'bundles/coresystheme/' . $theme . '/' . $asset;
        $packageName = null;
        $container = $this->getContainer();
        return $container->get('templating.helper.assets')->getUrl($path, $packageName);
    }

    public function getThemes()
    {
        $repo = $this->getRepo('CoreSysThemeBundle:Theme');
        return $repo->findAll();
    }

    public function getDefaultTheme( $which = null )
    {

    }

    public function getAdminTheme()
    {
        $repo = $this->getRepo('CoreSysAdminBundle:Config');
        $config = $repo->getConfig();
        return $config->getTheme();
    }

    public function getSiteTheme()
    {
        $repo = $this->getRepo('CoreSysSiteBundle:Config');
        $config = $repo->getConfig();
        return $config->getTheme();
    }
}