<?php

namespace CoreSys\AdBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use CoreSys\AdvertiserBundle\Entity\Advertiser as BaseEntity;
use CoreSys\AdBundle\Entity\Campaign;

/**
 * Advertiser
 *
 * @ORM\Table(name="ad_advertiser")
 * @ORM\Entity(repositoryClass="CoreSys\AdBundle\Entity\AdvertiserRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Advertiser extends BaseEntity
{
    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="CoreSys\AdBundle\Entity\Campaign", mappedBy="advertiser")
     */
    private $campaigns;
    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     */
    private $active;

    /**
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->setCampaigns(new ArrayCollection());
        $this->setActive(true);
    }

    /**
     * Get campaigns
     *
     * @return ArrayCollection
     */
    public function getCampaigns()
    {
        return $this->campaigns;
    }

    /**
     * Set campaigns
     *
     * @param ArrayCollection $campaigns
     * @return Advertiser
     */
    public function setCampaigns($campaigns)
    {
        $this->campaigns = $campaigns;

        return $this;
    }

    /**
     * @param Campaign $campaign
     * @return $this
     */
    public function addCampaign(Campaign $campaign)
    {
        if (!$this->campaigns->contains($campaign)) {
            $this->campaigns->add($campaign);
        }
        return $this;
    }

    /**
     * @param Campaign $campaign
     * @return $this
     */
    public function removeCampaign(Campaign $campaign)
    {
        if ($this->campaigns->contains($campaign)) {
            $this->campaigns->removeElement($campaign);
        }
        return $this;
    }

    public function getAdminTableArray()
    {
        $user_data = $this->getUser()->getAdminTableArray();
        $data = array();
        $data['user'] = $user_data;

        $data['id'] = $this->getId();
        $data['campaigns'] = 0;
        $data['campaigns_count'] = 0;
        $data['ads_count'] = 0;
        $data['ads'] = 0;
        $data['active'] = $data['enabled'] = $this->getActive();
        $data['check'] = '<input type="checkbox" value="' . $this->getId() . '" data-id="' . $this->getId() . '" class="row-check" />';

        return $data;
    }

    public function getActive()
    {
        if (empty($this->active)) {
            $this->active = true;
        }
        return $this->active === true;
    }

    public function setActive($active = true)
    {
        $this->active = $active === true;
        return $this;
    }

    public function getCampaignsCount()
    {
        return count($this->campaigns);
    }
}
