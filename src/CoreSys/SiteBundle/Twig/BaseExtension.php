<?php

namespace CoreSys\SiteBundle\Twig;

use Twig_Extension;
use Twig_Filter_Method;
use Doctrine\ORM\Collections\Collection;
use Doctrine\ORM\PersistentCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Base case extension to be used among all CoreSys Twig Extensions
 * Adds common functionality that would otherwise be copy/pasted
 *
 * Class BaseExtension
 * @package CoreSys\SiteBundle\Twig
 */
class BaseExtension extends Twig_Extension
{
    /**
     * @var string
     */
    protected $name = 'base_extension';
    /**
     * @var EntityManager $entity_manager
     */
    private $entity_manager;
    /**
     * @var ContainerInterface $container
     */
    private $container;
    /**
     * @var Session
     */
    private $session;

    /**
     * construct a new common extensions object
     *
     * @param EntityManager $entity_manager
     * @param ContainerInterface $container
     * @param session $session
     */
    public function __construct(EntityManager $entity_manager, ContainerInterface $container, Session $session)
    {
        $this->entity_manager = $entity_manager;
        $this->container = $container;
        $this->session = $session;
    }

    /**
     * get session
     *
     * @return Session
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * @return EntityManager
     */
    public function getEm()
    {
        return $this->getEntityManager();
    }

    /**
     * get the entity manager
     *
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->entity_manager;
    }

    /**
     * @param $name
     * @return repo|null
     */
    public function getRepository($name)
    {
        return $this->getRepo($name);
    }

    /**
     * Get a specified repository
     *
     * @param string $name
     * @return repo | null
     */
    public function getRepo($name)
    {
        return $this->getEntityManager()->getRepository($name);
    }

    /**
     * @return array
     */
    public function getFilters()
    {
        return parent::getFilters();
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return parent::getFunctions();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $route
     * @param array $parameters
     * @param bool $referenceType
     * @return mixed
     */
    public function generateUrl($route, $parameters = array(), $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH)
    {
        return $this->get('router')->generate($route, $parameters, $referenceType);
    }

    /**
     * get
     *
     * @param string name
     * @return mixed
     */
    public function get($name)
    {
        return $this->getContainer()->get($name);
    }

    /**
     * get the container
     *
     * @return ContainerInterface
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * Get a user from the Security Context
     *
     * @return mixed
     *
     * @throws \LogicException If SecurityBundle is not available
     *
     * @see Symfony\Component\Security\Core\Authentication\Token\TokenInterface::getUser()
     */
    public function getUser()
    {
        if (!$this->getContainer()->has('security.context')) {
            throw new \LogicException('The SecurityBundle is not registered in your application.');
        }

        if (null === $token = $this->getContainer()->get('security.context')->getToken()) {
            return null;
        }

        if (!is_object($user = $token->getUser())) {
            return null;
        }

        return $user;
    }
}