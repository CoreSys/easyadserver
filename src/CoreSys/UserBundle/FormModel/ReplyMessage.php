<?php

namespace CoreSys\UserBundle\FormModel;

use FOS\MessageBundle\FormModel\ReplyMessage as BaseMessage;
use FOS\MessageBundle\Model\ParticipantInterface;

class ReplyMessage extends BaseMessage
{
    /**
     * @var ParticipantInterface
     */
    protected $participant;

    /**
     * @return ParticipantInterface
     */
    public function getParticipant()
    {
        return $this->participant;
    }

    /**
     * @param ParticipantInterface $participant
     */
    public function setParticipant($participant)
    {
        $this->participant = $participant;
    }


}