<?php

namespace CoreSys\MediaBundle\Model;

use CoreSys\SiteBundle\Model\BaseManager;
use CoreSys\MediaBundle\Entity\Image;
use Imagine\Gd\Imagine;
use Imagine\Gd\Image as IImage;
use Imagine\Image\Box;
use Imagine\Image\Point;
use Imagine\Image\color;
use Imagine\Image\ImageInterface;
use CoreSys\MediaBundle\Model\GifCreator;
use CoreSys\MediaBundle\Model\GiffFrameExtractor;
use CoreSys\MediaBundle\Model\ImageWorkshop\ImageWorkshop;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageManager extends BaseManager
{
    public function getMediaFolder()
    {
        return parent::getMediaFilesFolder();
    }

    public function getImagesFolder()
    {
        return parent::getMediaImagesFolder();
    }

    public function getDatedFolder( \DateTime $date = null )
    {
        if( empty( $date ) ) {
            $date = new \DateTime();
        }

        $folders = array(
            $date->format( 'Y' ),
            $date->format( 'm' ),
            $date->format( 'd' ),
            $date->format( 'H' )
        );

        $folder = $this->verifyFolder( $this->getImagesFolder() );
        foreach( $folders as $f ) {
            $folder .= DIRECTORY_SEPARATOR . $f;
            $folder = $this->verifyFolder( $folder );
            $this->log( 'Verified: ' . $folder );
        }

        return $folder;
    }

    public function processUploadedFile( $filename = null )
    {
        $uploads_folder = $this->getUploadFolder();
        $uploaded_file = $uploads_folder . DIRECTORY_SEPARATOR . $filename;

        if( !is_file( $uploaded_file ) ) {
            return 'Could not locate: ' . $uploaded_file;
        }

        // lets get the basic information from the uploaded file
        $original_name = $filename;
        $original_ext = pathinfo( $filename, PATHINFO_EXTENSION );
        $mime_type = 'Image/' . $original_ext;

        return $this->uploadImage( $original_name, $original_ext, $mime_type, true, false, null, null );
    }

    public function uploadNewImage( UploadedFile &$file, $save = false, $crop = false, $crop_width = null, $crop_height = null )
    {
        // lets get the basic information from the uploaded file
        $original_name = $file->getClientOriginalName();
        $original_ext = $file->getClientOriginalExtension();
        $mime_type = $file->getMimeType();

//        $this->setLog( true );
        $this->log( 'Original Name: ' . $original_name );
        $this->log( 'Original Ext: ' . $original_ext );
        $this->log( 'Mime Type: ' . $mime_type );

        // move the file to the uploads folder
        $upload_folder = $this->getUploadFolder();
        $file->move( $upload_folder, $original_name );
        $this->log( 'Moved file to: ' . $upload_folder . DIRECTORY_SEPARATOR . $original_name );

        return $this->uploadImage( $original_name, $original_ext, $mime_type, $save, $crop, $crop_width, $crop_height );
    }

    public function uploadImage($original_name, $original_ext, $mime_type, $save = false, $crop = false, $crop_width = null, $crop_height = null)
    {
        // lets create a new image entity
        $image = new Image();
        $image->setExt( $original_ext );
        $image->setType( $mime_type );

        $upload_folder = $this->getUploadFolder();
        $image_folder = $this->getDatedFolder( $image->getCreatedAt() );
        $this->log( 'Dated Folder: ' . $image_folder );

        // perform some ext cleanup
        $ext = $image->getExt();
        if( $ext == 'jpeg' ) {
            $image->setExt( 'jpg' );
            $ext = 'jpg';
        } else if( $ext == 'bmp' ) {
            $image->setExt( 'wbmp' );
            $ext = 'wbmp';
        }

        $filename = str_replace( '.', '', microtime( true ) ) . '.' . $ext;
        $image->setFilename( $filename );

        $src = $upload_folder . DIRECTORY_SEPARATOR . $original_name;
        $dst = $image_folder . DIRECTORY_SEPARATOR . $filename;

        $image_size = getimagesize( $src );
        $width = $image_size[ 0 ];
        $height = $image_size[ 1 ];

        if(!$crop && !empty( $crop_width ) && !empty( $crop_height ) ) {
            $width = $crop_width;
            $height = $crop_height;
        }

        $ratio = $height / $width;
        $image->setWidth( $width );
        $image->setHeight( $height );
        $image->setRatio( $ratio );

        if( $image->isGif() ) {
            $result = $this->uploadAndCropGifImage( $src, $dst, $width, $height );
        } else {
            $result = false;
        }

        if( !$result ) {
            $imagine = new Imagine();
            $size = new Box( $width, $height );
            $imagine->open( $src )->resize( $size )->save( $dst );
        }

        // we can safely remove the src file now from the uploads directory
        @unlink( $src );

        $sizing_results = $this->createImageSizes( $image );
        $this->createPublicImageFiles( $image );

        if( $save && $sizing_results ) {
            $this->persist( $image );
            $this->flush();
        }

        return $image;
    }

    public function uploadAndCropGifImage( $src = null, $dst = null, $width = 0, $height = 0 )
    {
        $src_size = getimagesize( $src );
        $src_width = $src_size[ 0 ];
        $src_height = $src_size[ 1 ];

        if( $src_width == $width && $src_height = $height ) {
            // we do not need to edit this file at all
            @copy( $src, $dst );
            return true;
        }

        $offset_x = $offset_y = 0;
        if( $src_width != $width ) {
            $offset_x = intval( ( $src_width - $width ) / 2 );
        }
        if( $src_height != $height ) {
            $offset_y = intval( ( $src_height - $height ) / 2 );
        }

        if( GifFrameExtractor::isAnimatedGif( $src ) ) {
            $gfe = new GifFrameExtractor();
            $frames = $gfe->extract( $src );
            $retouched_frames = array();
            foreach( $frames as $frame ) {
                $frame_layer = ImageWorkshop::initFromResourceVar( $frame[ 'image' ] );
                $frame_layer->cropInPixel( $width, $height, $offset_x, $offset_y );
                $retouched_frames[] = $frame_layer->getResult();
            }
            $gc = new GifCreator();
            $gc->create( $retouched_frames, $gfe->getFrameDurations(), 0);
            file_put_contents( $dst, $gc->getGif() );

            return true;
        }

        return false;
    }

    public function getConfigImageSizes( $full = true )
    {
        return null;
    }

    public function getImageSizes( $full = true )
    {
        // try to get the sizes from the image configuration
        // and return this result
        $config_sizes = $this->getConfigImageSizes( $full );

        if( !empty( $config_sizes ) ) {
            return $config_sizes;
        }

        if( !$full ) {
            return array(
                'teenie', 'tiny', 'thumb', 'small', 'medium', 'large',
                'xlarge', 'xxlarge', 'xxxlarge', 'xxxxlarge'
            );
        } else {
            return array(
                'teenie' => 50,
                'tiny' => 75,
                'thumb' => 150,
                'small' => 240,
                'medium' => 800,
                'large' => 1024,
                'xlarge' => 1280,
                'xxlarge' => 1600,
                'xxxlarge' => 2400,
                'xxxxlarge' => 3200
            );
        }

    }

    public function createImageSizes( Image &$image )
    {
        $sizes = $this->getImageSizes( true );
        // the image entity needs the full list of available sizes to
        // start with, we will update this to accurately reflect the
        // actual sizes available to the image
        $image->setSizes( $sizes );

        $src = $image->getSizeFile( 'original', $this->getImagesFolder() );
        $this->log( print_r( $sizes, true ) );
        // we would like the list of sizes from largest to smallest
        $sizes = array_reverse( $sizes );
        $width = $image->getWidth();
        $height = $image->getHeight();
        $ratio = $image->getRatio();

        $imagine = new Imagine();
        $mode = ImageInterface::THUMBNAIL_OUTBOUND;
        $ext = pathinfo( $src, PATHINFO_EXTENSION );
        $filename = $image->getFilename();

        // lets keep a list of available sizes so we can add this to the
        // image sizes array
        $available_sizes = array();

        $base = $this->getImagesFolder();

        foreach( $sizes as $size => $dst_width ) {
            if( $size != 'orig' && $size !== 'original' ) {
                if( $dst_width <= $width ) {
                    $dst = $image->getSizeFolder( $size, $base ) . DIRECTORY_SEPARATOR . $filename;
                    $dst_height = intval( $dst_width * $ratio );
                    $this->log( 'Creating size: ' . $size );
                    $this->log( 'File: ' . $dst );
                    $this->log( 'Dims: ' . $dst_width . 'x' . $dst_height );
                    $available_sizes[ $size ] = $dst_width;

                    if( $image->isGif() ) {
                        $this->resizeGifImage( $src, $dst, $dst_width, $dst_height );
                    } else {
                        $dst_size = new Box( $dst_width, $dst_height );
                        $imagine->open( $src )->resize( $dst_size )->save( $dst );
                    }

                    if( is_file( $dst ) ) {
                        $src = $dst;
                    }
                }
            }
        }

        $image->setSizes( $available_sizes );
        return true;
    }

    public function resizeGifImage( $src, $dst, $width, $height )
    {
        $src_size = getimagesize( $src );
        $src_width = $src_size[ 0 ];
        $src_height = $src_size[ 1 ];

        if( $src_width == $width && $src_height == $height ) {
            @copy( $src, $dst );
            return true;
        }

        $offset_x = $offset_y = 0;
        if( GifFrameExtractor::isAnimatedGif( $src ) ) {
            $gfe = new GifFrameExtractor();
            $frames = $gfe->extract( $src );
            $retouched_frames = array();
            foreach( $frames as $frame ) {
                $frame_layer = ImageWorkshop::initFromResourceVar( $frame[ 'image' ] );
                $frame_layer->resizeInPixel( $width, $height, true );
                $retouched_frames[] = $frame_layer->getResult();
            }

            $gc = new GifCreator();
            $gc->create( $retouched_frames, $gfe->getFrameDurations(), 0 );

            file_put_contents( $dst, $gc->getGif() );

            return true;
        } else {
            $imagine = new Imagine();
            $size = new Box( $width, $height );
            $mode = ImageInterface::THUMBNAIL_OUTBOUND;
            $imagine->open( $src )->thumbnail( $size, $mode )->save( $dst );
            return true;
        }

        return false;
    }

    public function showImage( Image &$image, $size = null ) {
        $out = null;
        if( empty( $size ) ) {
            $size = 'original';
        }

        $size = strtolower( trim( $size ) );
        if( !$image->hasSize( $size ) ) {
            switch($size) {
                case 'original':
                    return $this->showImage( $image, 'xxxxlarge' );
                    break;
                case 'xxxxlarge':
                    return $this->showImage( $image, 'xxxlarge' );
                break;
                case 'xxxlarge':
                    return $this->showImage( $iamge, 'xxlarge' );
                break;
                case 'xxlarge':
                    return $this->showImage( $image, 'xlarge' );
                break;
                case 'xlarge':
                    return $this->showImage( $image, 'large');
                break;
                case 'large':
                    return $this->showImage( $image, 'medium' );
                break;
                case 'medium':
                    return $this->showImage( $image, 'small' );
                break;
                case 'small':
                    return $this->showImage( $image, 'thumb' );
                break;
                case 'thumb':
                    return $this->showImage( $image, 'tiny');
                break;
                case 'tiny':
                    return $this->showImage( $image, 'teenie');
                break;
                default:
                    echo 'File not found';
                    return false;
                break;
            }
        }

        $base = $this->getImagesFolder();
        $file = $image->getSizeFile( $size, $base );
        if( !is_file( $file ) ) {
            echo 'File not found';
            return false;
        }

        $ext = $image->getExt();
        $imagine = new Imagine();
        $img = $imagine->open( $file );
        if( $ext === 'jpg' ) {
            $ext = 'jpeg';
        }
        $img->show( $ext );

        return true;
    }

    public function removeImage( Image &$image )
    {
        $this->log( '------' );
        $this->log( '<h3>Removing image</h3>');
        $this->removeAbsoluteImageFiles( $image );
        $this->removePublicImageFiles( $image );
        $this->remove( $image );
        $this->flush();
        $this->log( '--------' );
        $this->log( '--------' );
    }

    public function removeAbsoluteImageFiles( Image &$image )
    {
        $base = $this->getImagesFolder();
        $sizes = $image->getSizes( false );
        $sizes[] = 'original';

        foreach( $sizes as $size ) {
            $file = $image->getSizeFile( $size, $base );
            $this->log( 'Attempting to remove file: ' . $file);
            if( is_file( $file ) ) {
                unlink( $file );
                $this->log( 'Removed file: ' . $file);
            }
        }
    }

    public function removePublicImageFiles( Image &$image )
    {
        $base = $this->getPublicImagesFolder();
        $sizes = $image->getSizes( false );
        $sizes[] = 'original';

        foreach( $sizes as $size ) {
            $file = $image->getSizeFile( $size, $base );
            $this->log( 'Attempting to remove file: ' . $file);
            if( is_file( $file ) ) {
                unlink( $file );
                $this->log( 'Removed file: ' . $file);
            }
        }
    }

    public function getPublicMediaFolder()
    {
        $folder = $this->getWebFolder() . DIRECTORY_SEPARATOR . 'media_files';
        return $this->verifyFolder( $folder );
    }

    public function getPublicImagesFolder()
    {
        $folder = $this->getPublicMediaFolder() . DIRECTORY_SEPARATOR . 'photos';
        return $this->verifyFolder( $folder );
    }

    public function createPublicImageFiles( Image &$image )
    {
        $base = $this->getImagesFolder();
        $sizes = $image->getSizes( false );

        $this->log( 'Creating public images' );
        $this->log( $base );

        $pub_base = $this->getPublicImagesFolder();

        foreach( $sizes as $size ) {
            $this->log( $size );
            $src = $image->getSizeFile( $size, $base );
            $this->log( $src );
            if( is_file( $src ) ) {
                $dst = $image->getSizeFile( $size, $pub_base );
                $this->verifyPublicImageFolderStructureForFile( $dst );
                @copy( $src, $dst );
            }
        }

        // cannot forget the original
        $src = $image->getSizeFile( 'original', $base );
        $dst = $image->getSizeFile( 'original', $pub_base );
        @copy( $src, $dst );
    }

    public function verifyPublicImageFolderStructureForFile( $file = null )
    {
        $path = dirname( $file );
        $parts = explode( DIRECTORY_SEPARATOR, $path );
        $base = null;
        foreach ($parts as $part) {
            if( !empty( $part ) ) {
                $base .= DIRECTORY_SEPARATOR . $part;
//                $this->log( $base );
                if( !is_dir( $base ) ) {
                    $this->log( 'Creating public folder: ' . $base );
                    @mkdir( $base, 0777 );
                    @chmod( $base, 0777 );
                }
            }
        }
    }
}