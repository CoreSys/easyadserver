<?php

namespace CoreSys\AdBundle\Model;

use CoreSys\SiteBundle\Model\BaseManager;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;

class AdManager extends BaseManager
{
    protected $ad_type = 'default';
    private $managers = array();

    public function getManager( $type = null )
    {
        $class = ucfirst( $type ) . 'AdManager';
        if( isset( $this->managers[ $class ] ) ) {
            return $this->managers[ $class ];
        }

        try {
            $obj = new $class( $this->getEntityManager(), $this->getContainer() );
            $this->managers[ $class ] = $obj;
            return $obj;
        } catch( \Exception $e ) {
            return null;
        }
    }
}