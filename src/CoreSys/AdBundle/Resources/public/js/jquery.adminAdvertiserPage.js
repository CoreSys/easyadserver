;(function($,undefined){
    $.fn.adminAdvertiserPage = function(options) {
        var p = this,
            defaults = {
                checkable: true,
                entity: 'Advertiser',
                selected_count_selector: '.selected-count',
                check_master_selector: 'thead tr .master-check',
                check_row_selector: 'td .row-check',
                add_row_btn_selector: '.add-advertiser',
                remove_rows_btn_selector: '.remove-advertisers',
                table_selector: '#advertisers-table',
                row_edit_btn_selector: '.btn-edit',
                row_view_btn_selector: '.btn-view',
                row_remove_btn_selector: '.btn-remove',
                row_manage_btn_selector: '.btn-manage',
                dt_display_length: 10,
                new_modal_selector: '#new-modal',
                edit_modal_selector: '#edit-modal',
                view_modal_selector: '#view-modal',
                no_sort_columns: [0,6],
                dataTables_aoColumns: [
                    {"sName": 'check', "bSortable": false, "bSearchable": false, "mData": 'check'},
                    {"sName": 'id', "mData": 'id'},
                    {"sName": 'user.username', "mData": 'user.username'},
                    {"sName": 'user.email', "mData": 'user.email'},
                    {"sName": 'user.created_at', "bSearchable": false, "mData": 'user.created_at'},
                    {"sName": 'user.last_login', "bSearchable": false, "mData": 'user.last_login'},
                    {"sName": 'user.roles', "mData": 'user.roles'},
                    {"sName": 'campaigns_count', "bSearchable": false, "mData": 'campaigns_count'},
                    {"sName": 'ads_count', "bSearchable": false, "mData": 'ads_count'},
                    {"sName": 'active', "bSearchable": false, "mData": 'active'},
                    {"sName": 'options', "bSortable": false, "bSearchable": false, "mData": 'options'}
                ],
                remove_row_url: Routing.generate( 'admin_ajax_ads_advertisers_remove_advertiser'),
                add_row_url: Routing.generate('admin_ajax_ads_advertisers_new'),
                edit_row_url: Routing.generate('admin_ajax_ads_advertisers_edit'),
                view_row_url: Routing.generate('admin_ajax_ads_advertisers_view_advertiser'),
                dataTables_ajax_url: Routing.generate('admin_ajax_ads_advertisers_data_tables'),
                manage_row_url: Routing.generate( 'admin_ads_advertisers_manage' ),
                remove_message: 'Are you sure you want to remove this Advertiser?<br>This will not remove the User associated with the advertiser account.',
                fnGetRowData: getRowData,
                fnPopulateEditedRowData: populateEditedRowData
            };

        p.settings = {};
        p.page = null;

        function init()
        {
            p.settings = $.extend({}, defaults, options );
            p.page = new $.fn.adminAjaxTablePage(p.settings);
            p.page.datatable.fnSort( [ [1,'desc'] ] )
        }

        function getRowData(data) {
            return data;
        };

        function populateEditedRowData( row, data ) {
            p.page.datatable.fnReloadAjax();
            p.page.resetRowSelects();
        };

        init();

        return this;
    };
})(jQuery);