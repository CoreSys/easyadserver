<?php

namespace CoreSys\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class DefaultController
 * @package CoreSys\SiteBundle\Controller
 * @Route("/")
 */
class DefaultController extends BaseController
{
    /**
     * @Route("/", name="site_base")
     * @Template()
     */
    public function indexAction()
    {
        exit;
    }
}
