<?php

namespace CoreSys\AdBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CoreSys\SiteBundle\Controller\AdminController as BaseController;

/**
 * Class AdminCampaignsController
 * @package CoreSys\AdBundle\Controller
 * @Route("/admin/ads/campaigns", options={"expose"=true})
 */
class AdminCampaignsController extends BaseController
{
   /**
    * @Route("/", name="admin_ads_campaigns_index")
    * @Template()
    */
    public function indexAction()
    {
        return array();
    }
}
