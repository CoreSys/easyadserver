<?php

namespace CoreSys\CampaignBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Event\DataEvent;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;

class CampaignType extends AbstractType
{

    public function buildForm( FormBuilderInterface $builder, array $options )
    {
        $builder
            ->add( 'name', NULL, array( 'attr' => array() ) )
            ->add( 'user', 'entity', array( 'required' => TRUE, 'class' => 'CoreSysUserBundle:user', 'attr' => array( 'class' => 'form-control') ) )
            ->add( 'description', 'textarea',
                array( 'required' => TRUE, 'attr' => array( 'rows' => 6 ) ) )
            ->add('active','checkbox', array( 'required' => false, 'attr' => array('class' => 'make-switch') ) );
    }

    public function getName()
    {
        return 'campaign_type';
    }
}
