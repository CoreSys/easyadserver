<?php
/**
 * This file is part of the CoreSysUserBundle package.
 * (c) J&L Core Systems http://jlcoresystems.com | http://joshmccreight.com
 */

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

//namespace FOS\UserBundle\Form\Type;
namespace CoreSys\AdvertiserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\Validator\Constraint\UserPassword;
use CoreSys\UserBundle\Form\UserType;
use CoreSys\SiteBundle\Form\AddressMinimalType;

/**
 * Class UserType
 * @package CoreSys\AdvertiserBundle\Form
 */
class AdvertiserType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
//        parent::buildForm( $builder, $options );
        $builder
            ->add('user', new UserType())
            ->add('shipping_address', new AddressMinimalType())
            ->add('billing_address', new AddressMinimalType())
            ->add('shipping_same', 'checkbox', array('required' => false, 'label' => 'Same as Billing Address'));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CoreSys\AdvertiserBundle\Entity\Advertiser',
            'intention' => 'create',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'coresys_advertiser_type';
    }

}