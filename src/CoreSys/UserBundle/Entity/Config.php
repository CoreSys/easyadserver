<?php

namespace CoreSys\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CoreSys\UserBundle\Entity\Role;

/**
 * Config
 *
 * @ORM\Table(name="user_config")
 * @ORM\Entity(repositoryClass="CoreSys\UserBundle\Entity\ConfigRepository")
 */
class Config
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Role
     *
     * @ORM\ManyToOne(targetEntity="Role")
     * @ORM\JoinColumn(name="mtr_id", referencedColumnName="id", nullable=true)
     */
    private $messageTrashRole;

    /**
     * @var Role
     *
     * @ORM\ManyToOne(targetEntity="Role")
     * @ORM\JoinColumn(name="mar_id", referencedColumnName="id", nullable=true)
     */
    private $messageAdminRole;

    /**
     * @var Role
     *
     * @ORM\ManyToOne(targetEntity="Role")
     * @ORM\JoinColumn(name="mmr_id", referencedColumnName="id", nullable=true)
     */
    private $messageModeratorRole;

    /**
     * @var integer
     *
     * @ORM\Column(name="minimum_password_length", type="integer", nullable=true)
     */
    private $minimumPasswordLength;

    /**
     * @var integer
     *
     * @ORM\Column(name="maximum_password_length", type="integer", nullable=true)
     */
    private $maximumPasswordLength;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get messageTrashRole
     *
     * @return mixed
     */
    public function getMessageTrashRole()
    {
        return $this->messageTrashRole;
    }

    /**
     * Set messageTrashRole
     *
     * @param Role $messageTrashRole
     * @return Config
     */
    public function setMessageTrashRole(Role $messageTrashRole = null)
    {
        $this->messageTrashRole = $messageTrashRole;

        return $this;
    }

    /**
     * Get messageAdminRole
     *
     * @return mixed
     */
    public function getMessageAdminRole()
    {
        return $this->messageAdminRole;
    }

    /**
     * Set messageAdminRole
     *
     * @param Role $messageAdminRole
     * @return Config
     */
    public function setMessageAdminRole(Role $messageAdminRole = null)
    {
        $this->messageAdminRole = $messageAdminRole;

        return $this;
    }

    /**
     * Get messageModeratorRole
     *
     * @return mixed
     */
    public function getMessageModeratorRole()
    {
        return $this->messageModeratorRole;
    }

    /**
     * Set messageModeratorRole
     *
     * @param Role $messageModeratorRole
     * @return Config
     */
    public function setMessageModeratorRole(Role $messageModeratorRole = null)
    {
        $this->messageModeratorRole = $messageModeratorRole;

        return $this;
    }

    /**
     * Get minimumPasswordLength
     *
     * @return integer
     */
    public function getMinimumPasswordLength()
    {
        return intval($this->minimumPasswordLength);
    }

    /**
     * Set minimumPasswordLength
     *
     * @param integer $minimumPasswordLength
     * @return Config
     */
    public function setMinimumPasswordLength($minimumPasswordLength = null)
    {
        $this->minimumPasswordLength = intval($minimumPasswordLength);

        return $this;
    }

    /**
     * Get maximumPasswordLength
     *
     * @return integer
     */
    public function getMaximumPasswordLength()
    {
        return intval($this->maximumPasswordLength);
    }

    /**
     * Set maximumPasswordLength
     *
     * @param integer $maximumPasswordLength
     * @return Config
     */
    public function setMaximumPasswordLength($maximumPasswordLength = null)
    {
        $this->maximumPasswordLength = intval($maximumPasswordLength);

        return $this;
    }
}
