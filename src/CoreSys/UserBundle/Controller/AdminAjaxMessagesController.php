<?php
/**
 * This file is part of the CoreSysUserBundle package.
 * (c) J&L Core Systems http://jlcoresystems.com | http://joshmccreight.com
 */

namespace CoreSys\UserBundle\Controller;

use CoreSys\UserBundle\CoreSysUserEvents;
use CoreSys\UserBundle\Entity\Message;
use CoreSys\UserBundle\Event\RoleEvent;
use CoreSys\UserBundle\Form\UserAddType;
use CoreSys\UserBundle\Form\UserEditType;
use CoreSys\UserBundle\Form\UserType;
use CoreSys\UserBundle\Form\RoleType;
use CoreSys\UserBundle\Form\AccessType;
use CoreSys\UserBundle\Entity\User;
use CoreSys\UserBundle\Entity\Role;
use CoreSys\UserBundle\Entity\Thread;
use CoreSys\UserBundle\Entity\Access;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CoreSys\SiteBundle\Controller\AjaxController as BaseController;
use CoreSys\UserBundle\Model\MessageManager;

/**
 * Class AdminAjaxMessagesController
 * @package CoreSys\UserBundle\Controller
 * @Route("/admin/ajax/userMessages", options={"expose"=true})
 */
class AdminAjaxMessagesController extends BaseController
{
    /**
     * @var MessageManager
     */
    private $manager;

    /**
     * @Route("/inbox/data_tables", name="admin_ajax_user_messages_inbox_data_tables", defaults={"slug"=null})
     * @Route("/inbox/data_tables/{slug}", name="admin_ajax_user_messages_inbox_data_tables_slug", defaults={"slug"=null})
     * @Template()
     */
    public function inboxDataTablesAction( $slug )
    {
        $repo = $this->getRepo( 'CoreSysUserBundle:User' );
        $user = $repo->locateUser( $slug );

        $data = $this->processDatatables();
        $manager = $this->getManager();
        $results = $manager->getInboxDatatablesResults( $data, $user );

        $results[ 'data' ] = $data;

        $this->echoJsonResponse( $results, true );
        exit;
    }

    /**
     * @Route("/sent/data_tables", name="admin_ajax_user_messages_sent_data_tables", defaults={"slug"=null})
     * @Route("/sent/data_tables/{slug}", name="admin_ajax_user_messages_sent_data_tables_slug", defaults={"slug"=null})
     * @Template()
     */
    public function sentDataTablesAction( $slug )
    {
        $repo = $this->getRepo( 'CoreSysUserBundle:User' );
        $user = $repo->locateUser( $slug );

        $data = $this->processDatatables();
        $manager = $this->getManager();
        $results = $manager->getSentDatatablesResults( $data, $user );

        $results[ 'data' ] = $data;

        $this->echoJsonResponse( $results, true );
        exit;
    }

    /**
     * @Route("/trash/data_tables", name="admin_ajax_user_messages_trash_data_tables", defaults={"slug"=null})
     * @Route("/trash/data_tables/{slug}", name="admin_ajax_user_messages_trash_data_tables_slug", defaults={"slug"=null})
     * @Template()
     */
    public function trashDataTablesAction( $slug )
    {
        $repo = $this->getRepo( 'CoreSysUserBundle:User' );
        $user = $repo->locateUser( $slug );

        $data = $this->processDatatables();
        $manager = $this->getManager();
        $results = $manager->getTrashDatatablesResults( $data, $user );

        $results[ 'data' ] = $data;

        $this->echoJsonResponse( $results, true );
        exit;
    }

    /**
     * @Route("/inbox", name="admin_ajax_user_messages_inbox", defaults={"slug"=null})
     * @Route("/inbox/{slug}", name="admin_ajax_messages_inbox_slug", defaults={"slug"=null})
     * @Template()
     */
    public function inboxAction($slug)
    {
        $manager = $this->getManager();
        $threads = $manager->getInboxThreads();

        return array( 'threads' => $threads );
    }

    public function sendDummyMessages()
    {
        $to = $this->getManager()->getAuthenticatedParticipant();
        $repo = $this->getRepo( 'CoreSysUserBundle:User' );

        for( $i = 2; $i <= 100; $i++ ) {
            $from = $repo->findOneById( $i );
            if( !empty( $from ) ) {
                $this->sendDummyMessage( $from, $to, 'Title sample ' . $i, 'Sample message body ' . $i );
            }
        }
    }

    public function sendDummyMessage( $from, $to, $title, $body )
    {
        $this->getManager()->compose( $from, $to, $title, $body, true );
    }

    /**
     * @return MessageManager|object
     */
    public function getManager()
    {
        if (!empty($this->manager)) {
            return $this->manager;
        }

        $this->manager = $this->get('core_sys_user.message_manager');
        return $this->manager;
    }

    /**
     * @Route("/markThreadsAsRead", name="admin_ajax_user_messages_mark_threads_as_read", defaults={"slug":null})
     * @Route("/markThreadsAsRead/{slug}", name="admin_ajax_user_messages_mark_threads_as_read_slug", defaults={"slug":null})
     * @Template()
     */
    public function markThreadsAsReadAction($slug)
    {
        $repo = $this->getRepo( 'CoreSysUserBundle:User' );
        $user = $repo->locateUser( $slug );

        if( empty( $user ) ) {
            $user = $this->getUser();
        }

        if( empty( $user ) ) {
            $this->echoJsonError( 'Could not determine user' );
        }

        $request = $this->get( 'request' );
        $ids = $request->get( 'ids', array() );
        if( !is_array( $ids ) ) {
            $this->echoJsonError( 'No Thread IDs Specified' );
            exit;
        }

        $results = array( 'ids' => array() );
        $repo = $this->getRepo( 'CoreSysUserBundle:Thread' );
        $manager = $this->get( 'core_sys_user.message_manager' );

        foreach( $ids as $id ) {
            $thread = $repo->findOneById( $id );
            if( !empty( $thread ) ) {
                $results[ 'ids' ][] = $id;
                $manager->getMessageManager()->markIsReadByThreadAndParticipant($thread, $user, true);
            }
        }

        $this->flush();

        $this->echoJsonSuccess( 'Successfully marked selected messages as read', $results );
        exit;
    }

    /**
     * @Route("/markThreadsAsUnRead", name="admin_ajax_user_messages_mark_threads_as_unread", defaults={"slug":null})
     * @Route("/markThreadsAsUnRead/{slug}", name="admin_ajax_user_messages_mark_threads_as_unread_slug", defaults={"slug":null})
     * @Template()
     */
    public function markThreadsAsUnReadAction($slug)
    {
        $repo = $this->getRepo( 'CoreSysUserBundle:User' );
        $user = $repo->locateUser( $slug );

        if( empty( $user ) ) {
            $user = $this->getUser();
        }

        if( empty( $user ) ) {
            $this->echoJsonError( 'Could not determine user' );
        }

        $request = $this->get( 'request' );
        $ids = $request->get( 'ids', array() );
        if( !is_array( $ids ) ) {
            $this->echoJsonError( 'No Thread IDs Specified' );
            exit;
        }

        $results = array( 'ids' => array() );
        $repo = $this->getRepo( 'CoreSysUserBundle:Thread' );
        $manager = $this->get( 'core_sys_user.message_manager' );

        foreach( $ids as $id ) {
            $thread = $repo->findOneById( $id );
            if( !empty( $thread ) ) {
                $results[ 'ids' ][] = $id;
                $manager->getMessageManager()->markIsReadByThreadAndParticipant($thread, $user, false);
            }
        }

        $this->flush();

        $this->echoJsonSuccess( 'Successfully marked selected messages as unread', $results );
        exit;
    }

    /**
     * @Route("/removeThreads", name="admin_ajax_user_messages_remove_thread", defaults={"slug"=null})
     * @Route("/removeThreads/{slug}", name="admin_ajax_user_messages_remove_thread_slug", defaults={"slug"=null})
     * @Template()
     */
    public function removeThreadsAction($slug)
    {
        $repo = $this->getRepo( 'CoreSysUserBundle:User' );
        $user = $repo->locateUser( $slug );

        if( empty( $user ) ) {
            $user = $this->getUser();
        }

        if( empty( $user ) ) {
            $this->echoJsonError( 'Could not determine user' );
        }

        $request = $this->get( 'request' );
        $ids = $request->get( 'ids', array() );
        if( !is_array( $ids ) ) {
            $this->echoJsonError( 'No Thread IDs Specified' );
            exit;
        }

        $results = array( 'ids' => array() );
        $repo = $this->getRepo( 'CoreSysUserBundle:Thread' );
        $manager = $this->get( 'core_sys_user.message_manager' );

        foreach( $ids as $id ) {
            $thread = $repo->findOneById( $id );
            if( !empty( $thread ) ) {
                $results[ 'ids' ][] = $id;
                foreach( $thread->getAllMetadata() as $md ) {
                    if($md->getParticipant()->getId() == $user->getId() ) {
                        $md->setIsDeleted(true);
                        $this->persist( $md );
                    }
                }
            }
        }

        $this->flush();

        $this->echoJsonSuccess( 'Successfully deleted selected messages', $results );
        exit;
    }

    /**
     * @Route("/removeThread/{slug}/{id}", name="admin_ajax_user_messages_remove_thread", defaults={"id"=null,"slug"=null})
     * @ParamConverter("thread", class="CoreSysUserBundle:Thread")
     * @Template()
     */
    public function removeThreadAction( Thread $thread, $slug )
    {
        $repo = $this->getRepo( 'CoreSysUserBundle:User' );
        $user = $repo->locateUser( $slug );

        if( empty( $user ) ) {
            $user = $this->getUser();
        }

        if( empty( $user ) ) {
            $this->echoJsonError( 'Could not determine user' );
        }

        foreach( $thread->getAllMetadata() as $md ) {
            if($md->getParticipant()->getId() == $user->getId() ) {
                $md->setIsDeleted(true);
                $this->persist( $md );
            }
        }

        $this->flush();

        $this->echoJsonSuccess( 'Success' );
        exit;
    }

    /**
     * @Route("/deleteThread/{tid}", name="admin_ajax_user_messages_delete_thread", defaults={"slug"=null,"tid"=null})
     * @Route("/deleteThread/{slug}/{tid}", name="admin_ajax_user_messages_delete_thread_slug", defaults={"slug"=null,"tid"=null})
     * @Template()
     */
    public function deleteThreadAction($slug,$tid)
    {
        $repo = $this->getRepo( 'CoreSysUserBundle:User' );
        $user = $repo->locateUser( $slug );

        if(empty($user)) {
            $user = $this->getUser();
        }
        if(empty($user)) {
            $this->echoJsonError('Could not determine user');
        }
        // we aren't actually going to remove anything!
        $this->echoJsonSuccess( 'success' );
    }

    /**
     * @Route("/undeleteThread/{id}", name="admin_ajax_user_messages_undelete_thread", defaults={"slug"=null, "id"=null})
     * @Route("/undeleteThread/{slug}/{id}", name="admin_ajax_user_messages_undelete_thread_slug", defaults={"slug"=null, "id"=null})
     * @paramConverter("thread", class="CoreSysUserBundle:Thread")
     * @Template()
     */
    public function undeleteThreadAction(Thread $thread, $slug) {
        $repo = $this->getRepo( 'CoreSysUserBundle:User');
        $user = $repo->locateUser($slug);
        if(empty($user)) {
            $user = $this->getUser();
        }
        if(empty($user)) {
            $this->echoJsonError('Could not determine user');
        }

        foreach( $thread->getAllMetadata() as $md ) {
            if($md->getParticipant()->getId() == $user->getId() ) {
                $md->setIsDeleted(false);
                $this->persist( $md );
            }
        }

        $this->flush();

        $this->echoJsonSuccess( 'Success' );
    }

    /**
     * @Route("/viewThread/{slug}/{tid}", name="admin_ajax_users_view_thread", defaults={"slug"=null,"tid"=null})
     * @Template()
     */
    public function viewThreadAction($slug,$tid)
    {
        $user = $this->getRepo('CoreSysUserBundle:User')->locateUser($slug);
    }

    /**
     * @Route("/newAdminThread", name="admin_ajax_user_messages_new_admin_thread")
     * @Template()
     */
    public function adminNewThreadAction()
    {
        $manager = $this->get( 'core_sys_user.message_manager' );
        $form = $manager->getAdminNewThreadForm();

        $action = $this->generateUrl( 'admin_ajax_user_messages_handle_new_admin_thread' );

        return array( 'form' => $form->createView(), 'action' => $action );
    }

    /**
     * @Route("/handleNewAdminThread", name="admin_ajax_user_messages_handle_new_admin_thread")
     * @Template()
     */
    public function handleAdminNewThreadAction() {
        $manager = $this->get( 'core_sys_user.message_manager');

        $result = $manager->processAdminNewThreadForm( );

        if( $result instanceof Message || $result === true ) {
            $this->echoJsonSuccess( 'Success' );
        } else {
            $this->echoJsonError( $result );
        }

        exit;
    }

    /**
     * @Route("/replyAdminThread/{id}/{slug}", name="admin_ajax_user_messages_reply_admin_thread", defaults={"id"=null,"slug"=null})
     * @ParamConverter("thread", class="CoreSysUserBundle:Thread")
     * @Template()
     */
    public function adminReplyThreadAction(Thread $thread, $slug)
    {
        $repo = $this->getRepo( 'CoreSysUserBundle:User' );
        $user = $repo->locateUser( $slug );
        if( empty( $user ) ) {
            $user = $this->getUser();
        }

        $manager = $this->get( 'core_sys_user.message_manager' );
        $form = $manager->getAdminReplyThreadForm( $thread, $user );

        $action = $this->generateUrl( 'admin_ajax_user_messages_handle_reply_admin_thread', array( 'id' => $thread->getId() ) );

        return array( 'thread' => $thread, 'form' => $form->createView(), 'action' => $action );
    }

    /**
     * @Route("/handleReplyAdminThread/{id}", name="admin_ajax_user_messages_handle_reply_admin_thread", defaults={"id"=null})
     * @ParamConverter("thread", class="CoreSysUserBundle:Thread")
     * @Template()
     */
    public function handleReplyAdminThreadAction( Thread $thread )
    {
        $manager = $this->get( 'core_sys_user.message_manager' );
        $result = $manager->processAdminReplyThread( $thread );

        if($result instanceof Message || $result === true) {
            $this->echoJsonSuccess( 'Success' );
        } else { 
            $this->echoJsonError( $result );
        }
    }

    /**
     * @Route("/emptyTrash", name="admin_ajax_user_messages_empty_trash", defaults={"slug"=null})
     * @Route("/emptyTrash/{slug}", name="admin_ajax_user_messages_empty_trash_slug", defaults={"slug"=null})
     * @Template()
     */
    public function emptyTrashAction( $slug )
    {
        $repo = $this->getRepo('CoreSysUserBundle:User');
        $user = $repo->locateUser($slug);

//        $this->echoJsonSuccess( 'success' );
//        exit;

        $manager = $this->get( 'core_sys_user.message_manager');
        $result = $manager->emptyTrash($user);

        if($result === true) {
            $this->echoJsonSuccess( 'Success' );
        } else {
            $this->echoJsonError( 'Error' );
        }

        exit;
    }
}