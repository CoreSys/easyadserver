<?php

namespace CoreSys\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Address
 *
 * @ORM\Table(name="address")
 * @ORM\Entity(repositoryClass="CoreSys\SiteBundle\Entity\AddressRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Address
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;
    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=64)
     */
    private $firstName;
    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=64)
     */
    private $lastName;
    /**
     * @var string
     *
     * @ORM\Column(name="company_name", type="string", length=128, nullable=true)
     */
    private $companyName;
    /**
     * @var string
     *
     * @ORM\Column(name="address1", type="string", length=255)
     */
    private $address1;
    /**
     * @var string
     *
     * @ORM\Column(name="address2", type="string", length=255, nullable=true)
     */
    private $address2;
    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=128)
     */
    private $city;
    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=128)
     */
    private $state;
    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=128)
     */
    private $country;
    /**
     * @var string
     *
     * @ORM\Column(name="zip_code", type="string", length=32)
     */
    private $zipCode;
    /**
     * @var string
     *
     * @ORM\Column(name="home_phone", type="string", length=64, nullable=true)
     */
    private $homePhone;
    /**
     * @var string
     *
     * @ORM\Column(name="work_phone", type="string", length=64, nullable=true)
     */
    private $workPhone;
    /**
     * @var string
     *
     * @ORM\Column(name="cell_phone", type="string", length=64, nullable=true)
     */
    private $cellPhone;
    /**
     * @var string
     *
     * @ORM\Column(name="home_phone_extension", type="string", length=12, nullable=true)
     */
    private $homePhoneExtension;
    /**
     * @var string
     *
     * @ORM\Column(name="work_phone_extension", type="string", length=12, nullable=true)
     */
    private $workPhoneExtension;
    /**
     * @var string
     *
     * @ORM\Column(name="cell_phone_extension", type="string", length=12, nullable=true)
     */
    private $cellPhoneExtension;
    /**
     * @var string
     *
     * @ORM\Column(name="email_address", type="string", length=128, nullable=true)
     */
    private $emailAddress;
    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=32)
     */
    private $type;
    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     */
    private $active;

    /**
     *
     */
    public function __construct()
    {
        $this->setCreatedAt(new \DateTime());
        $this->setUpdatedAt(new \DateTime());
        $this->setType('both');
        $this->setActive(true);
    }

    /**
     * @return bool
     */
    public function getActive()
    {
        return $this->active === true;
    }

    /**
     * @param bool $active
     * @return $this
     */
    public function setActive($active = true)
    {
        $this->active = $active === true;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type = 'billing')
    {
        $valid = array('billing', 'shipping', 'both');

        $type = strtolower(trim($type));
        if (in_array($type, $valid)) {
            $this->type = $type;
        } else {
            $this->type = 'both';
        }

        return $this;
    }

    public function __toString()
    {
        $name = $this->getFirstName() . ' ' . $this->getLastName();
        $address1 = $this->getAddress1();
        $address2 = $this->getAddress2();
        $address = $address1 . (!empty($address2) ? '<br>' . $address2 : '');
        $location = $this->getCity() . ' ' . $this->getState() . ', ' . $this->getCountry() . ' ' . $this->getZipCode();

        return $name . '<br>' . $address . '<br>' . $location;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return Address
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return Address
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get address1
     *
     * @return string
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * Set address1
     *
     * @param string $address1
     * @return Address
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;

        return $this;
    }

    /**
     * Get address2
     *
     * @return string
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * Set address2
     *
     * @param string $address2
     * @return Address
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Address
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set state
     *
     * @param string $state
     * @return Address
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Address
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get zipCode
     *
     * @return string
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * Set zipCode
     *
     * @param string $zipCode
     * @return Address
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function prepersist()
    {
        $this->setUpdatedAt(new \DateTime());
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Address
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Address
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get companyName
     *
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Set companyName
     *
     * @param string $companyName
     * @return Address
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get homePhone
     *
     * @return string
     */
    public function getHomePhone()
    {
        return $this->homePhone;
    }

    /**
     * Set homePhone
     *
     * @param string $homePhone
     * @return Address
     */
    public function setHomePhone($homePhone)
    {
        $this->homePhone = $homePhone;

        return $this;
    }

    /**
     * Get workPhone
     *
     * @return string
     */
    public function getWorkPhone()
    {
        return $this->workPhone;
    }

    /**
     * Set workPhone
     *
     * @param string $workPhone
     * @return Address
     */
    public function setWorkPhone($workPhone)
    {
        $this->workPhone = $workPhone;

        return $this;
    }

    /**
     * Get cellPhone
     *
     * @return string
     */
    public function getCellPhone()
    {
        return $this->cellPhone;
    }

    /**
     * Set cellPhone
     *
     * @param string $cellPhone
     * @return Address
     */
    public function setCellPhone($cellPhone)
    {
        $this->cellPhone = $cellPhone;

        return $this;
    }

    /**
     * Get homePhoneExtension
     *
     * @return string
     */
    public function getHomePhoneExtension()
    {
        return $this->homePhoneExtension;
    }

    /**
     * Set homePhoneExtension
     *
     * @param string $homePhoneExtension
     * @return Address
     */
    public function setHomePhoneExtension($homePhoneExtension)
    {
        $this->homePhoneExtension = $homePhoneExtension;

        return $this;
    }

    /**
     * Get workPhoneExtension
     *
     * @return string
     */
    public function getWorkPhoneExtension()
    {
        return $this->workPhoneExtension;
    }

    /**
     * Set workPhoneExtension
     *
     * @param string $workPhoneExtension
     * @return Address
     */
    public function setWorkPhoneExtension($workPhoneExtension)
    {
        $this->workPhoneExtension = $workPhoneExtension;

        return $this;
    }

    /**
     * Get cellPhoneExtension
     *
     * @return string
     */
    public function getCellPhoneExtension()
    {
        return $this->cellPhoneExtension;
    }

    /**
     * Set cellPhoneExtension
     *
     * @param string $cellPhoneExtension
     * @return Address
     */
    public function setCellPhoneExtension($cellPhoneExtension)
    {
        $this->cellPhoneExtension = $cellPhoneExtension;

        return $this;
    }

    /**
     * Get emailAddress
     *
     * @return string
     */
    public function getEmailAddress()
    {
        return $this->emailAddress;
    }

    /**
     * Set emailAddress
     *
     * @param string $emailAddress
     * @return Address
     */
    public function setEmailAddress($emailAddress)
    {
        $this->emailAddress = $emailAddress;

        return $this;
    }

    public function valid( $array = false, $delim = '<br>' )
    {
        $required = array( 'first_name', 'last_name', 'email_address', 'address1', 'city', 'country', 'zip_code', 'state' );
        $errors = array();
        $has_errors = false;
        foreach( $required as $item ) {
            $function = 'get' . str_replace( ' ', '', ucwords( str_replace( '_', ' ', strtolower( trim( $item ) ) ) ) );
            if(method_exists( $this, $function)) {
                $value = $this->$function();
                if( empty( $value ) ) {
                    $has_errors = true;
                    $type = $this->getType();
                    if( $type == 'both' ) {
                        $type = "Address";
                    } else {
                        $type = ucwords( $type ) . ' Address';
                    }
                    $errors[] = $type . ' requires ' . ucwords( str_replace( '_', ' ', $item ) );
                }
            }
        }

        if( !$has_errors ) {
            return true;
        }

        return $array ? $errors : implode( $delim, $errors );
    }
}
