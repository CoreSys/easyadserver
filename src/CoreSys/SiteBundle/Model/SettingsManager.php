<?php

namespace CoreSys\SiteBundle\Model;

use CoreSys\MediaBundle\Entity\Image;
use CoreSys\SiteBundle\Entity\Config;
use CoreSys\SiteBundle\Model\BaseManager;

class SettingsManager extends BaseManager
{
    private $imageManager;

    public function getImageManager()
    {
        if( !empty( $this->imageManager ) ) {
            return $this->imageManager;
        }

        $this->imageManager = $this->get( 'core_sys_media.image_manager' );
        return $this->imageManager;
    }

    public function processSettingsForm( &$form, Config &$settings )
    {
        $this->processUploadedDefaultLogo( $settings );
        $this->processUploadedAltLogo( $settings );
        return $settings;
    }

    public function processUploadedDefaultLogo( Config &$settings )
    {
        $logo1 = $settings->getLogoFileOne();
        if( !empty( $logo1 ) ) {
            $manager = $this->getImageManager();
            $image = $manager->uploadNewImage( $logo1, false );
            if( $image instanceof Image ) {
                // first we need to remove the old logo if it already exists
                $logo = $settings->getLogo();
                if( !empty( $logo ) ) {
                    $settings->setLogo( null );
                    $this->persist( $settings );
                    $this->flush();
                    $manager->removeImage( $logo );
                }
                // logos should not have watermarks
                $image->setApplyWatermark( false );
                $image->setSlug('default_logo');
                $this->persist( $image );
                $this->flush();
                $settings->setLogo( $image );
            }
        }
        return true;
    }

    public function processUploadedAltLogo( Config &$settings )
    {
        $logo2 = $settings->getLogoFileTwo();
        if( !empty( $logo2 ) ) {
            $manager = $this->getImageManager();
            $image = $manager->uploadNewImage( $logo2, false );
            if( $image instanceof Image ) {
                // first we need to remove the old logo if it already exists
                $logo = $settings->getAltLogo();
                if( !empty( $logo ) ) {
                    $settings->setAltLogo( null );
                    $this->persist( $settings );
                    $this->flush();
                    $manager->removeImage( $logo );
                }
                // logos should not have watermarks
                $image->setApplyWatermark( false );
                $image->setSlug('alt_logo');
                $this->persist( $image );
                $this->flush();
                $settings->setAltLogo( $image );
            }
        }
        return true;
    }
}