<?php

namespace CoreSys\AdBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CoreSys\SiteBundle\Controller\AdminController as BaseController;

/**
 * Class AdminController
 * @package CoreSys\AdBundle\Controller
 * @Route("/admin/ads")
 */
class AdminController extends BaseController
{
   /**
    * @Route("/", name="admin_ads_index")
    * @Template()
    */
    public function indexAction()
    {
        return array();
    }
}
