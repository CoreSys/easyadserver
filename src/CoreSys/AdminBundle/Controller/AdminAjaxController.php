<?php

namespace CoreSys\AdminBundle\Controller;

use CoreSys\AdminBundle\Entity\Menu;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CoreSys\SiteBundle\Controller\AjaxController as BaseController;

/**
 * Class AdminController
 * @package CoreSys\AdminBundle\Controller
 * @Route("/admin/adminajax")
 */
class AdminAjaxController extends BaseController
{
    /**
     * @Route("/save_menu_order", name="admin_ajax_save_menu_order", options={"expose"=true})
     * @Template()
     */
    public function saveMenuOrderAction()
    {
        $request = $this->get( 'request' );
        if( $request->isMethod( 'POST' ) ) {
            $ids = $request->get('ids');
            if( !empty( $ids ) ) {
                $repo = $this->getRepo( 'CoreSysAdminBundle:Menu' );
                $position = 0;
                foreach( $ids as $id ) {
                    $id = trim( $id );
                    if( !empty( $id ) ) {
                        $menu = $repo->findOneById( $id );
                        if( !empty( $menu ) )
                        {
                            $menu->setPosition( $position );
                            $this->persist( $menu );
                            $position++;
                        }
                    }
                }
                $this->flush();
                $this->echoJsonSuccess( 'Successfully saved menu order', $ids );
            } else {
                $this->echoJsonError( 'No IDs supplied' );
            }
        } else {
            $this->echoJsonError( 'No post detected' );
        }
        exit;
    }

    /**
     * @Route("/toggle_menu_active/{id}", name="admin_ajax_toggle_menu_active", options={"expose"=true})
     * @ParamConverter("menu", class="CoreSysAdminBundle:Menu")
     * @Template()
     **/
    public function toggleMenuActiveAction( Menu $menu )
    {
        $active = !$menu->getActive();
        $menu->setActive( $active );
        $this->persistAndFlush( $menu );
        $this->echoJsonSuccess( 'Successfully set menu as ' . ( $active ? 'Active' : 'Inactive' ) . '<br>Please refresh the page to reflect the changes in the menu', array( 'active' => $active ) );
    }
}
