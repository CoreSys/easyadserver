<?php

namespace CoreSys\AdBundle\Model;

use CoreSys\SiteBundle\Model\BaseManager;

class BaseAdManager extends BaseManager
{
    protected $ad_type = 'default';

    public function getAdType()
    {
        return $this->ad_type;
    }
}