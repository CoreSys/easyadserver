<?php
/**
 * This file is part of the CoreSysUserBundle package.
 * (c) J&L Core Systems http://jlcoresystems.com | http://joshmccreight.com
 */

namespace CoreSys\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Tests\AbstractTableLayoutTest;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\Validator\Constraint\UserPassword;
use FOS\MessageBundle\FormType\NewThreadMessageFormType as BaseType;

/**
 * Class ReplyType
 * @package CoreSys\UserBundle\Form
 */
class ReplyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('participant', 'entity', array(
                'label' => 'From',
                'class' => 'CoreSysUserBundle:User',
                'attr' => array('class' => 'form-control select2', 'data-postdesc' => 'Who is sending this reply?')
            ))
            ->add('body', 'textarea');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'intention'  => 'thread_reply_message',
        ));
    }

    public function getName()
    {
        return 'core_sys_user_message_reply_thread';
    }
}