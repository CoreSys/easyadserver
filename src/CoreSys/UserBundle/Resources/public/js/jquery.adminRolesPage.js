;(function($,undefined){
    $.fn.adminRolesPage = function(options) {
        var p = this,
            defaults = {
                checkable: true,
                entity: 'Role',
                selected_count_selector: '.selected-count',
                check_master_selector: 'thead tr .master-check',
                check_row_selector: 'td .row-check',
                add_row_btn_selector: '.add-role',
                remove_rows_btn_selector: '.remove-roles',
                table_selector: '#roles-table',
                row_edit_btn_selector: '.btn-edit',
                row_remove_btn_selector: '.btn-remove',
                dt_display_length: 10,
                no_sort_columns: [0,8],
                no_show_columns: [0],
                new_modal_selector: '#new-modal',
                edit_modal_selector: '#edit-modal',
                remove_row_url: Routing.generate( 'admin_ajax_users_role_remove'),
                add_row_url: Routing.generate('admin_ajax_users_roles_new'),
                edit_row_url: Routing.generate('admin_ajax_users_roles_edit'),
                fnGetRowData: getRowData,
                fnPopulateEditedRowData: populateEditedRowData,
                onViewBtnClick: onViewBtnClick
            };

        p.settings = {};
        p.page = null;

        function init()
        {
            p.settings = $.extend({}, defaults, options );
            p.page = new $.fn.adminBasicTablePage(p.settings);
        }

        function getRowData(data) {
            var rowData = [];

            var sw = '<i class="fa fa-' + ( data.switch ? 'checkgreen' : 'times red' ) + '"></i>',
                ac = '<i class="fa fa-' + ( data.active ? 'checkgreen' : 'times red' ) + '"></i>',
                actions = '<a class="btn btn-xs btn-info btn-edit" href="javascript:void(0)" title="Edit ' + t.settings.entity + '"><i class="fa fa-edit"></i> Edit</a><a class="btn btn-xs btn-danger btn-remove" href="javascript:void(0)" title="Remove ' + t.settings.entity + '"><i class="fa fa-trash-o"></i> Remove</a>';

            // create the data row
            rowData[0] = '<input type="checkbox" data-id="' + data.id  + '" class="row-check" value="1">';
            rowData[1] = data.id;
            rowData[2] = data.name;
            rowData[3] = data.role;
            rowData[4] = data.parents;
            rowData[5] = data.users;
            rowData[6] = sw;
            rowData[7] = ac;
            rowData[8] = actions;

            return rowData;
        };

        function populateEditedRowData( row, data ) {
            var $row = $(row);

            $row.find('td').eq(2).html( data.name );
            $row.find('td').eq(3).html( data.role );
            $row.find('td').eq(4).html( data.parents );
            $row.find('td').eq(5).html( data.users );

            var sw = 'fa fa-check green';
            if( !data.switch ) {
                sw = 'fa fa-times red';
            }
            $row.find('td').eq(6).html( '<i class="' + sw + '"></i>' );

            var ac = 'fa fa-check green';
            if( !data.active ) {
                ac = 'fa fa-times red';
            }
            $row.find('td').eq(7).html( '<i class="' + ac + '"></i>' );

            $row.find('td').eq(2).css('color',data.color + ' !important');
        };

        function onViewBtnClick(e,btn,id) {
            var $btn = $(btn);
            e.preventDefault();
            var $modal = $('#view-modal');
            if(id === null || id === undefined) {
                id = $btn.attr('data-id');
            }
            $modal.modal({'remote': Routing.generate( 'admin_ajax_users_roles_users', {'id': id})});
            return false;
        }

        init();

        return this;
    };
})(jQuery);