;(function($,undefined){
    $.fn.adminCampaignPage = function(options) {
        var p = this,
            defaults = {
                checkable: true,
                entity: 'Campaign',
                selected_count_selector: '.selected-count',
                check_master_selector: 'thead tr .master-check',
                check_row_selector: 'td .row-check',
                add_row_btn_selector: '.add-campaign',
                remove_rows_btn_selector: '.remove-campaigns',
                table_selector: '#campaigns-table',
                row_edit_btn_selector: '.btn-edit',
                row_view_btn_selector: '.btn-view',
                row_remove_btn_selector: '.btn-remove',
                dt_display_length: 10,
                new_modal_selector: '#new-modal',
                edit_modal_selector: '#edit-modal',
                view_modal_selector: '#view-modal',
                no_sort_columns: [0,7],
                dataTables_aoColumns: [
                    {"sName": 'check', "bSortable": false, "bSearchable": false, "mData": 'check'},
//                    {"sName": 'id', "mData": 'id'},
                    {"sName": 'name', "mData": 'name'},
                    {"sName": 'description', "mData": 'description'},
                    {"sName": 'created_at', "bSearchable": false, "mData": 'created_at'},
                    {"sName": 'updated_at', "bSearchable": false, "mData": 'updated_at'},
                    {"sName": 'user', "mData": 'user'},
                    {"sName": 'active', "bSearchable": false, "mData": 'active'},
                    {"sName": 'options', "bSortable": false, "bSearchable": false, "mData": 'options'}
                ],
                dataTables_aoColumnFilter: [
                    null,
                    {"type":"text"},
                    {"type":"text"},
                    {"type":"date-range"},
                    {"type":"date-range"},
                    {"type":"text"},
                    {"type":"checkbox"},
                    null
                ],
                remove_row_url: Routing.generate( 'admin_ajax_campaigns_remove_campaign'),
                add_row_url: Routing.generate('admin_ajax_campaigns_new'),
                edit_row_url: Routing.generate('admin_ajax_campaigns_edit'),
                view_row_url: null,
                dataTables_ajax_url: Routing.generate('admin_ajax_datatables'),
                fnGetRowData: getRowData,
                fnPopulateEditedRowData: populateEditedRowData
            };

        p.settings = {};
        p.page = null;

        function init()
        {
            p.settings = $.extend({}, defaults, options );
            p.page = new $.fn.adminAjaxTablePage(p.settings);
            p.page.datatable.fnSort( [ [5,'desc'] ] )
        }

        function getRowData(data) {
            return data;
        };

        function populateEditedRowData( row, data ) {
            p.page.datatable.fnReloadAjax();
            p.page.resetRowSelects();
        };

        init();

        return this;
    };
})(jQuery);