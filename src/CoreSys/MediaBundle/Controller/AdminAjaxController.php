<?php

namespace CoreSys\MediaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CoreSys\SiteBundle\Controller\AjaxController as BaseAjaxController;

/**
 * Class DefaultController
 * @package CoreSys\MediaBundle\Controller
 *
 * @Route("/admin/media/ajax", options={"expose"=true})
 */
class DefaultController extends BaseAjaxController
{

}
