<?php

namespace CoreSys\PublisherBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CoreSys\SiteBundle\Controller\AdminController as BaseController;

/**
 * Class AdminController
 * @package CoreSys\PublisherBundle\Controller
 *
 * @Route("/admin/publishers")
 */
class AdminController extends BaseController
{
    /**
     * @Route("/", name="admin_publishers_index")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }
}
