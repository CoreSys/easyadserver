<?php

    /**
     * This file is part of the CoreSysUserBundle package.
     * (c) J&L Core Systems http://jlcoresystems.com | http://joshmccreight.com
     */

    namespace CoreSys\UserBundle\Model;

    use CoreSys\SiteBundle\Model\BaseManager;
    use CoreSys\UserBundle\Entity\Message;
    use CoreSys\UserBundle\Entity\Thread;
    use CoreSys\UserBundle\Entity\User;
    use CoreSys\UserBundle\Form\ReplyType;
    use CoreSys\UserBundle\FormModel\AdminNewThread;
    use Doctrine\Common\Collections\ArrayCollection;
    use CoreSys\UserBundle\FormModel\ReplyMessage;
    use FOS\MessageBundle\ModelManager\ThreadManagerInterface;
    use CoreSys\UserBundle\Form\ThreadType;
    use FOS\MessageBundle\Security\AuthorizerInterface;
    use FOS\MessageBundle\Reader\ReaderInterface;
    use FOS\MessageBundle\Security\ParticipantProviderInterface;
    use FOS\MessageBundle\ModelManager\MessageManagerInterface;
    use FOS\MessageBundle\Model\ThreadInterface;
    use FOS\MessageBundle\Sender\SenderInterface;
    use FOS\MessageBundle\Composer\ComposerInterface;
    use FOS\MessageBundle\Model\MessageInterface;
    use FOS\MessageBundle\Model\ParticipantInterface;
    use FOS\MessageBundle\Provider\ProviderInterface;
    use Symfony\Component\Form\Form;

    /**
     * Class MessageManager
     * @package CoreSys\UserBundle\Model
     */
    class MessageManager extends BaseManager
    {

        /**
         * @var SenderInterface
         */
        public $sender;

        /**
         * @var ProviderInterface
         */
        private $provider;

        /**
         * @var ThreadManagerInterface
         */
        private $thread_manager;

        /**
         * @var ComposerInterface
         */
        private $composer;

        /**
         * @var ParticipantProviderInterface
         */
        private $participant_provider;

        /**
         * @var MessageManagerInterface
         */
        private $message_manager;

        /**
         * @param null $threadid
         *
         * @return mixed
         */
        public function getThread( $threadid = NULL )
        {
            return $this->getProvider()->getThread( $threadid );
        }

        /**
         * @return ProviderInterface|mixed
         */
        public function getProvider()
        {
            if ( !empty( $this->provider ) ) {
                return $this->provider;
            }

            $this->provider = $this->get( 'fos_message.provider' );

            return $this->provider;
        }

        /**
         * @param ParticipantInterface $user
         *
         * @return mixed
         */
        public function unreadCount( ParticipantInterface $user = NULL )
        {
            if ( empty( $user ) ) {
                $provider = $this->getProvider();

                return $provider->getNbUnreadMessages();
            } else {
                $manager = $this->getMessageManager();

                return $manager->getNbUnreadMessageByParticipant( $user );
            }
        }

        /**
         * @return MessageManagerInterface|mixed
         */
        public function getMessageManager()
        {
            if ( !empty( $this->message_manager ) ) {
                return $this->message_manager;
            }

            $this->message_manager = $this->get( 'fos_message.message_manager' );

            return $this->message_manager;
        }

        /**
         * Get datatables results
         */
        public function getInboxDatatablesResults( $data, ParticipantInterface $user = NULL )
        {
            $threads = $this->getInboxThreads( $user );
            $total   = count( $threads );

            // now we need to sort the threads by the specified parameters
            $sorted = $this->datatablesSortThreads( $data, $threads );

            // now lets filter the sorted threads by column filters
            // or in our case search parameters
            $filtered = $this->datatablesFilterThreads( $data, $sorted );

            // lets limit the return results to the limit we have requested
            $limited = $this->datatablesLimitThread( $data, $filtered );

            $return = array(
                'sEcho'                => $data[ 'echo' ],
                'iTotalRecords'        => count( $filtered ),
                'iTotalDisplayRecords' => $total,
                'aaData'               => array()
            );

            foreach ( $limited as $idx => $thread ) {
                $tdata = $this->getDatatablesThreadEntityDataRow( $thread, $data[ 'searchString' ], $user, 'inbox' );
                if ( !isset( $return[ 'aaData' ][ $idx ] ) ) {
                    $return[ 'aaData' ][ $idx ] = array();
                }

                if ( empty( $data[ 'columns' ][ 0 ][ 'id' ] ) ) {
                    $return[ 'aaData' ][ $idx ][ 0 ] = NULL;
                }

                foreach ( $tdata as $k => $v ) {
                    $return[ 'aaData' ][ $idx ][ $k ] = $v;
                }

                if ( empty( $data[ 'columns' ][ count( $data[ 'columns' ] ) - 1 ][ 'id' ] ) ) {
                    $return[ 'aaData' ][ $idx ][ count( $data[ 'columns' ] ) - 1 ] = NULL;
                }
            }

            return $return;
        }

        /**
         * @param ParticipantInterface $user
         *
         * @return mixed
         */
        public function getInboxThreads( ParticipantInterface $user = NULL )
        {
            if ( empty( $user ) ) {
                $user = $this->getAuthenticatedParticipant();
            }

            $manager = $this->getThreadManager();
            $threads = $manager->findParticipantInboxThreads( $user );

            return $threads;
        }

        /**
         * Gets the current authenticated user
         *
         * @return ParticipantInterface
         */
        public function getAuthenticatedParticipant()
        {
            return $this->getParticipantProvider()->getAuthenticatedParticipant();
        }

        /**
         * @return ParticipantProviderInterface|mixed
         */
        public function getParticipantProvider()
        {
            if ( !empty( $this->participant_provider ) ) {
                return $this->participant_provider;
            }

            $this->participant_provider = $this->get( 'fos_message.participant_provider' );

            return $this->participant_provider;
        }

        /**
         * @return ThreadManagerInterface|mixed
         */
        public function getThreadManager()
        {
            if ( !empty( $this->thread_manager ) ) {
                return $this->thread_manager;
            }

            $this->thread_manager = $this->get( 'fos_message.thread_manager' );

            return $this->thread_manager;
        }

        /**
         * @param $data
         * @param $threads
         *
         * @return array
         */
        public function datatablesSortThreads( $data, $threads )
        {
            $sorted   = array();
            $sort_col = $data[ 'columns' ][ $data[ 'sortColumn' ] ][ 'id' ];
            $sort_dir = strtolower( trim( $data[ 'sortDir' ] ) );
            $sort_col = preg_replace( '/_(.?)/e', "strtoupper('$1')", $sort_col );

            switch ( $sort_col ) {
                case 'createdAt':
                    $sorted = $this->sortThreadsByCreatedAt( $threads );
                    break;
                case 'from':
                    $sorted = $this->sortThreadsByCreatedBy( $threads );
                    break;
                case 'subject':
                    $sorted = $this->sortThreadsBySubject( $threads );
                    break;
                case 'read':
                    $sorted = $this->sortThreadsByRead( $threads );
                    break;
                default:
                    $sorted = $threads;
                    break;
            }

            if ( $sort_dir == 'desc' ) {
                $sorted = array_reverse( $sorted );
            }

            return $sorted;
        }

        /**
         * @param $threads
         *
         * @return array
         */
        public function sortThreadsByCreatedAt( $threads )
        {
            $return = array();
            $sorted = array();

            foreach ( $threads as $thread ) {
                $ts = $thread->getLastCreatedAt()->getTimestamp();
                if ( !isset( $sorted[ $ts ] ) ) {
                    $sorted[ $ts ] = array();
                }

                $sorted[ $ts ][ ] = $thread;
            }

            ksort( $sorted );

            foreach ( $sorted as $ts => $ts_threads ) {
                foreach ( $ts_threads as $thread ) {
                    $return[ ] = $thread;
                }
            }

            unset( $sorted );

            return $return;
        }

        /**
         * @param $threads
         *
         * @return array
         */
        public function sortThreadsByCreatedBy( $threads )
        {
            $return = array();
            $sorted = array();

            foreach ( $threads as $thread ) {
                $username = $thread->getLastCreatedBy()->getUsername();
                $ts       = $thread->getLastCreatedAt()->getTimestamp();
                if ( !isset( $sorted[ $username ] ) ) {
                    $sorted[ $username ] = array();
                }
                if ( !isset( $sorted[ $username ][ $ts ] ) ) {
                    $sorted[ $username ][ $ts ] = array();
                }
                $sorted[ $username ][ $ts ][ ] = $thread;
            }

            // sort the timestamps
            foreach ( $sorted as $username => $tsthreads ) {
                ksort( $sorted[ $username ] );
            }

            ksort( $sorted );

            foreach ( $sorted as $sthreads ) {
                foreach ( $sthreads as $ts_threads ) {
                    foreach ( $ts_threads as $thread ) {
                        $return[ ] = $thread;
                    }
                }
            }

            unset( $sorted );

            return $return;
        }

        /**
         * @param $threads
         *
         * @return array
         */
        public function sortThreadsBySubject( $threads )
        {
            $return = array();
            $sorted = array();

            foreach ( $threads as $thread ) {
                $subject = $thread->getSubject();
                $subject = strtolower( trim( $subject ) );
                $subject = preg_replace( '/[^a-z0-9]+/', '', $subject );
                if ( !isset( $sorted[ $subject ] ) ) {
                    $sorted[ $subject ][ ] = $thread;
                }
            }

            ksort( $sorted );

            foreach ( $sorted as $sub => $sthreads ) {
                foreach ( $sthreads as $thread ) {
                    $return[ ] = $thread;
                }
            }

            unset( $sorted );

            return $return;
        }

        /**
         * @param $threads
         *
         * @return array
         */
        public function sortThreadsByRead( $threads )
        {
            $sorted = array( 0 => array(), 1 => array() );
            $return = array();

            foreach ( $threads as $thread ) {
                $ts   = $thread->getLastCreatedAt()->getTimestamp();
                $read = $thread->hasUnread() ? 0 : 1;
                if ( !isset( $sorted[ $read ][ $ts ] ) ) {
                    $sorted[ $read ][ $ts ] = array();
                }
                $sorted[ $read ][ $ts ][ ] = $thread;
            }

            foreach ( $sorted as $k => $v ) {
                ksort( $sorted[ $k ] );
            }

            ksort( $sorted );

            foreach ( $sorted as $read => $rthreads ) {
                foreach ( $rthreads as $ts => $tsthreads ) {
                    foreach ( $tsthreads as $thread ) {
                        $return[ ] = $thread;
                    }
                }
            }

            unset( $sorted );

            return $return;
        }

        /**
         * @param $data
         * @param $threads
         *
         * @return array
         */
        public function datatablesFilterThreads( $data, $threads )
        {
            $filtered      = array();
            $search_string = $data[ 'searchString' ];

            if ( empty( $search_string ) ) {
                return $threads;
            }

            foreach ( $data[ 'columns' ] as $col ) {
                if ( $col[ 'searchable' ] ) {
                    $col[ 'id' ] = preg_replace( '/_(.?)/e', "strtoupper('$1')", $col[ 'id' ] );
                    foreach ( $threads as $thread ) {
                        $match = FALSE;
                        if ( $col[ 'id' ] == 'from' ) {
                            $from = $thread->getLastCreatedBy()->getUsername();
                            if ( strstr( $from, $search_string ) ) {
                                $match = TRUE;
                            }
                        } else if ( $col[ 'id' ] == 'subject' ) {
                            $subject = $thread->getSubject();
                            if ( strstr( $subject, $search_string ) ) {
                                $match = TRUE;
                            }
                        }

                        if ( $match ) {
                            $filtered[ ] = $thread;
                        }
                    }
                }
            }

            return $filtered;
        }

        /**
         * @param $data
         * @param $threads
         *
         * @return array
         */
        public function datatablesLimitThread( $data, $threads )
        {
            $limit  = intval( $data[ 'limit' ] );
            $offset = intval( $data[ 'offset' ] );

            $return = array_slice( $threads, $offset, $limit );

            return $return;
        }

        /**
         * @param      $thread
         * @param null $search_string
         * @param null $user
         *
         * @return array
         */
        public function getDatatablesThreadEntityDataRow( $thread, $search_string = NULL, $user = NULL, $type = 'inbox' )
        {
            $search_string = trim( $search_string );

            $date    = $thread->getLastCreatedAt()->format( 'M d, Y g:i a' );
            $from    = $thread->getLastCreatedBy()->getUsername();
            $subject = $thread->getSubject();
            $id      = $thread->getId();
            $data_id = 'data-id="' . $id . '"';

            if ( !empty( $user ) ) {
                $is_read = $thread->isReadByParticipant( $user );
            } else {
                $is_read = $thread->hasUnread();
            }

            $check   = '<input data-read="' . ( !$is_read ? '0' : '1' ) . '" type="checkbox" ' . $data_id . ' class="row-check" value="' . $id . '" />';
            $actions = array(
                '<li class="mark-as-read text-left" ' . $data_id . '><a href="javascript:void(0)" ' . $data_id . ' class="btn btn-mark-as-read"><i class="icon-mail-read"></i> Mark as Read</a></li>',
                '<li class="mark-as-unread text-left" ' . $data_id . '><a href="javascript:void(0)" ' . $data_id . ' class="btn btn-mark-as-unread"><i class="icon-mail-unread"></i> Mark as UnRead</a></li>',
                '<li class="thread-reply text-left" ' . $data_id . '><a href="javascript:void(0)" ' . $data_id . ' class="btn btn-reply"><i class="fa fa-reply"></i> Reply</a></li>',
                '<li class="undelete text-left" ' . $data_id . '><a href="javascript:void(0)" ' . $data_id . ' class="btn btn-undelete"><i class="fa fa-refresh"></i> UnDelete</a></li>',
                '<li class="remove-row text-left" ' . $data_id . '><a href="javascript:void(0)" ' . $data_id . ' class="btn btn-remove"><i class="fa fa-times red"></i> Delete</a></li>',
            );
            $actions = '<div class="btn-group" ' . $data_id . '><button class="btn btn-xs dropdown-toggle" type="button" data-toggle="dropdown">Options <i class="fa fa-angle-down"></i></button><ul class="dropdown-menu pull-right" role="menu">' . implode( $actions ) . '</ul></div>';

            $read = '<i class="icon-mail-' . ( !$is_read ? 'unread' : 'read' ) . '"></i>';

            if ( !empty( $search_string ) ) {
                $from    = preg_replace( "/($search_string)/i", "<span class=\"highlight\">$1</span>", $from );
                $subject = preg_replace( "/($search_string)/i", "<span class=\"highlight\">$1</span>", $subject );
            }

            $to = !empty( $user ) ? $user->getUsername() : 'N/A';

            if ( $type == 'sent' ) {
                $from = $to;
                $to   = array();
                foreach ( $thread->getOtherParticipants( $user ) as $p ) {
                    $to[ ] = $p->getUsername();
                }
                $to = implode( ',', $to );
            }

            $msgs = $thread->getMessages();
            $msgs = count( $msgs );

            return array(
                'check'      => $check,
                'read'       => $read,
                'from'       => $from,
                'to'         => $to,
                'subject'    => $subject,
                'msgs'       => intval( $msgs ),
                'created_at' => $date,
                'actions'    => $actions,
                'id'         => $id
            );
        }

        /**
         * Get datatables results
         */
        public function getSentDatatablesResults( $data, ParticipantInterface $user = NULL )
        {
            $threads = $this->getSentThreads( $user );
            $total   = count( $threads );

            // now we need to sort the threads by the specified parameters
            $sorted = $this->datatablesSortThreads( $data, $threads );

            // now lets filter the sorted threads by column filters
            // or in our case search parameters
            $filtered = $this->datatablesFilterThreads( $data, $sorted );

            // lets limit the return results to the limit we have requested
            $limited = $this->datatablesLimitThread( $data, $filtered );

            $return = array(
                'sEcho'                => $data[ 'echo' ],
                'iTotalRecords'        => count( $filtered ),
                'iTotalDisplayRecords' => $total,
                'aaData'               => array()
            );

            foreach ( $limited as $idx => $thread ) {
                $tdata = $this->getDatatablesThreadEntityDataRow( $thread, $data[ 'searchString' ], $user, 'sent' );
                if ( !isset( $return[ 'aaData' ][ $idx ] ) ) {
                    $return[ 'aaData' ][ $idx ] = array();
                }

                if ( empty( $data[ 'columns' ][ 0 ][ 'id' ] ) ) {
                    $return[ 'aaData' ][ $idx ][ 0 ] = NULL;
                }

                foreach ( $tdata as $k => $v ) {
                    $return[ 'aaData' ][ $idx ][ $k ] = $v;
                }

                if ( empty( $data[ 'columns' ][ count( $data[ 'columns' ] ) - 1 ][ 'id' ] ) ) {
                    $return[ 'aaData' ][ $idx ][ count( $data[ 'columns' ] ) - 1 ] = NULL;
                }
            }

            return $return;
        }

        /**
         * @param ParticipantInterface $user
         *
         * @return mixed
         */
        public function getSentThreads( ParticipantInterface $user = NULL )
        {
            if ( !empty( $user ) ) {
                $manager = $this->getThreadManager();
                $threads = $manager->findParticipantSentThreads( $user );
            } else {
                // get the threads of the current authorized user */
                $provider = $this->getProvider();
                $threads  = $provider->getSentThreads();
            }

            return $threads;
        }

        /**
         * Get datatables results
         */
        public function getTrashDatatablesResults( $data, ParticipantInterface $user = NULL )
        {
            $threads = $this->getTrashThreads( $user );
            $total   = count( $threads );

            // now we need to sort the threads by the specified parameters
            $sorted = $this->datatablesSortThreads( $data, $threads );

            // now lets filter the sorted threads by column filters
            // or in our case search parameters
            $filtered = $this->datatablesFilterThreads( $data, $sorted );

            // lets limit the return results to the limit we have requested
            $limited = $this->datatablesLimitThread( $data, $filtered );

            $return = array(
                'sEcho'                => $data[ 'echo' ],
                'iTotalRecords'        => count( $filtered ),
                'iTotalDisplayRecords' => $total,
                'aaData'               => array()
            );

            foreach ( $limited as $idx => $thread ) {
                $tdata = $this->getDatatablesThreadEntityDataRow( $thread, $data[ 'searchString' ], $user, 'trash' );
                if ( !isset( $return[ 'aaData' ][ $idx ] ) ) {
                    $return[ 'aaData' ][ $idx ] = array();
                }

                if ( empty( $data[ 'columns' ][ 0 ][ 'id' ] ) ) {
                    $return[ 'aaData' ][ $idx ][ 0 ] = NULL;
                }

                foreach ( $tdata as $k => $v ) {
                    $return[ 'aaData' ][ $idx ][ $k ] = $v;
                }

                if ( empty( $data[ 'columns' ][ count( $data[ 'columns' ] ) - 1 ][ 'id' ] ) ) {
                    $return[ 'aaData' ][ $idx ][ count( $data[ 'columns' ] ) - 1 ] = NULL;
                }
            }

            return $return;
        }

        /**
         * @param ParticipantInterface $user
         *
         * @return \FOS\MessageBundle\Model\ThreadInterface[]|\FOS\MessageBundle\Provider\ThreadInterface[]
         */
        public function getTrashThreads( ParticipantInterface $user = NULL )
        {
            if ( !empty( $user ) ) {
                $manager = $this->getThreadManager();
                $threads = $manager->findParticipantDeletedThreads( $user );
            } else {
                $provider = $this->getProvider();
                $threads  = $provider->getDeletedThreads();
            }

            return $threads;
        }

        /**
         * Process amdin new thread form
         *
         * @param Form $form
         *
         * @return mixed
         */
        public function processAdminNewThreadForm( Form $form = NULL )
        {
            if ( empty( $form ) ) {
                $form = $this->getAdminNewThreadForm();
            }

            $request = $this->get( 'request' );
            $form->handleRequest( $request );
            if ( $form->isValid() ) {
                $threadModal = $form->getData();
                $from        = $threadModal->getCreatedBy();
                $to          = $threadModal->getRecipient();
                $subject     = $threadModal->getSubject();
                $body        = $threadModal->getBody();

                $result = $this->compose( $from, $to, $subject, $body, TRUE );

                return $result;
            } else {
                return 'Error in the form';
            }
        }

        /**
         * Get a new thread form
         */
        public function getAdminNewThreadForm()
        {
            $model = new AdminNewThread();
            $form  = $this->get( 'form.factory' )->create( new ThreadType(), $model );

            return $form;
        }

        /**
         * Compose a new message and either send it or return it
         *
         * @param ParticipantInterface|null $sender
         * @param ParticipantInterface|null $recipient
         * @param string                    $subject
         * @param string                    $body
         * @param boolean                   $send - send the message (true) or return the message (false)
         *
         * @return mixed
         */
        public function compose( ParticipantInterface $sender = NULL, ParticipantInterface $recipient = NULL, $subject = NULL, $body = NULL, $send = TRUE )
        {
            if ( empty( $sender ) ) {
                // if not sender is provided
                // we will use the currently authenticated user
                $sender = $this->getAuthenticatedParticipant();
            }

            if ( empty( $recipient ) ) {
                // normally, this is used for testing, but if no recipient is
                // specified, then it will send to the sender user
                $recipient = $sender;
            }

            $composer = $this->getComposer();
            $message  = $composer->newThread()
                                 ->setSender( $sender )
                                 ->addRecipient( $recipient )
                                 ->setSubject( $subject )
                                 ->setBody( $body )
                                 ->getMessage();

            if ( $send ) {
                return $this->sendMessage( $message );
            }

            return $message;
        }

        /**
         * @return ComposerInterface|mixed
         */
        public function getComposer()
        {
            if ( !empty( $this->composer ) ) {
                return $this->composer;
            }

            $this->composer = $this->get( 'fos_message.composer' );

            return $this->composer;
        }

        /**
         * Send a message
         *
         * @param MessageInterface $message
         *
         * @return bool
         */
        public function sendMessage( MessageInterface $message = NULL )
        {
            if ( empty( $message ) ) {
                return NULL;
            }

            $sender = $this->getSender();
            $sender->send( $message );

            return TRUE;
        }

        /**
         * @return SenderInterface|mixed
         */
        public function getSender()
        {
            if ( !empty( $this->sender ) ) {
                return $this->sender;
            }

            $this->sender = $this->get( 'fos_message.sender' );

            return $this->sender;
        }

        /**
         * @param Thread $thread
         * @param Form   $form
         *
         * @return bool|string
         */
        public function processAdminReplyThread( Thread $thread, Form $form = NULL )
        {
            if ( empty( $form ) ) {
                $form = $this->getAdminReplyThreadForm( $thread, NULL );
            }

            $request = $this->get( 'request' );
            $form->handleRequest( $request );
            if ( $form->isValid() ) {
                $model = $form->getData();
                $from  = $model->getParticipant();
                $body  = $model->getBody();

                $result = $this->reply( $from, $thread, $body, TRUE );
                if ( $result instanceof Message || $result === TRUE ) {
                    return TRUE;
                } else {
                    return FALSE;
                }
            } else {
                return 'Could not send reply';
            }
        }

        /**
         * @param Thread               $thread
         * @param ParticipantInterface $user
         *
         * @return mixed
         */
        public function getAdminReplyThreadForm( Thread $thread, ParticipantInterface $user = NULL )
        {
            if ( empty( $user ) ) {
                $user = $this->getAuthenticatedParticipant();
            }
            $model = new ReplyMessage();
            $model->setParticipant( $user );
            $model->setThread( $thread );

            $form = $this->get( 'form.factory' )->create( new ReplyType(), $model );

            return $form;
        }

        /**
         * Reply to a thread
         *
         * @param ParticipantInterface|null $sender
         * @param ThreadInterface|null      $thread
         * @param string                    $body
         * @param boolean                   $send
         *
         * @return mixed
         */
        public function reply( ParticipantInterface $sender, ThreadInterface $thread = NULL, $body = NULL, $send = TRUE )
        {
            if ( empty( $sender ) ) {
                $sender = $this->getAuthenticatedParticipant();
            }

            if ( empty( $thread ) ) {
                return FALSE;
            }

            $composer = $this->getComposer();
            $message  = $composer->reply( $thread )
                                 ->setSender( $sender )
                                 ->setBody( $body )
                                 ->getMessage();

            if ( $send ) {
                return $this->sendMessage( $message );
            }

            return $message;
        }

        /**
         * @param ParticipantInterface $user
         *
         * @return bool
         */
        public function emptyTrash( ParticipantInterface $user = NULL )
        {
            $threads = $this->getTrashThreads( $user );
            $manager = $this->getThreadManager();
            foreach ( $threads as $thread ) {
                $manager->deleteThread( $thread );
            }

            return TRUE;
        }
    }