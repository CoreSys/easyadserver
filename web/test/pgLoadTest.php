<?php
    function write() {
        $dir = dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 'logs';
        if(!is_dir( $dir ) ) {
            @mkdir( $dir, 0777 );
            @chmod( $dir, 0777 );
        }

        $time = microtime( true );
        $file = $dir . DIRECTORY_SEPARATOR . $time . '.log.gz';
        $exists = file_exists( $file );

        $data = $_SERVER;
        $data[ 'REQUEST_VARS' ] = $_REQUEST;
        $data = serialize( $data );

        if( $exists ) {
            // append to the file
            $lines = gzFile( $file );
            $lines[] = $data;
            $write = array();
            foreach( $lines as $line ) {
                $line = trim( $line );
                if(!empty($line)) {
                    $write[] = $line;
                }
            }
            unset( $lines );
            $write = implode( "\n", $write );
        } else {
            // write the file
            $write = $data;
        }

        unset( $data );
        $gz = gzopen( $file, 'w9' );
        gzwrite( $gz, $write );
        gzclose( $gz );

        // write the corresponding data to the benchmark database
        $link = pg_Connect("dbname=jladserv_bench user=jladserv_tester password=fr4nkwh0r3");

        $sql = "INSERT INTO bench.bench (time,data) VALUES('$time','$write')";

        $result = pg_exec($link, $sql);
    }

    for( $count = 10; $count <= 100000; $count += 10 ) {
        $start = microtime( true );
        $num = 0;
        for( $i = 0; $i < $count; $i++ ) {
            write();
        }
        echo '<br>';
        echo 'Wrote ' . $i . ' rows in: ' . (microtime( true ) - $start) . ' seconds';
        echo '<br>';
        sleep( 1 );
    }