<?php

namespace CoreSys\AdBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Website
 *
 * @ORM\Table(name="ad_campaign_website")
 * @ORM\Entity(repositoryClass="CoreSys\AdBundle\Entity\WebsiteRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Website
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    private $url;

    /**
     * @var boolean
     *
     * @ORM\Column(name="secure", type="boolean", nullable=true)
     */
    private $secure;

    /**
     * @var boolean
     *
     * @ORM\Column(name="allow", type="boolean", nullable=true)
     */
    private $allow;

    /**
     * @var Campaign
     *
     * @ORM\ManyToOne(targetEntity="Campaign", inversedBy="websites")
     * @ORM\JoinColumn(name="campaign_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $campaign;

    public function setCampaign( Campaign $campaign )
    {
        $this->campaign = $campaign;
        return $this;
    }

    public function getCampaign( )
    {
        return $this->campaign;
    }

    /**
     * @param boolean $secure
     */
    public function setSecure($secure)
    {
        $this->secure = $secure;
    }

    /**
     * @return boolean
     */
    public function getSecure()
    {
        return $this->secure;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Website
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    public function __construct()
    {
        $this->setSecure( false );
    }

    /**
     * @ORM\PrePersist
     */
    public function prepersist()
    {
        $url = $this->getUrl();
        $lower_url = strtolower( trim( $url ) );
        if( strstr( $lower_url, 'https' ) ) {
            $this->setSecure( true );
        }

        $url = str_replace( 'Https://', '', $url );
        $url = str_replace( 'https://', '', $url );
        $url = str_replace( 'HTTPS://', '', $url );

        $this->setUrl( $url );
    }
}
