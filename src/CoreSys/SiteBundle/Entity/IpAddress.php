<?php

namespace CoreSys\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * IpAddress
 *
 * @ORM\Table(name="ip_address")
 * @ORM\Entity(repositoryClass="CoreSys\SiteBundle\Entity\IpAddressRepository")
 * @ORM\HasLifecycleCallbacks
 */
class IpAddress
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="ip_address", type="string", length=32)
     */
    private $ipAddress;

    /**
     * @var boolean
     *
     * @ORM\Column(name="black_listed", type="boolean", nullable=true)
     */
    private $blackListed;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return IpAddress
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return IpAddress
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set ipAddress
     *
     * @param string $ipAddress
     * @return IpAddress
     */
    public function setIpAddress($ipAddress)
    {
        $this->ipAddress = $ipAddress;

        return $this;
    }

    /**
     * Get ipAddress
     *
     * @return string 
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    /**
     * Set blackListed
     *
     * @param boolean $blackListed
     * @return IpAddress
     */
    public function setBlackListed($blackListed = false)
    {
        $this->blackListed = $blackListed === true;

        return $this;
    }

    /**
     * Get blackListed
     *
     * @return boolean 
     */
    public function getBlackListed()
    {
        return $this->blackListed;
    }

    public function __construct()
    {
        $this->setCreatedAt( new \DateTime() );
        $this->setUpdatedAt( new \DateTime() );
        $this->setBlackListed( false );
    }

    /**
     * @ORM\PrePersist
     */
    public function prepersist()
    {
        $this->setUpdatedAt( new \DateTime() );
    }
}
