<?php

namespace CoreSys\SiteBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Doctrine\ORM\Events;

class CoreSysSiteBundle extends Bundle
{
    public function boot()
    {
        $em = $this->container->get( 'doctrine.orm.default_entity_manager' );
        $discriminatorMapListener = $this->container->get( 'core_sys_site.listener.discriminator_map' );

        $evm = $em->getEventManager();
        $evm->addEventListener( Events::loadClassMetadata, $discriminatorMapListener );
    }
}
