<?php

namespace CoreSys\SiteBundle\Model;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class BaseManager
 * @package CoreSys\SiteBundle\Model
 */
class BaseManager
{

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entity_manager;

    /**
     * @var
     */
    private $output;

    /**
     * @var bool
     */
    private $log;

    /**
     * @var \Symfony\Component\DependencyInjection\Container
     */
    private $container;

    /**
     * @var
     */
    private $upload_folder;

    /**
     * @param EntityManager $em
     * @param Container $container
     */
    public function __construct(EntityManager $em, Container $container)
    {
        $this->entity_manager = $em;
        $this->log = FALSE;
        $this->container = $container;
    }

    /**
     * @param $name
     *
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getRepo($name)
    {
        return $this->getEntityManager()->getRepository($name);
    }

    /**
     * @param $name
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getRepository($name)
    {
        return $this->getRepo($name);
    }

    /**
     * @param $entity
     */
    public function persist($entity)
    {
        if (is_object($entity)) {
            if (method_exists($entity, 'setContainer')) {
                $container = $this->container;
                $entity->setContainer($container);
            }
            $functions = array('PrePersist', 'prePersist', 'PrePersist');
            foreach ($functions as $function) {
                if (method_exists($entity, $function)) {
                    $entity->$function();
                }
            }
        }
        $this->getEntityManager()->persist($entity);
    }

    /**
     * @param       $entity
     * @param array $params
     */
    public function remove($entity, $params = array())
    {
        if (is_object($entity)) {
            if (method_exists($entity, 'setContainer')) {
                $container = $this->container;
                $entity->setContainer($container);
            }
            $functions = array('preremove', 'preRemove', 'PreRemove', 'predelete', 'preDelete', 'PreDelete', 'remove', 'delete');
            foreach ($functions as $function) {
                if (method_exists($entity, $function)) {
                    call_user_func_array(array($entity, $function), $params);
                }
            }
        }
        $this->getEntityManager()->remove($entity);
    }

    /**
     *
     */
    public function flush()
    {
        $this->getEntityManager()->flush();
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->entity_manager;
    }

    /**
     * @param $entity_manager
     */
    public function setEntityManager($entity_manager)
    {
        $this->entity_manager = $entity_manager;
    }

    /**
     * @return bool
     */
    public function getLog()
    {
        return $this->log;
    }

    /**
     * @param $log
     */
    public function setLog($log)
    {
        $this->log = $log;
    }

    /**
     * @return mixed
     */
    public function getOutput()
    {
        return $this->output;
    }

    /**
     * @param $output
     */
    public function setOutput($output)
    {
        $this->output = $output;
    }

    /**
     * @param $msg
     */
    public function log($msg)
    {
        if (!$this->log) {
            return;
        }
        if (is_array($msg)) {
            $msg = implode('<br>', $msg);
        }
        if (!empty($this->output)) {
            $msg = str_replace('<br>', "\n", $msg);
            $this->output->writeln($msg);
        } else {
            $msg = preg_replace("/[\r\n]+/", '<br>', $msg);
            echo $msg . '<br>';
        }
    }

    /**
     * @return string
     */
    public function getMediaImagesFolder()
    {
        $folder = $this->getMediaFilesFolder() . DIRECTORY_SEPARATOR . 'photos';
        return $this->verifyFolder( $folder );
    }

    /**
     * @return string
     */
    public function getMediaVideosFolder()
    {
        return $this->getMediaFilesFolder() . DIRECTORY_SEPARATOR . 'videos';
    }

    /**
     * @return string
     */
    public function getMediaFilesFolder()
    {
        $folder = $this->getRootFolder() . DIRECTORY_SEPARATOR . 'media_files';
        return $this->verifyFolder( $folder );
    }

    /**
     * @return string
     */
    public function getRootFolder()
    {
        $base = dirname(__FILE__);
        $web = $base . DIRECTORY_SEPARATOR . 'web';
        while (!is_dir($web)) {
            $base = dirname($base);
            $web = $base . DIRECTORY_SEPARATOR . 'web';
        }

        return $base;
    }

    /**
     * @return null
     */
    public function getUploadsFolder()
    {
        return $this->getUploadFolder();
    }

    /**
     * @return null
     */
    public function getUploadFolder()
    {
        if (!empty($this->upload_folder)) {
            return $this->upload_folder;
        }
        $folder = $this->getWebFolder() . DIRECTORY_SEPARATOR . 'upload';
        $this->upload_folder = $this->verifyFolder($folder);

        return $this->upload_folder;
    }

    /**
     * @return string
     */
    public function getWebFolder()
    {
        return $this->getRootFolder() . DIRECTORY_SEPARATOR . 'web';
    }

    /**
     * @param null $folder
     * @return null
     */
    public function verifyFolder($folder = NULL)
    {
        if (!is_dir($folder)) {
            @mkdir($folder, 0777);
            @chmod($folder, 0777);
        }

        return $folder;
    }

    /**
     * @param $name
     * @return mixed
     */
    public function get($name)
    {
        return $this->container->get($name);
    }

    /**
     * @return Container
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @return mixed
     */
    public function getSiteSettings()
    {
        $repo = $this->getRepo('CoreSysSiteBundle:Config');
        return $repo->getConfig();
    }

    /**
     * @return mixed
     */
    public function getSiteConfig()
    {
        return $this->getSiteSettings();
    }

    /**
     * @var
     */
    private $src_folder;

    /**
     * @return string
     */
    public function getSrcFolder()
    {
        if (!empty($this->src_folder)) {
            return $this->src_folder;
        }

        $this->src_folder = $this->getRootFolder() . DIRECTORY_SEPARATOR . 'src';
        return $this->src_folder;
    }

    /**
     * @param null $namespace
     * @return string
     */
    public function getNamespaceFolder($namespace = null)
    {
        return $this->getSrcFolder()
        . DIRECTORY_SEPARATOR . $namespace;
    }

    /**
     * @param null $namespace
     * @param null $bundle
     * @return string
     */
    public function getBundleFolder($namespace = null, $bundle = null)
    {
        return $this->getNamespaceFolder($namespace)
        . DIRECTORY_SEPARATOR . $bundle;
    }

    /**
     * @param null $namespace
     * @param null $bundle
     * @return string
     */
    public function getBundleResourcesFolder($namespace = null, $bundle = null)
    {
        return $this->getBundleFolder($namespace, $bundle)
        . DIRECTORY_SEPARATOR . 'Resources';
    }

    /**
     * @param null $namespace
     * @param null $bundle
     * @return string
     */
    public function getBundleViewsFolder($namespace = null, $bundle = null)
    {
        return $this->getBundleResourcesFolder($namespace, $bundle)
        . DIRECTORY_SEPARATOR . 'views';
    }

    /**
     * Generates a URL from the given parameters.
     *
     * @param string         $route         The name of the route
     * @param mixed          $parameters    An array of parameters
     * @param Boolean|string $referenceType The type of reference (one of the constants in UrlGeneratorInterface)
     *
     * @return string The generated URL
     *
     * @see UrlGeneratorInterface
     */
    public function generateUrl($route, $parameters = array(), $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH)
    {
        return $this->get('router')->generate($route, $parameters, $referenceType);
    }
}