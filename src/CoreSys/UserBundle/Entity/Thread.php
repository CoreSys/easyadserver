<?php
// src/CoreSys/UserBundle/Entity/Thread.php

namespace CoreSys\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\MessageBundle\Entity\Thread as BaseThread;

/**
 * @ORM\Table(name="user_thread")
 * @ORM\Entity
 */
class Thread extends BaseThread
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="CoreSys\UserBundle\Entity\User")
     */
    protected $createdBy;

    /**
     * @ORM\OneToMany(
     *   targetEntity="CoreSys\UserBundle\Entity\Message",
     *   mappedBy="thread",
     * cascade={"persist", "remove"}
     * )
     * @var Message[]|\Doctrine\Common\Collections\Collection
     */
    protected $messages;

    /**
     * @ORM\OneToMany(
     *   targetEntity="CoreSys\UserBundle\Entity\ThreadMetadata",
     *   mappedBy="thread",
     *   cascade={"all"}
     * )
     * @var ThreadMetadata[]|\Doctrine\Common\Collections\Collection
     */
    protected $metadata;

    public function hasUnread()
    {
        $return = false;
        foreach( $this->getMessages() as $message ) {
            $md = $message->getAllMetadata();
            if( !empty( $md ) ) {
                foreach( $md as $m ) {
                    if( !$m->getIsRead() ) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public function getLastCreatedAt()
    {
        $date = new \DateTime(  );
        $date->setTimestamp( mktime( 0, 0, 0, 1, 1, 2000 ) );
        foreach( $this->getMessages() as $message ) {
            $mdate = $message->getCreatedAt();
            if( $mdate >= $date ) {
                $date = $mdate;
            }
        }
        return $date;
    }

    /**
     * @return ParticipantInterface
     */
    public function getLastCreatedBy()
    {
        $user = $this->getCreatedBy();
        $date = new \DateTime(  );
        $date->setTimestamp( mktime( 0, 0, 0, 1, 1, 2000 ) );
        foreach( $this->getMessages() as $message ) {
            $mdate = $message->getCreatedAt();
            if( $mdate >= $date ) {
                $date = $mdate;
                $user = $message->getSender();
            }
        }
        return $user;
    }
}