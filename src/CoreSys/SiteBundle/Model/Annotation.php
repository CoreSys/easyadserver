<?php

namespace CoreSys\SiteBundle\Model;

use Doctrine\Common\Annotations\AnnotationReader;

class Annotation {
    public static $reader;
    public function __construct()
    {
        self::$reader = new AnnotationReader();
        self::$reader->setDefaultAnnotaionNamespace( __NAMESPACE__ . '\\' );
    }
    public static function getAnnotationsForClass( $className ) {
        $class = new \ReflectionClass( $className );
        return Annotation::$reader->getClassAnnotations( $class );
    }
}