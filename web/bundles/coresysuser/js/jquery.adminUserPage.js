;(function($,undefined){
    $.fn.adminUserPage = function(options) {
        var p = this,
            defaults = {
                checkable: true,
                entity: 'User',
                selected_count_selector: '.selected-count',
                check_master_selector: 'thead tr .master-check',
                check_row_selector: 'td .row-check',
                add_row_btn_selector: '.add-user',
                remove_rows_btn_selector: '.remove-users',
                table_selector: '#users-table',
                row_edit_btn_selector: '.btn-edit',
                row_view_btn_selector: '.btn-view',
                row_remove_btn_selector: '.btn-remove',
                dt_display_length: 10,
                new_modal_selector: '#new-modal',
                edit_modal_selector: '#edit-modal',
                view_modal_selector: '#view-modal',
                no_sort_columns: [0,6],
                dataTables_aoColumns: [
                    {"sName": 'check', "bSortable": false, "bSearchable": false, "mData": 'check'},
                    {"sName": 'id', "mData": 'id'},
                    {"sName": 'username', "mData": 'username'},
                    {"sName": 'email', "mData": 'email'},
                    {"sName": 'created_at', "bSearchable": false, "mData": 'created_at'},
                    {"sName": 'last_login', "bSearchable": false, "mData": 'last_login'},
                    {"sName": 'roles', "mData": 'roles'},
                    {"sName": 'active', "bSearchable": false, "mData": 'active'},
                    {"sName": 'options', "bSortable": false, "bSearchable": false, "mData": 'options'}
                ],
                remove_row_url: Routing.generate( 'admin_ajax_users_remove_user'),
                add_row_url: Routing.generate('admin_ajax_users_new'),
                edit_row_url: Routing.generate('admin_ajax_users_edit'),
                view_row_url: Routing.generate('admin_ajax_users_view'),
                dataTables_ajax_url: Routing.generate('admin_ajax_users_data_tables'),
                fnGetRowData: getRowData,
                fnPopulateEditedRowData: populateEditedRowData,
                onViewBtnClick: onViewBtnClick
            };

        p.settings = {};
        p.page = null;

        function init()
        {
            p.settings = $.extend({}, defaults, options );
            p.page = new $.fn.adminAjaxTablePage(p.settings);
            p.page.datatable.fnSort( [ [5,'desc'] ] )
        }

        function getRowData(data) {
            return data;
        };

        function populateEditedRowData( row, data ) {
            p.page.datatable.fnReloadAjax();
            p.page.resetRowSelects();
        };

        function onViewBtnClick(e,btn,id) {
            var $btn = $(btn),
                username = $btn.attr('data-username'),
                slug = username || id;

            window.location.href = Routing.generate( 'admin_users_view_user', {'slug': slug});

            return false;
        }

        init();

        return this;
    };
})(jQuery);