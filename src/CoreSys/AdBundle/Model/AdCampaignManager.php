<?php

namespace CoreSys\AdBundle\Model;

use CoreSys\SiteBundle\Model\BaseManager;
use CoreSys\UserBundle\Entity\User;

class AdCampaignManager extends BaseManager
{
    public function locateCampaignsByUser( User $user )
    {
        $repo = $this->getRepo( 'CoreSysAdBundle:Campaign' );
        $campaigns = $repo->findBy( array( 'user' => $user ) );
        return $campaigns;
    }

    public function locateCampaign( $hash = null )
    {
        $repo = $this->getRepo( 'CoreSysAdBundle:Campaign' );
        return $repo->locateCampaign( $hash );
    }
}