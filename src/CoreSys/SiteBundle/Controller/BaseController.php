<?php

namespace CoreSys\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\EventDispatcher\EventDispatcher;

/**
 * Class BaseController
 * @package CoreSys\SiteBundle\Controller
 */
class BaseController extends Controller
{
    /**
     * @var string
     */
    protected $type = 'front';
    /**
     * @var bool
     */
    protected $ajax = false;

    /**
     * @param boolean $ajax
     */
    public function setAjax($ajax)
    {
        $this->ajax = $ajax;
    }

    /**
     * @return boolean
     */
    public function getAjax()
    {
        return $this->ajax;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getEm()
    {
        return $this->getEntityManager();
    }

    /**
     * @return mixed
     */
    public function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * @param null $name
     * @return mixed
     */
    public function getRepo($name = NULL)
    {
        return $this->getEm()->getRepository($name);
    }

    /**
     * @param null $name
     * @return mixed
     */
    public function getRepository($name = NULL)
    {
        return $this->getRepo($name);
    }

    /**
     * @param null $msg
     * @param array $params
     */
    public function msgSuccess($msg = NULL, Array $params = array())
    {
        $this->get('session')->getFlashBag()->add(
            'success',
            $msg
        );
    }

    /**
     * @param null $msg
     * @param array $params
     */
    public function msgError($msg = NULL, Array $params = array())
    {
        $this->get('session')->getFlashBag()->add(
            'error',
            $msg
        );
    }

    /**
     * @param null $msg
     * @param array $params
     */
    public function msgWarning($msg = NULL, Array $params = array())
    {
        $this->get('session')->getFlashBag()->add(
            'warning',
            $msg
        );
    }

    /**
     * @param null $msg
     * @param array $params
     */
    public function msgInfo($msg = NULL, array $params = array())
    {
        $this->get('session')->getFlashBag()->add(
            'info',
            $msg
        );
    }

    /**
     * @param array $data
     * @param bool $exit
     */
    public function echoJsonResponse($data = array(), $exit = true)
    {
        echo json_encode($data);
        if ($exit) {
            exit();
        }
    }

    /**
     * @param array $data
     */
    public function echoJsonSuccess( $msg = null, $data = array())
    {
        $this->echoJsonResponse(array('success' => true, 'msg' => $msg, 'data' => $data));
    }

    /**
     * @param array $data
     */
    public function echoJsonError($msg = null, $data = array())
    {
        $this->echoJsonResponse(array('success' => false, 'msg' => $msg, 'data' => $data));
    }

    /**
     * @return mixed
     */
    public function getSiteSettings()
    {
        return $this->getSiteConfig();
    }

    /**
     * @return mixed
     */
    public function getSiteConfig()
    {
        $repo = $this->getRepo('CoreSysSiteBundle:Config');
        $config = $repo->getConfig();
        return $config;
    }

    /**
     * @var EventDispatcher
     */
    private $dispatcher;

    /**
     * @return EventDispatcher
     */
    public function getDispatcher()
    {
        if (!empty($this->dispatcher)) {
            return $this->dispatcher;
        }

        $this->dispatcher = new EventDispatcher();
        return $this->dispatcher;
    }

    /**
     * @param $entity
     * @param array $params
     */
    public function persist($entity, $params = array())
    {
        $methods = array('prepersist', 'prePersist', 'PrePersist');
        foreach ($methods as $method) {
            if (method_exists($entity, $method)) {
                $entity->$method($params);
            }
        }
        $this->getEntityManager()->persist($entity);
    }

    public function persistAndFlush( $entity )
    {
        $this->persist( $entity );
        $this->flush();
    }

    /**
     * @param $entity
     * @param array $params
     */
    public function remove($entity, $params = array())
    {
        $methods = array('preremove', 'preRemove', 'PreRemove', 'remove', 'Remove');
        foreach ($methods as $method) {
            if (method_exists($entity, $method)) {
                $entity->$method($params);
            }
        }
        $this->getEntityManager()->remove($entity);
    }

    /**
     * flush the entity manager
     */
    public function flush()
    {
        $this->getEntityManager()->flush();
    }

    public function set500Header()
    {
        header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
    }

    public function set500Halt( $msg = null )
    {
        $this->set500Header();
        echo $msg;
        exit;
    }

    /**
     * Dispatch an event
     *
     * @param Event  $event
     * @param string $type
     */
    public function dispatchEvent( &$event, $type = NULL )
    {
        $dispatcher = $this->get( 'event_dispatcher' );
        $dispatcher->dispatch( $type, $event );
    }
}
