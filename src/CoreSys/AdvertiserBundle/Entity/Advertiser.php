<?php

namespace CoreSys\AdvertiserBundle\Entity;

use CoreSys\SiteBundle\Entity\Address;
use Doctrine\ORM\Mapping as ORM;
use CoreSys\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Advertiser
 *
 * @ORM\Table(name="advertiser")
 * @ORM\Entity(repositoryClass="CoreSys\AdvertiserBundle\Entity\AdvertiserRepository")
 * @ORM\HasLifecycleCallbacks
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"advertiser" = "Advertiser", "" = "Advertiser"})
 */
class Advertiser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;
    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="CoreSys\UserBundle\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;
    /**
     * @var Address
     *
     * @ORM\ManyToMany(targetEntity="CoreSys\SiteBundle\Entity\Address")
     * @ORM\JoinTable(name="advertiser_addresses", joinColumns={@ORM\JoinColumn(name="adv_id", referencedColumnName="id")}, inverseJoinColumns={@ORM\JoinColumn(name="addr_id", referencedColumnName="id")})
     */
    private $addresses;
    /**
     * @var Address
     *
     * @ORM\ManyToOne(targetEntity="CoreSys\SiteBundle\Entity\Address", cascade={"persist"})
     * @ORM\JoinColumn(name="shipping_address_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $shipping_address;
    /**
     * @var Address
     *
     * @ORM\ManyToOne(targetEntity="CoreSys\SiteBundle\Entity\Address", cascade={"persist"})
     * @ORM\JoinColumn(name="billing_address_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $billing_address;
    /**
     * @var boolean
     *
     * @ORM\Column(name="shipping_same", type="boolean", nullable=true)
     */
    private $shipping_same;

    /**
     *
     */
    public function __construct()
    {
        $this->setCreatedAt(new \DateTime());
        $this->setUpdatedAt(new \DateTime());
        $this->setAddresses(new ArrayCollection());
        $this->setShippingSame(true);
    }

    /**
     * @param $var
     * @return null
     */
    public function __get($var)
    {
        $function = strtolower(trim($var));
        $function = 'get' . str_replace(' ', '', ucwords(str_replace('_', ' ', $function)));
        if (method_exists($this, $function)) {
            return $this->$function();
        } else {
            if (method_exists($this->user, $function)) {
                return $this->user->$function();
            }
        }
        return null;
    }

    /**
     * @param $var
     * @return null
     */
    public function __set($var,$val)
    {
        $function = strtolower(trim($var));
        $function = 'set' . str_replace(' ', '', ucwords(str_replace('_', ' ', $function)));
        if (method_exists($this, $function)) {
            return $this->$function();
        } else {
            if (method_exists($this->user, $function)) {
                return $this->user->$function();
            }
        }
        return null;
    }

    /**
     * @return Address
     */
    public function getAddresses()
    {
        return $this->addresses;
    }

    /**
     * @param $addresses
     * @return $this
     */
    public function setAddresses($addresses)
    {
        $this->addresses = $addresses;
        return $this;
    }

    /**
     * @param Address $address
     * @return $this
     */
    public function addAddress(Address $address)
    {
        if (!$this->addresses->contains($address)) {
            $this->addresses->add($address);
        }
        return $this;
    }

    /**
     * @param Address $address
     * @return $this
     */
    public function removeAddress(Address $address)
    {
        if ($this->addresses->contains($address)) {
            $this->addresses->removeElement($address);
        }
        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Advertiser
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Advertiser
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        if (!empty($this->user)) {
            return $this->user->getUsername();
        }
        return 'Unknown advertiser';
    }

    /**
     * @ORM\PrePersist
     */
    public function prepersist()
    {
        $this->setUpdatedAt(new \DateTime());
        $billing = $this->getBillingAddress();
        $shipping = $this->getShippingAddress();

        if (!empty($billing)) {
            $billing->setType('billing');
            $billing->setFirstName($this->getUser()->getFirstName());
            $billing->setLastName($this->user->getLastName());
            $billing->setEmailAddress($this->user->getEmail());
            $this->setBillingAddress( $billing );
        }

        if (!empty($shipping)) {
            $shipping->setType('shipping');
            $shipping->setFirstName($this->getUser()->getFirstName());
            $shipping->setLastName($this->user->getLastName());
            $shipping->setEmailAddress($this->user->getEmail());
            $this->setShippingAddress( $shipping );
        }

        if ($this->getShippingSame()) {
            if (!empty($billing)) {
                $this->setShippingAddress($billing);
                $shipping = null;
                $billing->setType('both');
            }
        }
    }

    /**
     * @return \CoreSys\SiteBundle\Entity\Address
     */
    public function getBillingAddress()
    {
        return $this->billing_address;
    }

    /**
     * @param \CoreSys\SiteBundle\Entity\Address $billing_address
     */
    public function setBillingAddress($billing_address)
    {
        $this->billing_address = $billing_address;
    }

    /**
     * @return \CoreSys\SiteBundle\Entity\Address
     */
    public function getShippingAddress()
    {
        return $this->shipping_address;
    }

    /**
     * @param \CoreSys\SiteBundle\Entity\Address $shipping_address
     */
    public function setShippingAddress($shipping_address)
    {
        $this->shipping_address = $shipping_address;
    }

    /**
     * @return bool
     */
    public function getShippingSame()
    {
        if( empty( $this->shipping_same ) ) {
            $this->shipping_same = true;
        }
        return $this->shipping_same === true;
    }

    /**
     * @param bool $same
     * @return $this
     */
    public function setShippingSame($same = true)
    {
        $this->shipping_same = $same === true;
        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user
     *
     * @param User $user
     * @return Advertiser
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }
}
