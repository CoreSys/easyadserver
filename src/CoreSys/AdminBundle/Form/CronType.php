<?php
    /**
     * Created by PhpStorm.
     * User: jmccreig
     * Date: 3/7/14
     * Time: 2:36 PM
     */

    namespace CoreSys\AdminBundle\Form;

    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\OptionsResolver\OptionsResolverInterface;

    /**
     * Class CronType
     * @package CoreSys\AdminBundle\Form
     */
    class CronType extends AbstractType
    {

        /**
         * @param FormBuilderInterface $builder
         * @param array                $options
         */
        public function buildForm( FormBuilderInterface $builder, array $options )
        {
            $builder->add( 'minute', null, array('label' => 'Min', 'attr' => array( 'title' => 'Minute' ) ) )
                    ->add( 'hour', null, array('label' => 'Hr', 'attr' => array( 'title' => 'Hour' ) ) )
                    ->add( 'dayOfMonth', null, array('label' => 'DoM', 'attr' => array( 'title' => 'Day of Month' ) ) )
                    ->add( 'month', null, array('label' => 'Mo', 'attr' => array( 'title' => 'Month' ) ) )
                    ->add( 'dayOfWeek', null, array('label' => 'DoW', 'attr' => array( 'title' => 'Day of Week' ) ) )
                    ->add( 'command' )
                    ->add( 'logFile', 'text', array( 'required' => FALSE ) )
                    ->add( 'errorFile', 'text', array( 'required' => FALSE ) )
                    ->add( 'comment', 'text', array( 'required' => FALSE ) );
        }

        /**
         * @param OptionsResolverInterface $resolver
         */
        public function setDefaultOptions( OptionsResolverInterface $resolver )
        {
            $resolver->setDefaults( array(
                                        'data_class' => 'CoreSys\AdminBundle\Entity\Cron'
                                    ) );
        }

        /**
         * @return string
         */
        public function getName()
        {
            return 'core_sys_admin_cron';
        }
    }