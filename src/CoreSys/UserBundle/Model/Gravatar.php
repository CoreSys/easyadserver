<?php

namespace CoreSys\UserBundle\Model;

use CoreSys\SiteBundle\Model\BaseManager;
use CoreSys\UserBundle\Entity\User;
use Doctrine\Common\Proxy\Exception\InvalidArgumentException;

/**
 * Class Gravatar
 * @package CoreSys\UserBundle\Model
 */
class Gravatar extends BaseManager
{

    /**
     *
     */
    CONST HTTP_URL = 'http://www.gravatar.com/avatar/';

    /**
     *
     */
    CONST HTTPS_URL = 'http://www.gravatar.com/avatar/';

    /**
     * @var int
     */
    protected $size = 80;

    /**
     * @var bool
     */
    protected $default_image = FALSE;

    /**
     * @var string
     */
    protected $max_rating = 'g';

    /**
     * @var bool
     */
    protected $use_secure_url = FALSE;

    /**
     * @var mixed
     */
    protected $param_cache = NULL;

    /**
     * Get ParamCache
     *
     * @return mixed
     */
    public function getParamCache()
    {
        return $this->param_cache;
    }

    /**
     * Set ParamCache
     *
     * @param mixed $param_cache
     */
    public function setParamCache( $param_cache = NULL )
    {
        $this->param_cache = $param_cache;

        return $this;
    }

    /**
     * Get UseSecureUrl
     *
     * @return boolean
     */
    public function getUseSecureUrl()
    {
        return $this->use_secure_url;
    }

    /**
     * Set UseSecureUrl
     *
     * @param boolean $use_secure_url
     */
    public function setUseSecureUrl( $use_secure_url = NULL )
    {
        $this->use_secure_url = $use_secure_url;

        return $this;
    }

    public function getUserGravatar( User $user, $size = 64 )
    {
        $email = $user->getEmail();
        $this->setSize( intval( $size ) );

        return $this->buildUrl( $email );
    }

    /**
     * @param      $email
     * @param bool $hash_email
     *
     * @return string
     */
    public function buildUrl( $email, $hash_email = TRUE )
    {
        return $this->buildGravatarURL( $email, $hash_email );
    }

    public function buildGravatarURL( $email, $hash_email = TRUE )
    {
        if ( $this->use_secure_url ) {
            $url = static::HTTPS_URL;
        } else {
            $url = static::HTTP_URL;
        }

        if ( $hash_email === TRUE && !empty( $email ) ) {
            $url .= $this->getEmailHash( $email );
        } else if ( !empty( $email ) ) {
            $url .= $email;
        } else {
            $url .= str_repeat( '0', 32 );
        }

        if ( $this->param_cache === NULL ) {
            $params    = array();
            $params[ ] = 's=' . $this->getSize();
            $params[ ] = 'r=' . $this->getMaxRating();
            if ( $this->getDefaultImage() ) {
                $params[ ] = 'd=' . $this->getDefaultImage();
            }

            $this->param_cache = ( !empty( $params ) ) ? '?' . implode( '&amp;', $params ) : '';
        }

        $tail = '';
        if ( empty( $email ) ) {
            $tail = !empty( $this->param_cache ) ? '&amp;f=y' : '?f=y';
        }

        return $url . $this->param_cache . $tail;
    }

    public function getEmailHash( $email )
    {
        return hash( 'md5', strtolower( trim( $email ) ) );
    }

    /**
     * Get Size
     *
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set Size
     *
     * @param int $size
     */
    public function setSize( $size = NULL )
    {
        $this->param_cache = NULL;

        if ( !is_int( $size ) && !ctype_digit( $size ) ) {
            throw new InvalidArgumentException( 'Avatar size specified must be an integer' );
        }

        $this->size = (int)$size;

        if ( $this->size > 512 || $this->size < 0 ) {
            throw new InvalidArgumentException( 'Avatar size must be within 0px and 512px' );
        }

        return $this;
    }

    /**
     * Get MaxRating
     *
     * @return string
     */
    public function getMaxRating()
    {
        return $this->max_rating;
    }

    /**
     * Set MaxRating
     *
     * @param string $max_rating
     */
    public function setMaxRating( $max_rating = NULL )
    {
        $this->param_cache = NULL;

        $rating        = strtolower( $max_rating );
        $valid_ratings = array( 'g' => 1, 'pg' => 1, 'r' => 1, 'x' => 1 );
        if ( !isset( $valid_ratings[ $rating ] ) ) {
            throw new InvalidArgumentException( sprintf( 'Invalid rating "%s" specified, only "g", "pg", "r", or "x" are allowed to be used.', $rating ) );
        }

        $this->max_rating = $rating;

        return $this;
    }

    /**
     * Get DefaultImage
     *
     * @return boolean
     */
    public function getDefaultImage()
    {
        return $this->default_image;
    }

    /**
     * Set DefaultImage
     *
     * @param boolean $default_image
     */
    public function setDefaultImage( $default_image = NULL )
    {
        if ( $default_image === FALSE ) {
            $this->default_image = FALSE;
        } else {
            $this->param_cache = NULL;
            $_image            = strtolower( $default_image );
            $valid_defaults    = array( '404' => 1, 'mm' => 1, 'identicon' => 1, 'monsterid' => 1, 'wavatar' => 1, 'retro' => 1 );
            if ( !isset( $valid_defaults[ $_image ] ) ) {
                if ( !filter_var( $default_image, FILTER_VALIDATE_URL ) ) {
                    throw new InvalidArgumentException( 'The default image specified is not a recognized gravatar "default" and is not a valid URL' );
                } else {
                    $this->default_image = rawurlencode( $default_image );
                }
            } else {
                $this->default_image = $_image;
            }
        }

        return $this;
    }
}