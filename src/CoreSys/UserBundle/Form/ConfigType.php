<?php
    /**
     * This file is part of the CoreSysUserBundle package.
     * (c) J&L Core Systems http://jlcoresystems.com | http://joshmccreight.com
     */

    namespace CoreSys\UserBundle\Form;

    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\OptionsResolver\OptionsResolverInterface;
    use Symfony\Component\Security\Core\Validator\Constraint\UserPassword;

    /**
     * Class ConfigType
     * @package CoreSys\UserBundle\Form
     */
    class ConfigType extends AbstractType
    {

        /**
         * @var
         */
        private $class;

        /**
         * @param FormBuilderInterface $builder
         * @param array                $options
         */
        public function buildForm( FormBuilderInterface $builder, array $options )
        {
            $builder
                ->add( 'messageAdminRole', 'entity', array( 'label' => 'Admin Role',
                                                            'class' => 'CoreSysUserBundle:Role',
                                                            'attr'  => array( 'class'         => 'select2 form-control',
                                                                              'data-postdesc' => 'The admin role for messages' ) ) )
                ->add( 'messageModeratorRole', 'entity', array( 'label' => 'Moderator Role',
                                                                'class' => 'CoreSysUserBundle:Role',
                                                                'attr'  => array( 'class'         => 'select2 form-control',
                                                                                  'data-postdesc' => 'The Moderator role for messages' ) ) )
                ->add( 'messageTrashRole', 'entity', array( 'label' => 'Trash Role', 'class' => 'CoreSysUserBundle:Role',
                                                            'attr'  => array( 'class' => 'select2 form-control', 'data-postdesc' => 'The minimum role to view trash folder' ) ) )
                ->add( 'minimumPasswordLength', 'number', array( 'attr' => array( 'data-postdesc' => 'Users minimum password length' ) ) )
                ->add( 'maximumPasswordLength', 'number', array( 'attr' => array( 'data-postdesc' => 'Users maximum password length' ) ) );
        }

        /**
         * @param OptionsResolverInterface $resolver
         */
        public function setDefaultOptions( OptionsResolverInterface $resolver )
        {
            $resolver->setDefaults( array(
                                        'intention' => 'configuration',
                                    ) );
        }

        /**
         * @return string
         */
        public function getName()
        {
            return 'coresys_user_config_type';
        }
    }