<?php

namespace CoreSys\AdvertiserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CoreSys\SiteBundle\Controller\AdminController as BaseController;

/**
 * Class AdminController
 * @package CoreSys\AdvertiserBundle\Controller
 *
 * @Route("/admin/advertisers")
 */
class AdminController extends BaseController
{
    /**
     * @Route("/", name="admin_advertisers_index")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }
}
