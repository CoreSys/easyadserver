;(function ($, undefined) {
    $.fn.adminBasicTablePage = function (options) {
        var t = this,
            $this = $(this),
            defaults = {
                checkable: true,
                entity: 'Role',
                selected_count_selector: '.selected-count',
                check_master_selector: 'thead tr .master-check',
                check_row_selector: 'td .row-check',
                add_row_btn_selector: '.add-role',
                remove_rows_btn_selector: '.remove-roles',
                table_selector: '#roles-table',
                row_edit_btn_selector: '.btn-edit',
                row_remove_btn_selector: '.btn-remove',
                row_view_btn_selector: '.btn-view',
                row_manage_btn_selector: '.btn-manage',
                dt_display_length: 10,
                new_modal_selector: '#new-modal',
                edit_modal_selector: '#edit-modal',
                view_modal_selector: '#view-modal',
                remove_row_url: Routing.generate( 'admin_ajax_users_role_remove'),
                add_row_url: Routing.generate('admin_ajax_users_roles_new'),
                edit_row_url: Routing.generate('admin_ajax_users_roles_edit'),
                view_row_url: null,
                manage_row_url: null,
                no_sort_columns: [0],
                no_show_columns: [],
                fnGetRowData: null,
                fnPopulateEditedRowData: null,
                aaSorting: [],
                aoColumns: [],
                no_rows_template: '<div class="well"><div class="row"><div class="col-md-12 no-rows text-center"><h3>There are currently no Access Rights</h3></div></div></div>',
                onViewBtnClick: null
            };

        t.settings = {};
        t.addRowBtn = null;
        t.removeRowsBtn = null;
        t.table = null;
        t.datatable = null;
        t.checkMaster = null;
        t.newModal = null;
        t.editModal = null;
        t.viewModal = null;
        t.responsiveHelper = null;

        function init() {
            t.settings = $.extend({}, defaults, options);
            
            t.addRowBtn = $(t.settings.add_row_btn_selector);
            t.addRowBtn.click(addNewRowClick)
            t.removeRowsBtn = $(t.settings.remove_rows_btn_selector);
            t.removeRowsBtn.hide();
            t.removeRowsBtn.removeClass('display-hide');
            t.removeRowsBtn.click(removeRowsClick);
            t.newModal = $(t.settings.new_modal_selector);
            t.editModal = $(t.settings.edit_modal_selector);
            t.viewModal = $(t.settings.view_modal_selector);

            initTable();
            postInitTable();

            initCheckable();
            t.checkSelectedRows(true);
            resetRowOptions();

            initDatatables();

            $('body').on('hidden.bs.modal', '.modal', function () {
                $(this).removeData('bs.modal');
            });

            checkRowCounts();
        }

        function resetRowOptions() {
            detectResponsiveExpanderClick();
            registerRowEditBtnClicks();
            registerRowRemoveBtnClicks();
            registerRowViewBtnClicks();
            registerRowManageBtnClicks();
            registerCheckboxUniforms();
        }

        function detectResponsiveExpanderClick()
        {
            // this will handle the closed detail-rows
            t.table.find('tbody tr').each(function(){
                var ex = $(this).find('.responsiveExpander');
                if(ex.length > 0 && ex !== undefined && ex !== null) {
                    ex.click(function(){
                        setTimeout(function(){
                            resetRowOptions();
                        },250);
                    });
                }
            });
            // what about the already open detail rows?
            t.table.find('tbody tr.row-detail').each(function(){
                t.getRowDataId( $(this) );
                var $btn = $(this).find(t.settings.row_edit_btn_selector);
                registerRowViewBtnClick($btn);
            });
        }

        function registerCheckboxUniforms()
        {
            if(t.settings.checkable) {
                $(':checkbox').not('.toggle').each(function(){
                    var $this = $(this);
                    if($.fn.uniform) {
                        $this.uniform();
                    }
                });
            }
        }

        function registerRowEditBtnClicks() {
            t.table.find('tbody').find('tr').each(function () {
                var $btn = $(this).find(t.settings.row_edit_btn_selector);
                registerRowEditBtnClick($btn);
            });
        }

        function checkRowCounts()
        {
            var rows = t.table.find('tbody').find('tr'),
                count = rows.length;

            if(count > 0) {
                $('.no-rows').remove();
            } else {
                var norows = $(t.settings.no_rows_template);
                t.table.after(norows);
            }
        }

        function registerRowEditBtnClick($btn) {
            if ($btn) {
                $btn.unbind('click');
                $btn.click(function (e) {
                    e.preventDefault();
                    var id = t.getRowDataId( $btn.closest('tr') );
                    t.editRow(id);
                });
            }
        }

        function registerRowRemoveBtnClicks() {
            t.table.find('tbody').find('tr').each(function () {
                var $btn = $(this).find(t.settings.row_remove_btn_selector);
                registerRowRemoveBtnClick($btn);
            });
        }

        function registerRowRemoveBtnClick($btn) {
            if ($btn) {
                $btn.unbind('click');
                $btn.click(function (e) {
                    e.preventDefault();
                    var id = t.getRowDataId( $btn.closest('tr') );
                    bootbox.confirm( 'Are you sure you want to remove this Row?', function(res){
                        if(res) {
                            removeRow(id);
                        }
                    });
                });
            }
        }

        function removeRow( id ) {
            $.ajax({
                url: t.settings.remove_row_url + '/' + id,
                data: {'id':id},
                type: 'POST',
                success:function(res) {
                    res = JSON.parse(res);
                    if(res.success) {
                        toastr['success']('Successfully removed ' + t.settings.entity, 'Removed');
                        var $row = t.getRowById( id),
                            row = $row[0];

                        t.datatable.fnDeleteRow(row);

                        resetRowOptions();
                        t.forceUncheckAll()

                        checkRowCounts();
                    } else {
                        toastr['error'](res.msg, 'Error');
                    }
                }
            });
        }

        function registerRowViewBtnClicks() {
            t.table.find('tbody').find('tr').each(function(){
                t.getRowDataId( $(this) );
                var $btn = $(this).find(t.settings.row_view_btn_selector);
                registerRowViewBtnClick($btn);
            });
        }

        function registerRowViewBtnClick($btn) {
            if( $btn ) {
                $btn.unbind('click');
                $btn.click(function(e){
                    e.preventDefault();
                    var id = t.getRowDataId( $btn.closest('tr') );
                    if(typeof t.settings.onViewBtnClick == 'function') {
                        if(!t.settings.onViewBtnClick(e,$btn,id)) {
                            return;
                        }
                    }
                    t.viewRow( id );
                });
            }
        }

        function registerRowManageBtnClicks()
        {
            t.table.find('tbody').find('tr').each(function(){
                t.getRowDataId( $(this) );
                var $btn = $(this).find(t.settings.row_manage_btn_selector);
                registerRowManageBtnClick($btn);
            });
        }

        function registerRowManageBtnClick($btn)
        {
            if($btn) {
                $btn.unbind('click');
                $btn.click(function(e){
                    e.preventDefault();
                    var id = t.getRowDataId( $btn.closest( 'tr' ) );
                    window.location = t.settings.manage_row_url + '/' + id;
                });
            }
        }

        function initCheckable() {
            if (t.settings.checkable) {
                t.checkMaster = t.table.find(t.settings.check_master_selector);
                if (t.checkMaster.length < 0) {
                    t.settings.checkable = false;
                } else {
                    t.checkMaster.change(checkMasterClick);
                }

                initRowSelects();
            }
        };

        function initRowSelects() {
            if (!t.settings.checkable) {
                return;
            }

            t.table.find('tbody').find('tr').each(function () {
                initRowSelect($(this));
            });
        }

        function initRowSelect($row) {
            var $cb = $row.find(t.settings.check_row_selector);
            if ($cb.length > 0) {
                $cb.unbind('change');
                $cb.change(function () {
                    var checked = $(this).prop('checked');
                    if (checked) {
                        t.selectRow($row);
                    } else {
                        t.unselectRow($row);
                    }
                    t.checkSelectedRows();
                });
            }
        }

        function checkMasterClick(e) {
            e.preventDefault();
            t.checkToggleAll();
        };

        function initTable() {
            t.table = $(t.settings.table_selector);
        };

        function postInitTable() {
            t.table.find('thead th.display-hide').each(function(){
                $(this).removeClass('display-hide');
            });
            t.table.find('tbody td.display-hide').each(function(){
                $(this).removeClass('display-hide');
            });
        };

        function initDatatables() {
            t.settings.aaSorting = t.settings.aaSorting || [];
            t.settings.aoColumns = t.settings.aoColumns || [];
            if(t.settings.aoColumns.length == 0) {
                t.settings.aoColumns = null;
            }

            t.table.dataTable({
                "sDom": "<'row'<'col-md-6 col-sm-6 col-xs-6'l><'col-md-6 col-sm-6 col-xs-6'f>r>t<'row paginator'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", //default layout without horizontal scroll(remove this setting to enable horizontal scroll for the table)
                "aLengthMenu": [
                    [5, 10, 25, 50, 100, -1],
                    [5, 10, 25, 50, 100, "All"] // change per page values here
                ],
                "aaSorting": t.settings.aaSorting,
                "aoColumns": t.settings.aoColumns,
                "sPaginationType": 'bootstrap',
                "iDisplayLength": t.settings.dt_display_length,
                "bAutoWidth": false,
                "aoColumnDefs": [
                    {"bSortable": false, aTargets: t.settings.no_sort_columns}
                ],
                "fnPreDrawCallback": function() {
                    getReponsiveHelper();
                },
                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    var r = getReponsiveHelper();
                    r.createExpandIcon(nRow);
                },
                "fnDrawCallback": function(oSettings) {
                    var r = getReponsiveHelper();
                    r.respond();
                    setTimeout(function(){
                        resetRowOptions();
                        initRowSelects();
                    },500);
                }
            });
            t.datatable = t.table.dataTable();

            var id = t.table.attr('id') + '_wrapper',
                tw = $('#' + id);
            tw.find('.dataTables_filter input').addClass('form-control');
            tw.find('.dataTables_length select').addClass('form-control input-small');
            tw.find('.dataTables_length select').select2();
            tw.find('.dataTables_length select').change(resetRowOptions);
            tw.find('.dataTables_length select').change(resetRowOptions);
            tw.find('.dataTables_length select').change(t.forceUncheckAll);
            tw.find('.pagination').click(resetRowOptions);
            tw.find('.pagination').click(t.forceUncheckAll);
            t.table.find('thead').click(resetRowOptions);

            // lets re-style the datatables_filter input
            var filter_wrapper = tw.find('.dataTables_filter'),
                label = filter_wrapper.find('label'),
                input = filter_wrapper.find('input'),
                $ii = $('<div class="input-icon" />'),
                $icon = $('<i class="fa fa-search"></i>');

            $ii.addClass('pull-right');
            $ii.append( $icon).append(input);
            filter_wrapper.append($ii);
            label.remove();

            // re-style the selection count
            var length_wrapper = tw.find('.dataTables_length'),
                label = length_wrapper.find('label');
            label.children().each(function(){
                length_wrapper.append($(this));
            });
            label.remove();
        };

        function getReponsiveHelper()
        {
            if(t.responsiveHelper === null) {
                t.responsiveHelper = new ResponsiveDatatablesHelper(t.table, {tablet: 1024, phone: 480}, {
                    hideEmptyColumnsInRowDetail: true,
                    doNotShowColumns: t.settings.no_show_columns
                });
            }
            return t.responsiveHelper;
        }

        function removeRowsClick(e) {
            e.preventDefault();
            bootbox.confirm('Are you sure you want to remove these ' + t.settings.entity + '(s)?',function(res){
                if(res) {
                    t.removeRows();
                }
            });
        };

        function addNewRowClick(e) {
            e.preventDefault();
            t.addNewRow();
        };

        t.checkSelectedRows = function (repeat) {
            repeat = repeat === true;
            if (t.getSelectedRowsCount() > 0) {
                t.removeRowsBtn.show();
            } else {
                t.removeRowsBtn.hide();
            }
            if (repeat) {
                setTimeout(function () {
                    t.checkSelectedRows(true);
                }, 2000);
            }
        }

        t.getSelectedRowsCount = function () {
            var rows = t.getSelectedRows();
            $(t.settings.selected_count_selector).html(rows.length);
            return rows.length;
        }

        t.getSelectedRows = function () {
            return t.table.find('tbody').find('tr.selected:visible');
        };

        t.forceUncheckAll = function () {
            t.checkMaster.prop('checked', false);
            if ($.uniform) {
                $.uniform.update(t.checkMaster);
            }
            t.checkMaster.trigger('change');
        }

        t.checkToggleAll = function () {
            if (!t.settings.checkable) {
                return;
            }

            var checked = t.checkMaster.prop('checked');
            if (checked) {
                t.checkAllRows();
            } else {
                t.uncheckAllRows();
            }

            t.checkSelectedRows();
        };

        t.checkAllRows = function () {
            if (!t.settings.checkable) {
                return;
            }
            t.table.find('tbody').find('tr').each(function () {
                var $cb = $(this).find('.row-check');
                if ($cb.attr('type') == 'checkbox') {
                    $cb.prop('checked', true);
                    if ($.uniform) {
                        $.uniform.update($cb);
                    }
                }
                t.selectRow($(this));
            });
        };

        t.uncheckAllRows = function () {
            if (!t.settings.checkable) {
                return;
            }
            t.table.find('tbody').find('tr').each(function () {
                var $cb = $(this).find('.row-check');
                if ($cb.attr('type') == 'checkbox') {
                    $cb.prop('checked', false);
                    if ($.uniform) {
                        $.uniform.update($cb);
                    }
                }
                t.unselectRow($(this));
            });
        };

        t.selectRow = function ($row) {
            if (!$row.hasClass('selected') && $row.attr('data-id') !== undefined) {
                $row.addClass('selected');
                var $cb = $row.find('.row-check');
                if ($cb.length > 0) {
                    $cb.prop('checked', true);
                    if ($.uniform) {
                        $.uniform.update($cb);
                    }
                }
            }
        };

        t.unselectRow = function ($row) {
            if ($row.hasClass('selected')) {
                $row.removeClass('selected');
                var $cb = $row.find('.row-check');
                if ($cb.length > 0) {
                    $cb.prop('checked', false);
                    if ($.uniform) {
                        $.uniform.update($cb);
                    }
                }
            }
        }

        t.removeRows = function () {
            var selected = t.getSelectedRows();
            $.each(selected,function(ix,row){
                var $row = $(row),
                    id = t.getRowDataId($row);
                if(id !== null && id) {
                    removeRow(id);
                }
            });
        };

        t.addNewRow = function () {
            t.newModal.modal({'remote': t.settings.add_row_url });
        };

        t.editRow = function(id) {
            t.editModal.modal({'remote': t.settings.edit_row_url + '/' + id});
        };

        t.addedNewRow = function(res, status ) {
            res = JSON.parse(res);
            var data = res.data;

            if(!res.success) {
                toastr['error'](res.msg, 'Error');
                return;
            }

            var rowData = t.getRowData( data );

            // add the row to the table
            t.datatable.fnAddData( rowData );

            setTimeout(function(){
                resetRowOptions();
                t.forceUncheckAll();
            },750);

            toastr['success']('Successfully added new Role', 'Added');

            checkRowCounts();
        };

        t.getRowData = function(data) {
            if(t.settings.fnGetRowData !== undefined) {
                if(typeof t.settings.fnGetRowData == 'function') {
                    return t.settings.fnGetRowData( data );
                }
            }

            var rowData = [];

            var sw = '<i class="fa fa-' + ( data.switch ? 'checkgreen' : 'times red' ) + '"></i>',
                ac = '<i class="fa fa-' + ( data.active ? 'checkgreen' : 'times red' ) + '"></i>',
                actions = '<a class="btn btn-xs btn-info btn-edit" href="javascript:void(0)" title="Edit ' + t.settings.entity + '"><i class="fa fa-edit"></i> Edit</a><a class="btn btn-xs btn-danger btn-remove" href="javascript:void(0)" title="Remove ' + t.settings.entity + '"><i class="fa fa-trash-o"></i> Remove</a>';

            // create the data row
            rowData[0] = '<input type="checkbox" data-id="' + data.id  + '" class="row-check" value="1">';
            rowData[1] = data.id;
            rowData[2] = data.name;
            rowData[3] = data.role;
            rowData[4] = data.parents;
            rowData[5] = data.users;
            rowData[6] = sw;
            rowData[7] = ac;
            rowData[8] = actions;

            return rowData;
        };


        t.getRowDataId = function(row) {
            var $row = $(row),
                did = $row.find('[data-id]');

            if($row.attr('data-id') !== undefined) {
                return $row.attr('data-id');
            }

            if(did.length > 0) {
                did = did[0]
                did = $(did);
                $row.attr('data-id', did.attr('data-id'));
                return did.attr('data-id');
            }

            return null;
        };

        t.getRowById = function(id) {
            var $row = null,
                did = t.table.find('tbody').find('[data-id=' + id + ']');

            if(did.length > 0) {
                did = did[0];
                did = $(did);

                if(did.is('tr')) {
                    return did;
                }

                $row = did.closest('tr');
                $row.attr('data-id',id);
            }

            return $row;
        }

        t.editedRow = function(res, status) {
            res = JSON.parse( res );
            var data = res.data,
                $row = t.getRowById(data.id);

            t.populateEditedRowData( $row, data );

            if(toastr) {
                toastr['success']('Successfully saved ' + t.settings.entity,'Saved ' + t.settings.entity);
            }
        };

        t.populateEditedRowData = function( row, data ) {
            if(t.settings.fnPopulateEditedRowData !== undefined) {
                if(typeof t.settings.fnPopulateEditedRowData == 'function'
                    || typeof t.settings.fnPopulateEditedRowData == 'object') {
                    return t.settings.fnPopulateEditedRowData( row, data );
                }
            }

            var $row = $(row);

            $row.find('td').eq(2).html( data.name );
            $row.find('td').eq(3).html( data.role );
            $row.find('td').eq(4).html( data.parents );
            $row.find('td').eq(5).html( data.users );

            var sw = 'fa fa-check green';
            if( !data.switch ) {
                sw = 'fa fa-times red';
            }
            $row.find('td').eq(6).html( '<i class="' + sw + '"></i>' );

            var ac = 'fa fa-check green';
            if( !data.active ) {
                ac = 'fa fa-times red';
            }
            $row.find('td').eq(7).html( '<i class="' + ac + '"></i>' );

            $row.find('td').eq(2).css('color',data.color + ' !important');
        };

        init();

        return t;
    }
})(jQuery);