<?php

namespace CoreSys\SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class IpAddressType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add( 'ipAddress', null, array( 'required' => true ) )
            ->add( 'blackListed', 'checkbox', array( 'required' => false ) );
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CoreSys\SiteBundle\Entity\IpAddress'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'coresys_sitebundle_ip_address';
    }
}
