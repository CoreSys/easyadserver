<?php

namespace CoreSys\SiteBundle\Controller;

use CoreSys\SiteBundle\Form\ConfigType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class SettingsController
 * @package CoreSys\SiteBundle\Controller
 *
 * @Route("/admin/site/settings", options={"expose"=true})
 */
class SettingsController extends BaseController
{
    /**
     * @Route("/upload_site_image", name="admin_site_settings_upload_image")
     * @Template()
     */
    public function uploadSiteImageAction()
    {
        $handler = $this->get( 'core_sys_media.upload_handler' );
        $handler->process();
        exit;
//        $manager = $this->get( 'core_sys_site.settings_manager');
//        $response = $manager->uploadSiteImage();
//        $this->echoJsonResponse( $response, true );
    }

    /**
     * @Route("/", name="admin_site_settings")
     * @Template()
     */
    public function indexAction()
    {
        $this->get( 'core_sys_theme.theme_manager' );
        $settings = $this->getSiteSettings();
        $form = $this->createForm( 'coresys_sitebundle_config', $settings );
        $result = $this->handleSettingsSave( $form );
        if(!empty( $result ) ) {
            return $result;
        }

        return array( 'form' => $form->createView(), 'settings' => $settings );
    }

    public function handleSettingsSave( $form )
    {
        $request = $this->get( 'request' );
        if( $request->isMethod( 'POST' ) ) {
            $form->handleRequest( $request );
            if( $form->isValid() ) {
                $settings = $form->getData();

                $manager = $this->get( 'core_sys_site.settings_manager' );
                $settings = $manager->processSettingsForm( $form, $settings );

                $this->persist( $settings );
                $this->flush();
                $this->msgSuccess( 'Successfully saved site settings' );
                return $this->redirect( $this->generateUrl( 'admin_site_settings' ) );
            } else {
                $this->msgError( 'Could not save site settings' );
            }
        }

        return null;
    }
}
